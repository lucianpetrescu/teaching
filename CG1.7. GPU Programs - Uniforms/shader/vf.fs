#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

uniform uint mode;
uniform vec3 color1;
uniform vec3 color2;
uniform vec3 color3;
uniform vec3 color4;
uniform vec3 not_used;

void main(){
	//red
	switch(mode){
		case 0 : colorout = color1; break;
		case 1 : colorout = color2; break;
		case 2 : colorout = color3; break;
		case 3 : colorout = color4; break;
	}
}
