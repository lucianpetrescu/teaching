#version 450

//get data from attribute pipe #0, interpret it as vec2 
layout(location = 0) in vec2 pos;

uniform float xdisp;
uniform float ydisp;

void main(){
	//send it down the pipeline
	gl_Position = vec4(pos + vec2(xdisp, ydisp), 0, 1);
}
