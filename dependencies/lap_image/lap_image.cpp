///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include "lap_image.hpp"

#include <cmath>
#include <iostream>

#if defined (__GNUG__)
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#endif

#if defined(_MSC_VER)
#pragma warning( push )
#pragma warning( disable : 4996)
#pragma warning( disable : 4244)
#endif
namespace lap {
	namespace dep {
		#include "lap_image_dep.hpp"
	}
}
#if defined(_MSC_VER)
#pragma warning(pop)
#endif

#if defined (__GNUG__)
	#pragma GCC diagnostic pop
#endif

#if defined(_MSC_VER)
#pragma warning( push )
#pragma warning( disable : 4800)
#pragma warning( disable : 4244)
#endif



namespace lap{

	void Image::internalCopy(unsigned char* dst, const unsigned char* src, int w, int h, int ndst, int nsrc, int r_channel, int g_channel, int b_channel, int a_channel){
		assert(dst && src);

		//no channel operations? -> fast memcpy
		if (nsrc == ndst){
			if ((nsrc == 1 && r_channel == 0) || (nsrc == 2 && r_channel == 0 && g_channel == 1) || (nsrc == 3 && r_channel == 0 && g_channel == 1 && b_channel == 2) || (nsrc == 4 && r_channel == 0 && g_channel == 1 && b_channel == 2 && a_channel ==3)){
				std::memcpy(dst, src, w*h*nsrc);
				return;
			}
		}

		//channel operations?
		unsigned char rgba[4];
		for (int i = 0; i<w*h; i++){
			//read
			rgba[0] = src[i*nsrc];
			if (nsrc>1) rgba[1] = src[i*nsrc + 1];
			if (nsrc>2) rgba[2] = src[i*nsrc + 2];
			if (nsrc>3) rgba[3] = src[i*nsrc + 3];
			
			//write
						dst[i*ndst]		= rgba[r_channel];
			if (ndst>1) dst[i*ndst + 1] = rgba[g_channel];
			if (ndst>2) dst[i*ndst + 2] = rgba[b_channel];
			if (ndst>3) dst[i*ndst + 3] = rgba[a_channel];
		}
	}
	bool Image::internalLoadFile(const std::string& filename, int ndst, int r, int g, int b, int a){
		int w, h, nsrc, frames;
		unsigned char* filedata = dep::stbi_xload(filename.c_str(), &w, &h, &nsrc, &frames);
		if (!filedata || frames == 0) return false;

		//adapt request if necessary
		if (ndst == -1){
			ndst = nsrc;

			//fix for 24bpp BMP in stb
			std::string ext = filename.substr(filename.rfind('.'));
			std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
			if (ext == ".bmp" && nsrc == 4) ndst = 3;
		}

		//check channels for reading
		if ((nsrc == 1 && r > 1) || (nsrc == 2 && (r > 2 || g > 2)) || (nsrc == 3 && (r > 3 || g > 3 || b > 3)) || (nsrc == 4 && (r > 4 || g > 4 || b > 4 || a > 4))){
			assert(false);
		}

		//check channels for writing
		if ((ndst == 1 && r < 0) || (ndst == 2 && (r < 0 || g < 0)) || (ndst == 3 && (r < 0 || g < 0 || b < 0)) || (ndst == 4 && (r < 0 || g < 0 || b < 0 || a < 0))){
			assert(false);
		}

		reset(w, h, ndst, frames);
		total_delay = 0;
		for (int i = 0; i < frames; i++){
			//copy
			internalCopy(&data[i][0], (const unsigned char*)filedata, w, h, ndst, nsrc, r, g, b, a);
			filedata = filedata + w * h * nsrc;
			//delay
			if (frames>1) delay[i] = (*((short*)filedata));
			else delay[i] = 0;
			total_delay += delay[i];
			filedata += 2;
		}
		filedata -= (w * h * nsrc + 2)*frames;

		dep::stbi_image_free(filedata);
		return true;
	}
	void Image::internalLoadImg(const Image& img, int ndst, int r, int g, int b, int a){
		int w = img.width;
		int h = img.height;
		int nsrc = img.num_channels;
		int frames = img.num_frames;

		//adapt request if necessary
		if (ndst == -1) ndst = nsrc;

		//check channels for reading
		if ((nsrc == 1 && r > 1) || (nsrc == 2 && (r > 2 || g > 2)) || (nsrc == 3 && (r > 3 || g > 3 || b > 3)) || (nsrc == 4 && (r > 4 || g > 4 || b > 4 || a > 4))){
			assert(false);
		}
		
		//check channels for writing
		if ((ndst == 1 && r < 0) || (ndst == 2 && (r < 0 || g < 0)) || (ndst == 3 && (r < 0 || g < 0 || b < 0)) || (ndst == 4 && (r < 0 || g < 0 || b < 0 || a < 0))){
			assert(false);
		}

		reset(w, h, ndst, frames);
		total_delay = 0;
		for (int i = 0; i < frames; i++){
			//copy
			internalCopy(&data[i][0], &(img.data[i][0]), w, h, ndst, nsrc, r, g, b, a);
			//delay
			if (frames>1) delay[i] = img.delay[i];
			else delay[i] = 0;
			total_delay += delay[i];
		}
	}
	void Image::internalLoadMemory(const unsigned char *memory, int w, int h, int nsrc, int ndst, int r, int g, int b, int a){
		assert(memory);

		//adapt request if necessary
		if (ndst == -1) ndst = nsrc;

		//check channels for reading
		if ((nsrc == 1 && r > 1) || (nsrc == 2 && (r > 2 || g > 2)) || (nsrc == 3 && (r > 3 || g > 3 || b > 3)) || (nsrc == 4 && (r > 4 || g > 4 || b > 4 || a > 4))){
			assert(false);
		}

		//check channels for writing
		if ((ndst == 1 && r < 0) || (ndst == 2 && (r < 0 || g < 0)) || (ndst == 3 && (r < 0 || g < 0 || b < 0)) || (ndst == 4 && (r < 0 || g < 0 || b < 0 || a < 0))){
			assert(false);
		}

		reset(w, h, ndst, 1);
		total_delay = 0;
		//copy
		internalCopy(&data[0][0], memory, w, h, ndst, nsrc, r, g, b, a);
		//delay
		delay[0] = 0;
		total_delay = 0;
	}
	void Image::internalLoadMemory(const unsigned char **memory, const unsigned int* times, int w, int h, int nsrc, int frames, int ndst, int r, int g, int b, int a){
		assert(memory && times);

		//adapt request if necessary
		if (ndst == -1) ndst = nsrc;

		//check channels for reading
		if ((nsrc == 1 && r > 1) || (nsrc == 2 && (r > 2 || g > 2)) || (nsrc == 3 && (r > 3 || g > 3 || b > 3)) || (nsrc == 4 && (r > 4 || g > 4 || b > 4 || a > 4))){
			assert(false);
		}

		//check channels for writing
		if ((ndst == 1 && r < 0) || (ndst == 2 && (r < 0 || g < 0)) || (ndst == 3 && (r < 0 || g < 0 || b < 0)) || (ndst == 4 && (r < 0 || g < 0 || b < 0 || a < 0))){
			assert(false);
		}

		reset(w, h, ndst, frames);
		for (int i = 0; i < frames; i++){
			//copy
			internalCopy(&data[i][0], memory[i], w, h, ndst, nsrc, r, g, b, a);
		}
		//delays
		total_delay = 0;
		for (int i = 0; i<frames-1; i++){
			delay[i] = times[i];	
			total_delay += delay[i];
		}
	}
	void Image::internalLoadMemory(const std::vector<std::vector<unsigned char>>& memory, const std::vector<unsigned int>& times, int w, int h, int nsrc, int frames, int ndst, int r, int g, int b, int a){
		//adapt request if necessary
		if (ndst == -1) ndst = nsrc;

		//check channels for reading
		if ((nsrc == 1 && r > 1) || (nsrc == 2 && (r > 2 || g > 2)) || (nsrc == 3 && (r > 3 || g > 3 || b > 3)) || (nsrc == 4 && (r > 4 || g > 4 || b > 4 || a > 4))){
			assert(false);
		}

		//check channels for writing
		if ((ndst == 1 && r < 0) || (ndst == 2 && (r < 0 || g < 0)) || (ndst == 3 && (r < 0 || g < 0 || b < 0)) || (ndst == 4 && (r < 0 || g < 0 || b < 0 || a < 0))){
			assert(false);
		}

		reset(w, h, ndst, frames);
		for (int i = 0; i < frames; i++){
			//copy
			internalCopy(&data[i][0], &memory[i][0], w, h, ndst, nsrc, r, g, b, a);
		}
		//delays
		total_delay = 0;
		for (int i = 0; i<frames - 1; i++){
			delay[i] = times[i];
			total_delay += delay[i];
		}
	}

	Image::Image() {
		reset(0, 0, 0, 0);
		color_space = Image::ColorSpace::SRGB;
	}
	Image::Image(unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Image::ColorSpace& cspc){
		assert(w > 0 && h > 0 && n > 0 && f > 0);
		reset(w,h,n,f);
		color_space = cspc;
	}
	Image::Image(const std::string& filename, const Image::ColorSpace& cspc, bool *ret ){
		bool result = internalLoadFile(filename,-1, 0, 1, 2, 3);
		color_space = cspc;
		if (ret) (*ret) = result;
	}
	Image::Image(const std::string& filename, const Channel& to_r_channel, const Image::ColorSpace& cspc, bool *ret){
		int r = static_cast<int>(to_r_channel);
		bool result = internalLoadFile(filename, 1, r, -1, -1, -1); 
		color_space = cspc;
		if (ret) (*ret) = result;
	}
	Image::Image(const std::string& filename, const Channel& to_r_channel, const Channel &to_g_channel, const Image::ColorSpace& cspc, bool *ret){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		bool result = internalLoadFile(filename, 2, r, g, -1, -1);
		color_space = cspc;
		if (ret) (*ret) = result;
	}
	Image::Image(const std::string& filename, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Image::ColorSpace& cspc, bool *ret){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		bool result = internalLoadFile(filename, 3, r, g, b, -1);
		color_space = cspc;
		if (ret) (*ret) = result;
	}
	Image::Image(const std::string& filename, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const Image::ColorSpace& cspc, bool *ret){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		int a = static_cast<int>(to_a_channel);
		bool result = internalLoadFile(filename, 4, r, g, b, a);
		color_space = cspc;
		if (ret) (*ret) = result;
	}


	Image::Image(const Image& img, const Channel& to_r_channel){
		int r = static_cast<int>(to_r_channel);
		internalLoadImg(img, 1, r, -1, -1, -1);
		hdr_gamma = img.hdr_gamma;
		hdr_scale = img.hdr_scale;
		color_space = img.color_space;
	}
	Image::Image(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		internalLoadImg(img, 2, r, g, -1, -1);
		hdr_gamma = img.hdr_gamma;
		hdr_scale = img.hdr_scale;
		color_space = img.color_space;
	}
	Image::Image(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		internalLoadImg(img, 3, r, g, b, -1);
		hdr_gamma = img.hdr_gamma;
		hdr_scale = img.hdr_scale;
		color_space = img.color_space;
	}
	Image::Image(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		int a = static_cast<int>(to_a_channel);
		internalLoadImg(img, 4, r, g, b, a);
		hdr_gamma = img.hdr_gamma;
		hdr_scale = img.hdr_scale;
		color_space = img.color_space;
	}


	Image::Image(const unsigned char* memory, unsigned int w, unsigned int h, unsigned int n, const Image::ColorSpace& cspc){
		assert(memory);
		internalLoadMemory(memory, w, h, n, -1, 0, 1, 2, 3);
		color_space = cspc;
	}
	Image::Image(const unsigned char* memory, unsigned int w, unsigned int h, unsigned int n, const Channel& to_r_channel, const Image::ColorSpace& cspc){
		assert(memory);
		int r = static_cast<int>(to_r_channel);
		internalLoadMemory(memory, w, h, n, 1, r, -1, -1, -1);
		color_space = cspc;
	}
	Image::Image(const unsigned char* memory, unsigned int w, unsigned int h, unsigned int n, const Channel& to_r_channel, const Channel& to_g_channel, const Image::ColorSpace& cspc){
		assert(memory);
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		internalLoadMemory(memory, w, h, n, 2, r, g, -1, -1);
		color_space = cspc;
	}
	Image::Image(const unsigned char* memory, unsigned int w, unsigned int h, unsigned int n, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Image::ColorSpace& cspc){
		assert(memory);
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		internalLoadMemory(memory, w, h, n, 3, r, g, b, -1);
		color_space = cspc;
	}
	Image::Image(const unsigned char* memory, unsigned int w, unsigned int h, unsigned int n, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const Image::ColorSpace& cspc){
		assert(memory);
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		int a = static_cast<int>(to_a_channel);
		internalLoadMemory(memory,w,h,n, 4, r, g, b, a);
		color_space = cspc;
	}


	Image::Image(const std::vector<unsigned char>& memory, unsigned int w, unsigned int h, unsigned int n, const ColorSpace& cspc){
		internalLoadMemory(&memory[0], w, h, n, -1, 0, 1, 2, 3);
		color_space = cspc;
	}
	Image::Image(const std::vector<unsigned char>& memory, unsigned int w, unsigned int h, unsigned int n, const Channel& to_r_channel, const ColorSpace& cspc){
		int r = static_cast<int>(to_r_channel);
		internalLoadMemory(&memory[0], w, h, n, 1, r, -1, -1, -1);
		color_space = cspc;
	}
	Image::Image(const std::vector<unsigned char>& memory, unsigned int w, unsigned int h, unsigned int n, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& cspc){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		internalLoadMemory(&memory[0], w, h, n, 2, r, g, -1, -1);
		color_space = cspc;
	}
	Image::Image(const std::vector<unsigned char>& memory, unsigned int w, unsigned int h, unsigned int n, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& cspc){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		internalLoadMemory(&memory[0], w, h, n, 3, r, g, b, -1);
		color_space = cspc;
	}
	Image::Image(const std::vector<unsigned char>& memory, unsigned int w, unsigned int h, unsigned int n, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& cspc){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		int a = static_cast<int>(to_a_channel);
		internalLoadMemory(&memory[0], w, h, n, 4, r, g, b, a);
		color_space = cspc;
	}

	


	Image::Image(const unsigned char** memory, const unsigned int *times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Image::ColorSpace& cspc){
		assert(memory && times);
		internalLoadMemory(memory, times, w, h, n, f,-1, 0, 1, 2, 3);
		color_space = cspc;
	}
	Image::Image(const unsigned char** memory, const unsigned int *times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Channel& to_r_channel, const Image::ColorSpace& cspc){
		assert(memory && times);
		int r = static_cast<int>(to_r_channel);
		internalLoadMemory(memory, times, w, h, n, f, 1, r, -1, -1, -1);
		color_space = cspc;
	}
	Image::Image(const unsigned char** memory, const unsigned int *times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Channel& to_r_channel, const Channel& to_g_channel, const Image::ColorSpace& cspc){
		assert(memory && times);
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		internalLoadMemory(memory, times, w, h, n, f, 2, r, g, -1, -1);
		color_space = cspc;
	}
	Image::Image(const unsigned char** memory, const unsigned int *times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Image::ColorSpace& cspc){
		assert(memory && times);
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		internalLoadMemory(memory, times, w, h, n, f, 3, r, g, b, -1);
		color_space = cspc;
	}
	Image::Image(const unsigned char** memory, const unsigned int *times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const Image::ColorSpace& cspc){
		assert(memory && times);
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		int a = static_cast<int>(to_a_channel);
		internalLoadMemory(memory, times, w, h, n, f, 4, r, g, b, a);
		color_space = cspc;
	}

	Image::Image(const std::vector<std::vector<unsigned char>>& memory, const std::vector<unsigned int>& times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Image::ColorSpace& cspc){
		internalLoadMemory(memory, times, w, h, n, f, -1, 0, 1, 2, 3);
		color_space = cspc;
	}
	Image::Image(const std::vector<std::vector<unsigned char>>& memory, const std::vector<unsigned int>& times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Channel& to_r_channel, const Image::ColorSpace& cspc){
		int r = static_cast<int>(to_r_channel);
		internalLoadMemory(memory, times, w, h, n, f, 1, r, -1, -1, -1);
		color_space = cspc;
	}
	Image::Image(const std::vector<std::vector<unsigned char>>& memory, const std::vector<unsigned int>& times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Channel& to_r_channel, const Channel& to_g_channel, const Image::ColorSpace& cspc){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		internalLoadMemory(memory, times, w, h, n, f, 2, r, g, -1, -1);
		color_space = cspc;
	}
	Image::Image(const std::vector<std::vector<unsigned char>>& memory, const std::vector<unsigned int>& times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Image::ColorSpace& cspc){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		internalLoadMemory(memory, times, w, h, n, f, 3, r, g, b, -1);
		color_space = cspc;
	}
	Image::Image(const std::vector<std::vector<unsigned char>>& memory, const std::vector<unsigned int>& times, unsigned int w, unsigned int h, unsigned int n, unsigned int f, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const Image::ColorSpace& cspc){
		int r = static_cast<int>(to_r_channel);
		int g = static_cast<int>(to_g_channel);
		int b = static_cast<int>(to_b_channel);
		int a = static_cast<int>(to_a_channel);
		internalLoadMemory(memory, times, w, h, n, f, 4, r, g, b, a);
		color_space = cspc;
	}
	Image::~Image(){
		//nothing here.
	}


	
	



	void Image::resize(unsigned int neww, unsigned int newh){
		std::vector<unsigned char> newdata; newdata.resize(neww*newh*num_channels);
		for (unsigned int i = 0; i < num_frames; i++){
			if (color_space==ColorSpace::SRGB){
				//srgb needs special filtering (data not in linear space)
				if(num_channels == 4) dep::stbir_resize_uint8_srgb(&data[i][0], width, height, width*num_channels, &newdata[0], neww, newh, neww*num_channels, num_channels, 3, 0);
				else dep::stbir_resize_uint8_srgb(&data[i][0], width, height, width*num_channels, &newdata[0], neww, newh, neww*num_channels, num_channels, STBIR_ALPHA_CHANNEL_NONE, 0);
			}
			else{
				//linear
				dep::stbir_resize_uint8(&data[i][0], width, height, width*num_channels, &newdata[0], neww, newh, neww*num_channels, num_channels);
			}
			data[i] = newdata;
		}
		width = neww;
		height = newh;
	}
	void Image::reset(unsigned int w, unsigned int h, unsigned int n, unsigned int f){
		data.resize(f);
		for (unsigned int i = 0; i < f; i++) data[i].resize(w*h*n);
		delay.resize(f);
		width = w;
		height = h;
		num_channels = n;
		num_frames = f;
		hdr_gamma = 2.2f;
		hdr_scale = 1.0f;
		total_delay = 0;
	}
	void Image::clear(){
		//clears data, does not modify structure
		for (unsigned int i = 0; i < data.size(); i++) std::fill(data[i].begin(), data[i].end(), 0);
		std::fill(delay.begin(), delay.end(), 0);
	}



	void Image::frameAdd(const unsigned char* memory, unsigned int d){
		assert(memory);
		num_frames++;
		data.push_back(std::vector<unsigned char>()); 
		data[num_frames - 1].resize(width*height*num_channels);
		std::memcpy(&data[num_frames - 1][0], memory, width*height*num_channels*sizeof(unsigned char));
		delay.push_back(d);
		total_delay += d;
	}
	void Image::frameAdd(const unsigned char* memory, unsigned int d, unsigned int frame){
		assert(memory && frame <num_frames);
		num_frames++;
		data.insert(data.begin()+frame, std::vector<unsigned char>());
		data[frame].resize(width*height*num_channels);
		std::memcpy(&data[frame][0], memory, width*height*num_channels*sizeof(unsigned char));
		delay.insert(delay.begin()+frame,d);
		total_delay += d;
	}
	void Image::frameAdd(const std::vector<unsigned char>& memory, unsigned int d){
		frameAdd(&memory[0], d);
	}
	void Image::frameAdd(const std::vector<unsigned char>& memory, unsigned int d, unsigned int frame){
		frameAdd(&memory[0], d, frame);
	}
	void Image::frameRemove(unsigned int frame){
		assert(frame < num_frames);
		data.erase(data.begin()+frame);
		total_delay -= delay[frame];
		delay.erase(delay.begin()+frame);
		num_frames--;
	}
	void Image::framesRemove(){
		data.clear();
		delay.clear();
		num_frames = 0;
		total_delay = 0;
	}
	void Image::framesReverse(){
		for (unsigned int i = 0; i < num_frames / 2; i++){
			std::iter_swap(data.begin() + i, data.end() - i - 1);
			std::iter_swap(delay.begin() + i, delay.end() - i - 1);
		}
	}
	void Image::framesSetSpeed(float factor) {
		if (factor < 0) {
			framesReverse();
			factor *= -1;
		}
		for (auto& d : delay) {
			d = (unsigned int) (d * factor / 100);
		}
	}



	void Image::channelSwitch(const Channel& channel1, const Channel& channel2){
		unsigned int c1 = static_cast<int>(channel1);
		unsigned int c2 = static_cast<int>(channel2);
		assert(c1 < num_channels && c2 < num_channels);
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++) for (unsigned int i = 0; i < width; i++){
				std::swap(data[f][(j*width + i)*num_channels + c1], data[f][(j*width + i)*num_channels + c2]);
			}
		}
	}

	void Image::channelInsert(const Channel& channel) {
		assert(channel == Channel::RED || channel == Channel::GREEN || channel == Channel::BLUE || channel == Channel::ALPHA);

		unsigned int ic = static_cast<int>(channel);
		// can't insert over 4 channels and can't insert beyond the last empty channel
		assert(num_channels <= 3 && ic<=num_channels);
		unsigned int new_num_channels = num_channels+1;

		//handle blank image case
		if (num_channels == 0) {
			num_frames = 1;
			data.resize(1);
		}

		for (unsigned int f = 0; f < num_frames; f++) {
			std::vector<unsigned char> new_frame_data; new_frame_data.resize(new_num_channels * width * height);
			
			//save data from previous channels
			if (num_channels > 0) {
				for (unsigned int j = 0; j < height; j++) for (unsigned int i = 0; i < width; i++) {
					for (unsigned int c = 0; c < new_num_channels; c++) {
						unsigned char& target = new_frame_data[(j*width + i)*new_num_channels + c];
						if (c < ic)  target = data[f][(j*width + i)*num_channels + c];	//np - p mapping
						else if (c == ic) target = 0;
						else target = data[f][(j*width + i)*num_channels + c - 1];		//np - (p-1) mapping
					}
				}
			}

			std::swap(data[f], new_frame_data);
		}
		num_channels = new_num_channels;
	}
	void Image::channelRemove(const Channel& channel) {
		assert(channel == Channel::RED || channel == Channel::GREEN || channel == Channel::BLUE || channel == Channel::ALPHA);
		unsigned int ic = static_cast<int>(channel);
		// can't remove from more than the number of channels
		assert(ic <= num_channels);
		unsigned int new_num_channels = num_channels-1;

		for (unsigned int f = 0; f < num_frames; f++) {
			std::vector<unsigned char> new_frame_data; new_frame_data.resize(new_num_channels * width * height);

			//save data from previous channels
			if (num_channels > 0) {
				for (unsigned int j = 0; j < height; j++) for (unsigned int i = 0; i < width; i++) {
					for (unsigned int c = 0; c < new_num_channels; c++) {
						unsigned char& target = new_frame_data[(j*width + i)*new_num_channels + c];
						if (c < ic)  target = data[f][(j*width + i)*num_channels + c];	//np - p mapping
						else target = data[f][(j*width + i)*num_channels + c + 1];		//np - (p+1) mapping
					}
				}
			}

			std::swap(data[f], new_frame_data);
		}
		num_channels = new_num_channels;
	}
	void Image::channelClone(const Channel& channel_src, const Channel& channel_dst) {
		assert(channel_src == Channel::RED || channel_src == Channel::GREEN || channel_src == Channel::BLUE || channel_src == Channel::ALPHA);
		assert(channel_dst == Channel::RED || channel_dst == Channel::GREEN || channel_dst == Channel::BLUE || channel_dst == Channel::ALPHA);

		unsigned int csrc = static_cast<int>(channel_src);
		unsigned int cdst = static_cast<int>(channel_dst);
		assert(csrc < num_channels && cdst < num_channels);
		for (unsigned int f = 0; f < num_frames; f++) {
			for (unsigned int j = 0; j < height; j++) for (unsigned int i = 0; i < width; i++) {
				data[f][(j*width + i)*num_channels + cdst] = data[f][(j*width + i)*num_channels + csrc];
			}
		}
	}
	void Image::channelColor(const Channel& channel, unsigned char color) {
		unsigned int chn = static_cast<int>(channel);
		if (chn < 3) { assert(chn < num_channels); }
		for (unsigned int f = 0; f < num_frames; f++) {
			for (unsigned int j = 0; j < height; j++) {
				for (unsigned int i = 0; i < width; i++) {
					if (chn < num_channels) data[f][(j*width + i)*num_channels + chn] = color;
					else if (channel == Channel::ALLNOA) {
						for (unsigned c = 0; c < num_channels; c++) if (c < 3) data[f][(j*width + i)*num_channels + c] = color;
					}
					else if (channel == Channel::ALL) {
						for (unsigned c = 0; c < num_channels; c++) data[f][(j*width + i)*num_channels + c] = color;
					}
				}
			}
		}
	}


	

	void Image::transformFlipX(){
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width / 2; i++){
					for (unsigned int c = 0; c < num_channels; c++){
						std::swap(data[f][(j*width + i)*num_channels + c], data[f][(j*width + (width-1-i))*num_channels + c]);
					}
				}
			}
		}
	}
	void Image::transformFlipY(){
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height/2; j++){
				for (unsigned int i = 0; i < width ; i++){
					for (unsigned int c = 0; c < num_channels; c++){
						std::swap(data[f][(j*width + i)*num_channels + c], data[f][((height-1-j)*width + i)*num_channels + c]);
					}
				}
			}
		}
	}
	void Image::transformFlipXY(){
		transformFlipX();
		transformFlipY();
	}



	float Image::internalRGBtoSRGB(unsigned char ch){
		//fast but inaccurate
		return pow( (float)ch / 255.f, 0.45f) *255.f;
	}
	float Image::internalSRGBtoRGB(unsigned char ch){
		//fast but inaccurate
		return pow( (float)ch/255.f, 2.2f) *255.f;
	}


	/// if(only r)	-> gray = r\n
	/// if(r,g)		-> gray = r*0.2126 + g*0.7152\n
	/// if(r,g,b)	-> gray = r*0.2126 + g*0.6152 + b*0.0722\n
	void Image::transformGrayscale(){
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					unsigned char gray = 0;
					if (num_channels == 1) {} //r=r
					if (num_channels == 2) {
						if (color_space == ColorSpace::SRGB){
							float r = internalSRGBtoRGB(data[f][(j*width + i)*num_channels + 0]);
							float g = internalSRGBtoRGB(data[f][(j*width + i)*num_channels + 1]);
							gray = (unsigned char)internalRGBtoSRGB(r*0.2126f + g*0.7152f);
						}
						else{
							float r = (float)data[f][(j*width + i)*num_channels + 0];
							float g = (float)data[f][(j*width + i)*num_channels + 1];
							gray = (unsigned char)(r*0.2126f + g*0.7152f);
						}
					}
					if (num_channels >=3) {
						if (color_space==ColorSpace::SRGB){
							float r = internalSRGBtoRGB(data[f][(j*width + i)*num_channels + 0]);
							float g = internalSRGBtoRGB(data[f][(j*width + i)*num_channels + 1]);
							float b = internalSRGBtoRGB(data[f][(j*width + i)*num_channels + 2]);
							gray = (unsigned char)internalRGBtoSRGB(r*0.2126f + g*0.7152f + b*0.0722f);
						}
						else{
							float r = (float)data[f][(j*width + i)*num_channels + 0];
							float g = (float)data[f][(j*width + i)*num_channels + 1];
							float b = (float)data[f][(j*width + i)*num_channels + 2];
							gray = (unsigned char)(r*0.2126f + g*0.7152f + b*0.0722f);
						}
					}
					for (unsigned int c = 0; c< num_channels; c++) data[f][(j*width + i)*num_channels + c] = gray;
				}
			}
		}
	}
	void Image::channelWhite(const Channel& channel){
		unsigned int chn = static_cast<int>(channel);
		if (chn < 3) { assert(chn < num_channels); }
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					if (chn < num_channels) data[f][(j*width + i)*num_channels + chn] = 255;
					else if (channel == Channel::ALLNOA){
						for (unsigned c = 0; c < num_channels; c++) if (c < 3) data[f][(j*width + i)*num_channels + c] = 255;
					}
					else if (channel == Channel::ALL){
						for (unsigned c = 0; c < num_channels; c++) data[f][(j*width + i)*num_channels + c] = 255;
					}
				}
			}
		}
	}
	void Image::channelBlack(const Channel& channel){
		unsigned int chn = static_cast<int>(channel);
		if (chn < 3) { assert(chn < num_channels); }
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					if (chn < num_channels) data[f][(j*width + i)*num_channels + chn] = 0;
					else if (channel == Channel::ALLNOA){
						for (unsigned c = 0; c < num_channels; c++) if (c < 3) data[f][(j*width + i)*num_channels + c] = 0;
					}
					else if (channel == Channel::ALL){
						for (unsigned c = 0; c < num_channels; c++) data[f][(j*width + i)*num_channels + c] = 0;
					}
				}
			}
		}
	}
	void Image::channelMin(const Channel& channel){
		unsigned int chn = static_cast<int>(channel);
		if (chn < 3) { assert(chn < num_channels); }
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					unsigned char minval = 255; //does not consider alpha
					for (unsigned c = 0; c < num_channels; c++) if (c<3 && data[f][(j*width + i)*num_channels + c] < minval) minval = data[f][(j*width + i)*num_channels + c];

					if (chn < num_channels) data[f][(j*width + i)*num_channels + chn] = minval;
					else if (channel == Channel::ALLNOA){
						for (unsigned c = 0; c < num_channels; c++) if (c < 3) data[f][(j*width + i)*num_channels + c] = minval;
					}
					else if (channel == Channel::ALL){
						for (unsigned c = 0; c < num_channels; c++) data[f][(j*width + i)*num_channels + c] = minval;
					}
				}
			}
		}
	}
	void Image::channelMax(const Channel& channel){
		unsigned int chn = static_cast<int>(channel);
		if (chn < 3) { assert(chn < num_channels); }
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					unsigned char maxval = 0; //does not consider alpha
					for (unsigned c = 0; c < num_channels; c++) if (c<3 && data[f][(j*width + i)*num_channels + c] > maxval) maxval = data[f][(j*width + i)*num_channels + c];

					if (chn < num_channels) data[f][(j*width + i)*num_channels + chn] = maxval;
					else if (channel == Channel::ALLNOA){
						for (unsigned c = 0; c < num_channels; c++) if (c < 3) data[f][(j*width + i)*num_channels + c] = maxval;
					}
					else if (channel == Channel::ALL){
						for (unsigned c = 0; c < num_channels; c++) data[f][(j*width + i)*num_channels + c] = maxval;
					}
				}
			}
		}
	}
	void Image::channelInvert(const Channel& channel){
		unsigned int chn = static_cast<int>(channel);
		if (chn < 3) { assert(chn < num_channels); }
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					if (chn < num_channels) data[f][(j*width + i)*num_channels + chn] = 255 - data[f][(j*width + i)*num_channels + chn];
					else if (channel == Channel::ALLNOA){
						for (unsigned c = 0; c < num_channels; c++) if (c < 3) data[f][(j*width + i)*num_channels + c] = 255 - data[f][(j*width + i)*num_channels + c];
					}
					else if (channel == Channel::ALL){
						for (unsigned c = 0; c < num_channels; c++)	data[f][(j*width + i)*num_channels + c] = 255 - data[f][(j*width + i)*num_channels + c];
					}
				}
			}
		}
	}

	void Image::transformRGBtoRBG(){
		assert(num_channels >= 3);
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					std::swap(data[f][(j*width + i)*num_channels + 1], data[f][(j*width + i)*num_channels + 2]);
				}
			}
		}
	}
	void Image::transformRGBtoBGR(){
		assert(num_channels >= 3);
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					std::swap(data[f][(j*width + i)*num_channels + 0], data[f][(j*width + i)*num_channels + 2]);
				}
			}
		}
	}
	void Image::transformRGBtoBRG(){
		assert(num_channels >= 3);
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					unsigned char r = data[f][(j*width + i)*num_channels + 0];
					unsigned char g = data[f][(j*width + i)*num_channels + 1];
					unsigned char b = data[f][(j*width + i)*num_channels + 2];
					data[f][(j*width + i)*num_channels + 0] = g;
					data[f][(j*width + i)*num_channels + 1] = b;
					data[f][(j*width + i)*num_channels + 2] = r;
				}
			}
		}
	}
	void Image::transformRGBtoGRB(){
		assert(num_channels >= 3);
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					std::swap(data[f][(j*width + i)*num_channels + 0], data[f][(j*width + i)*num_channels + 1]);
				}
			}
		}
	}
	void Image::transformRGBtoGBR(){
		assert(num_channels >= 3);
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					unsigned char r = data[f][(j*width + i)*num_channels + 0];
					unsigned char g = data[f][(j*width + i)*num_channels + 1];
					unsigned char b = data[f][(j*width + i)*num_channels + 2];
					data[f][(j*width + i)*num_channels + 0] = b;
					data[f][(j*width + i)*num_channels + 1] = r;
					data[f][(j*width + i)*num_channels + 2] = g;
				}
			}
		}
	}


	/// https://en.wikipedia.org/wiki/SRGB
	void Image::transformToSRGB(){
		assert(num_channels >= 3 && color_space == ColorSpace::RGB);
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					data[f][(j*width + i)*num_channels + 0] = (unsigned char)internalRGBtoSRGB(data[f][(j*width + i)*num_channels + 0]);
					data[f][(j*width + i)*num_channels + 1] = (unsigned char)internalRGBtoSRGB(data[f][(j*width + i)*num_channels + 1]);
					data[f][(j*width + i)*num_channels + 2] = (unsigned char)internalRGBtoSRGB(data[f][(j*width + i)*num_channels + 2]);
				}
			}
		}
		color_space = ColorSpace::SRGB;
	}
	/// https://en.wikipedia.org/wiki/RGB_color_space
	void Image::transformToRGB(){
		assert(num_channels >= 3 && color_space == ColorSpace::SRGB);
		for (unsigned int f = 0; f < num_frames; f++){
			for (unsigned int j = 0; j < height; j++){
				for (unsigned int i = 0; i < width; i++){
					data[f][(j*width + i)*num_channels + 0] = (unsigned char)internalSRGBtoRGB(data[f][(j*width + i)*num_channels + 0]);
					data[f][(j*width + i)*num_channels + 1] = (unsigned char)internalSRGBtoRGB(data[f][(j*width + i)*num_channels + 1]);
					data[f][(j*width + i)*num_channels + 2] = (unsigned char)internalSRGBtoRGB(data[f][(j*width + i)*num_channels + 2]);
				}
			}
		}
		color_space = ColorSpace::RGB;
	}
	

	bool Image::internalSave(const std::string& filename, int frame) const{
		//sanity check
		assert(num_channels > 0);
		if (frame > -1) assert(frame < (int)num_frames);

		//get extension in lowercase
		std::string ext = filename.substr(filename.rfind('.'));
		std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

		//if gif
		if (ext == ".gif"){
			dep::jo_gif_t gif = dep::jo_gif_start(filename.c_str(), width, height, 1, 16777215); //true color 2^24-1
			if (!gif.fp) return false;

			std::vector<unsigned char> temp;
			if (num_channels < 4) temp.resize(width * height * 4, 0);

			if (frame == -1){
				for (unsigned int i = 0; i < num_frames; i++){
					if (num_channels < 4){
						// saving only from 4 channel format => translating from x channel format to 4 channel format
						for (unsigned int k = 0; k < width*height; k++){
												temp[k * 4]		= data[i][k*num_channels];
							if (num_channels>1)	temp[k * 4 + 1] = data[i][k*num_channels + 1];
							if (num_channels>2)	temp[k * 4 + 2] = data[i][k*num_channels + 2];
							if (num_channels>3)	temp[k * 4 + 3] = data[i][k*num_channels + 3];
						}
						jo_gif_frame(&gif, &temp[0], (short)delay[i], false);
					}
					else{
						// direct save from 4 channel format, bad api.
						jo_gif_frame(&gif, &(data[i][0]), (short)delay[i], false);
					}
				}
			}
			else{
				if (num_channels < 4){
					// saving only from 4 channel format => translating from x channel format to 4 channel format
					for (unsigned int k = 0; k < width*height; k++){
											temp[k * 4]		= data[frame][k*num_channels];
						if (num_channels>1)	temp[k * 4 + 1] = data[frame][k*num_channels + 1];
						if (num_channels>2)	temp[k * 4 + 2] = data[frame][k*num_channels + 2];
						if (num_channels>3)	temp[k * 4 + 3] = data[frame][k*num_channels + 3];
					}
					jo_gif_frame(&gif, &temp[0], (short)delay[frame], false);
				}
				else{
					// direct save from 4 channel format, bad api.
					jo_gif_frame(&gif, &(data[frame][0]), (short)delay[frame], false);
				}
			}

			jo_gif_end(&gif);

			return true;
		}
		
		//neither format supports RG images and BMP doesn't support grayscale images
		int srcframe = 0;
		if (frame > -1) srcframe = frame;
		unsigned int src_num_channels = num_channels;
		const unsigned char* src_data = data[srcframe].data();
		std::vector<unsigned char> temp;
		if (num_channels ==2 || (ext==".bmp" && (num_channels==1 || num_channels==4))){
			temp.resize(width*height * 3, 0);

			for (unsigned int k = 0; k < width*height; k++){
				temp[k * 3] = data[srcframe][k*num_channels];
				if (num_channels>1) temp[k * 3 + 1] = data[srcframe][k*num_channels + 1];
				if (num_channels>2) temp[k * 3 + 2] = data[srcframe][k*num_channels + 2];
			}
			src_num_channels = 3;
			src_data = temp.data();
		}

		int result = 0;
		if (ext == ".png")		result = dep::stbi_write_png(filename.c_str(), width, height, src_num_channels, src_data, width*src_num_channels);
		else if (ext == ".bmp")	result = dep::stbi_write_bmp(filename.c_str(), width, height, src_num_channels, src_data);
		else if (ext == ".tga") result = dep::stbi_write_tga(filename.c_str(), width, height, src_num_channels, src_data);
		else{ assert(false); }	//format unsupported

		return result;
	}
	bool Image::internalSaveChannel(const std::string& filename, const Channel& channel, int frame) const{
		//sanity check
		assert(num_channels > 0);
		if (frame > -1) assert(frame < (int)num_frames);

		//all is valid
		if (channel == Channel::ALL) return internalSave(filename, frame);

		//allnoa
		if (channel == Channel::ALLNOA){
			if (num_channels == 1){
				Image img((*this), Channel::RED);
				return img.internalSave(filename, frame);
			}
			else if (num_channels == 2){
				Image img((*this), Channel::RED, Channel::GREEN);
				return img.internalSave(filename, frame);
			}
			else if (num_channels == 3 || num_channels ==4){
				Image img((*this), Channel::RED, Channel::GREEN, Channel::BLUE);
				return img.internalSave(filename, frame);
			}
		}

		//RED, GREEN, BLUE, ALPHA
		unsigned int chn = static_cast<unsigned int>(channel);
		assert(chn < num_channels);

		//get extension in lowercase
		std::string ext = filename.substr(filename.rfind('.'));
		std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

		//if gif
		if (ext == ".gif"){
			dep::jo_gif_t gif = dep::jo_gif_start(filename.c_str(), width, height, 1, 16777215); //true color 2^24-1
			if (!gif.fp) return false;
			std::vector<unsigned char> temp;
			temp.resize(width*height * 4, 0);

			if (frame == -1){
				for (unsigned int i = 0; i < num_frames; i++){
					for (unsigned int k = 0; k < width*height; k++)	temp[k * 4] = data[i][k*num_channels + chn];
					jo_gif_frame(&gif, &temp[0], (short)delay[i], false);
				}
			}
			else{
				for (unsigned int k = 0; k < width*height; k++)	temp[k * 4] = data[frame][k*num_channels + chn];
				jo_gif_frame(&gif, &temp[0], (short)delay[frame], false);
			}

			dep::jo_gif_end(&gif);
			return true;
		}
		else{
			int srcframe = 0;
			if (frame == -1)srcframe = 0;
			else srcframe = frame;

			int result=0;
			std::vector<unsigned char> temp;
			if (ext == ".bmp"){
				temp.resize(width*height * 3, 0);
				for (unsigned int k = 0; k < width*height; k++)	temp[k * 3] = data[srcframe][k*num_channels + chn];
			}
			else{
				temp.resize(width*height, 0);
				for (unsigned int k = 0; k < width*height; k++)	temp[k] = data[srcframe][k*num_channels + chn];
			}

			if (ext == ".png") result = dep::stbi_write_png(filename.c_str(), width, height, 1, &temp[0], width);
			else if (ext == ".bmp") result = dep::stbi_write_bmp(filename.c_str(), width, height, 3, &temp[0]);
			else if (ext == ".tga") result = dep::stbi_write_tga(filename.c_str(), width, height, 1, &temp[0]);
			else{ assert(false); }//extension not supported

			return result;
		}
		//format not supported
		return false;
	}

	bool Image::save(const std::string& filename) const {
		return internalSave(filename, -1);
	}
	bool Image::saveFrame(const std::string& filename, unsigned int frame) const{
		return internalSave(filename, frame);
	}
	bool Image::saveChannel(const std::string& filename, const Channel& channel) const{
		return internalSaveChannel(filename, channel, -1);
	}
	bool Image::saveFrameChannel(const std::string& filename, const Channel& channel, unsigned int frame) const{
		return internalSaveChannel(filename, channel, frame);
	}

	


	void Image::setHDRgamma(float gamma){
		hdr_gamma = gamma;
		dep::stbi_hdr_to_ldr_gamma(gamma);
	}
	void Image::setHDRscale(float scale){
		hdr_scale = scale;
		dep::stbi_hdr_to_ldr_scale(scale);
	}
	float Image::getHDRgamma() const{
		return hdr_gamma;
	}
	float Image::getHDRscale() const{
		return hdr_scale;
	}


	
	unsigned int Image::getWidth() const{
		return width;
	}
	unsigned int Image::getHeight() const{
		return height;
	}
	unsigned int Image::getNumChannels() const{
		return num_channels;
	}
	unsigned char* Image::getData(){
		return &(data[0][0]);
	}
	std::vector<unsigned char>& Image::getDataVec(){
		return data[0];
	}
	const unsigned char* Image::getData() const{
		return const_cast<const unsigned char*>(&(data[0][0]));
	}
	const std::vector<unsigned char>& Image::getDataVec() const{
		return data[0];
	}
	unsigned char* Image::getFrameData(unsigned int frame_num){
		assert(frame_num < num_frames);
		return &(data[frame_num][0]);
	}
	const unsigned char* Image::getFrameData(unsigned int frame_num) const{
		assert(frame_num < num_frames);
		return const_cast<const unsigned char*>(&(data[frame_num][0]));
	}
	std::vector<unsigned char>& Image::getFrameDataVec(unsigned int frame_num){
		assert(frame_num < num_frames);
		return data[frame_num];
	}
	const std::vector<unsigned char>& Image::getFrameDataVec(unsigned int frame_num) const{
		assert(frame_num < num_frames);
		return data[frame_num];
	}
	unsigned int Image::getFrameDelay(unsigned int frame_num) const{
		assert(frame_num < num_frames);
		return delay[frame_num];
	}
	void Image::setFrameDelay(unsigned int frame_num, unsigned int d){
		assert(frame_num < num_frames);
		this->delay[frame_num] = d;
	}
	unsigned int* Image::getDelays(){
		return &(delay[0]);
	}
	const unsigned int* Image::getDelays() const{
		return const_cast<const unsigned int*>(&(delay[0]));
	}
	std::vector<unsigned int>& Image::getDelaysVec(){
		return delay;
	}
	const std::vector<unsigned int>& Image::getDelaysVec() const{
		return delay;
	}
	unsigned int Image::getTotalFrameDelay() const{
		return total_delay;
	}
	unsigned int Image::getDataSize() const{
		return num_frames * width * height * num_channels;
	}
	unsigned int Image::getMemory() const {
		return num_frames * width * height * num_channels;
	}
	unsigned int Image::getNumFrames() const{
		return num_frames;
	}
	Image::ColorSpace Image::getColorSpace() const{
		return color_space;
	}


		
	unsigned char& Image::operator()(unsigned int idx){
		assert(idx < width*height*num_channels);
		return data[0][idx];
	}
	const unsigned char& Image::operator()(unsigned int idx) const{
		assert(idx < width*height*num_channels);
		return data[0][idx];
	}
	unsigned char& Image::operator()(unsigned int x, unsigned int y, unsigned int c){
		assert(x < width && y < height && c<num_channels);
		return data[0][(y*width + x)*num_channels+c];
	}
	const unsigned char& Image::operator()(unsigned int x, unsigned int y, unsigned int c) const{
		assert(x < width && y < height && c<num_channels);
		return data[0][(y*width + x)*num_channels+c];
	}
	unsigned char& Image::operator()(unsigned int x, unsigned int y, unsigned int c, unsigned int frame){
		assert(x < width && y < height && c<num_channels && frame<num_frames);
		return data[frame][(y*width + x)*num_channels+c];
	}
	const unsigned char& Image::operator()(unsigned int x, unsigned int y, unsigned int c, unsigned int frame) const{
		assert(x < width && y < height && c<num_channels && frame<num_frames);
		return data[frame][(y*width + x)*num_channels+c];
	}
}
#if defined(_MSC_VER)
#pragma warning(pop)
#endif
