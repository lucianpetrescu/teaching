///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------
 

#include "framebuffer.h"
#include <cassert>
 
namespace lab {
	
	Framebuffer::Framebuffer() {
		glCreateFramebuffers(1, &fbo);
		//get max possible attachments
		GLint maxattachments;
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxattachments);
		for (int i = 0; i < maxattachments; i++) attachments_color[i] = nullptr;
	}
	Framebuffer::~Framebuffer() {
		if(fbo!=0) glDeleteFramebuffers(1, &fbo);
	}

	//allow move operators
	Framebuffer::Framebuffer(Framebuffer&& rhs) {
		assert(this != &rhs);	///user error
		fbo = rhs.fbo;
		attachments_color = std::move(rhs.attachments_color);
		attachment_depth = rhs.attachment_depth;
		attachment_stencil = rhs.attachment_stencil;
		complete_status = rhs.complete_status;
		bind_state = rhs.bind_state;
		width = rhs.width;
		height = rhs.height;

		rhs.fbo = 0;
		rhs.attachment_depth = nullptr;
		rhs.attachment_stencil = nullptr;
		rhs.complete_status = false;
		rhs.bind_state = false;
		rhs.width = 0;
		rhs.height = 0;
	}
	Framebuffer& Framebuffer::operator=(Framebuffer&& rhs) {
		assert(this != &rhs);	///user error
		fbo = rhs.fbo;
		attachments_color = std::move(rhs.attachments_color);
		attachment_depth = rhs.attachment_depth;
		attachment_stencil = rhs.attachment_stencil;
		complete_status = rhs.complete_status;
		bind_state = rhs.bind_state;
		width = rhs.width;
		height = rhs.height;

		rhs.fbo = 0;
		rhs.attachment_depth = nullptr;
		rhs.attachment_stencil = nullptr;
		rhs.complete_status = false;
		rhs.bind_state = false;
		rhs.width = 0;
		rhs.height = 0;

		return (*this);
	}

	//clone
	Framebuffer Framebuffer::clone() const {
		assert(fbo != 0);
		Framebuffer result;
		result.width = width;
		result.height = height;
		result.bind_state = false;
		result.complete_status = false;
		for (auto& a : attachments_color) result.setAttachmentColor(a.second, a.first);
		result.setAttachmentDepth(attachment_depth);
		result.setAttachmentStencil(attachment_stencil);
		return result;//nrvo
	}

	//resize all attached textures but lose ALL data (creates NO mipmaps)
	void Framebuffer::resize(unsigned int in_width, unsigned int in_height) {
		assert(fbo != 0);
		width = in_width;
		height = in_height;
		
		//save previous
		std::map<int, Texture2D*> old_attachment_color = std::move(attachments_color);
		Texture2D* old_attachment_depth = attachment_depth;
		Texture2D* old_attachment_stencil = attachment_stencil;

		//resize
		for (auto& a : old_attachment_color) if(a.second) a.second->resize(width, height, false);
		if(old_attachment_depth) old_attachment_depth->resize(width, height, false);
		if (old_attachment_stencil && old_attachment_stencil != old_attachment_depth) old_attachment_stencil->resize(width, height);

		//update status
		internalCheck();
	}

	//bind and unbind
	void Framebuffer::bind() {
		assert(fbo != 0);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		bind_state = true;
	}
	void Framebuffer::unbind() {
		assert(fbo != 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		bind_state = false;
	}
	bool Framebuffer::getBindState() const {
		assert(fbo != 0);
		return bind_state;
	}

	//attachments
	void Framebuffer::setAttachmentColor(Texture2D* texture, unsigned int index) {
		assert(fbo != 0);
		if (texture) {
			glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT0 + index, texture->getGLobject(), 0);
			width = texture->getWidth();
			height = texture->getHeight();
		}
		else glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT0 + index, 0, 0);
		attachments_color[index] = texture;

		//update draw buffers
		std::vector<GLenum> drawbuffers;
		for (auto& c : attachments_color) if (c.second) drawbuffers.push_back(GL_COLOR_ATTACHMENT0 + c.first);
		if (drawbuffers.size() > 0) glNamedFramebufferDrawBuffers(fbo, (unsigned int)drawbuffers.size(), &drawbuffers[0]);

		//update status
		internalCheck();
	}
	void Framebuffer::setAttachmentDepth(Texture2D* texture) {
		assert(fbo != 0);
		if (texture) {
			glNamedFramebufferTexture(fbo, GL_DEPTH_ATTACHMENT, texture->getGLobject(), 0);
			width = texture->getWidth();
			height = texture->getHeight();
		}
		else glNamedFramebufferTexture(fbo, GL_DEPTH_ATTACHMENT, 0, 0);
		attachment_depth = texture;

		//update status
		internalCheck();
	}
	void Framebuffer::setAttachmentStencil(Texture2D* texture) {
		assert(fbo != 0);
		if (texture) {
			glNamedFramebufferTexture(fbo, GL_STENCIL_ATTACHMENT, texture->getGLobject(), 0);
			width = texture->getWidth();
			height = texture->getHeight();
		}
		else glNamedFramebufferTexture(fbo, GL_STENCIL_ATTACHMENT, 0, 0);
		attachment_stencil = texture;

		//update status
		internalCheck();
	}
	void Framebuffer::setAttachmentDepthStencil(Texture2D* texture) {
		assert(fbo != 0);
		if (texture) {
			glNamedFramebufferTexture(fbo, GL_DEPTH_STENCIL_ATTACHMENT, texture->getGLobject(), 0);
			width = texture->getWidth();
			height = texture->getHeight();
		}
		else glNamedFramebufferTexture(fbo, GL_DEPTH_STENCIL_ATTACHMENT, 0, 0);
		attachment_depth = attachment_stencil = texture;

		//update status
		internalCheck();
	}
	unsigned int Framebuffer::getAttachmentColorMax() const {
		return (unsigned int)attachments_color.size();
	}
	Texture2D* Framebuffer::getAttachmentColor(unsigned int index) const {
		assert(index < attachments_color.size());
		return attachments_color.at(index);
	}
	Texture2D* Framebuffer::getAttachmentDepth() const {
		return attachment_depth;
	}
	Texture2D* Framebuffer::getAttachmentStencil() const {
		return attachment_stencil;
	}
	bool Framebuffer::getCompleteStatus() const {
		assert(fbo != 0);
		return complete_status;
	}

	//basics
	unsigned int Framebuffer::getWidth() const {
		return width;
	}
	unsigned int Framebuffer::getHeight() const {
		return height;
	}
	GLuint Framebuffer::getGLobject() const {
		return fbo;
	}

	void Framebuffer::internalCheck() {
		//verify state
		GLenum result = glCheckNamedFramebufferStatus(fbo, GL_FRAMEBUFFER);
		if (result != GL_FRAMEBUFFER_COMPLETE) complete_status = false;
		else complete_status = true;
	}
}