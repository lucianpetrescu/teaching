###Screenshots###

Framebuffers are useful for storing visual output in a specified format.

In this lab, the same scene is rendered from multiple viewpoints, each of the visual results is stored in a different framebuffer, and in the end all the resulting images are displayed together.

![](img/output.png)
![](img/output_2.png)

A single parameter controls the number of views.
