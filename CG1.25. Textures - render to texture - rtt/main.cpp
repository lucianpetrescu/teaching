///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/rotate_vector.hpp"
#include "../dependencies/glm/gtx/transform.hpp"
#include "../dependencies/glm/gtx/transform2.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"
#include <ctime>
#include "shader.h"
#include "camera.h"
#include "mesh.h"
#include "texture.h"
#include "framebuffer.h"

const std::string ASSET_PATH = "../../assets/";

namespace lab {
	class CGLab {
		//original framebuffer
		unsigned int framebuffer_width = 0, framebuffer_height = 0;

		//scene
		Shader scene_shader;
		std::vector<Mesh> scene_meshes;
		Buffer scene_ubo{ GL_DYNAMIC_STORAGE_BIT };
		struct SceneUboData {
			glm::mat4 model_matrix[4] = {
				glm::rotate(glm::translate(glm::mat4(1), glm::vec3(-60, 0, 0)), 60.0f, glm::vec3(0, 1, 0)),
				glm::mat4(1),
				glm::translate(glm::mat4(1), glm::vec3(40, 0, 0)),
				glm::translate(glm::mat4(1), glm::vec3(0, -20, 0))
			};
			glm::vec4 material_constants[4] = {
				glm::vec4(0.05f, 0.48f, 0.47f, 16.0f),
				glm::vec4(0.05f, 0.85f, 0.1f, 2.0f),
				glm::vec4(0.05f, 0.1f, 0.85f, 64.0f),
				glm::vec4(0.05f, 0.85f, 0.1f, 1.0f)
			};
			glm::vec4 material_color[4] = {
				glm::vec4(1, 0, 0, 0),
				glm::vec4(0, 1, 0, 0),
				glm::vec4(0, 0, 1, 0),
				glm::vec4(0.6, 0.6, 0.6, 0)
			};
			glm::vec4 light_direction = glm::vec4(0, -1, 0, 0);
			glm::vec4 light_angles = glm::vec4(glm::radians(4.0f), glm::radians(7.0f), 0, 0);
			glm::vec4 light_color = glm::vec4(1, 1, 1, 0);
			glm::vec4 light_attenuation = glm::vec4(1, 0.0001f, 0.000002f, 0);
			glm::mat4 view_matrix, projection_matrix;
			glm::vec4 light_position = glm::vec4(-150, 220, 0, 0);
			glm::vec4 light_falloff = glm::vec4(0, 0, 0, 0);
			glm::vec4 camera_position;
		}scene_ubo_data;

		//render to texture
		Texture2D rtt_texture_depth;
		std::vector<Texture2D> rtt_textures_color;
		std::vector<Framebuffer> rtt_framebuffers;
		const unsigned int rtt_num_framebuffers = 3;
		unsigned int rtt_w = 0;
		unsigned int rtt_h = 0;

		//fullscreen
		Mesh fullscreen_mesh;
		Shader fullscreen_shader;
		TextureSampler fullscreen_texture_sampler;

		
	public:
		CGLab(unsigned int width, unsigned int height) {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;
			//window
			framebuffer_width = width;
			framebuffer_height = height;

			//state
			glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
			glEnable(GL_DEPTH_TEST);

			//scene
			scene_shader.load("../shader/scene_rtt.vert", "../shader/scene_rtt.frag");
			scene_meshes.resize(4);
			scene_meshes[0].load(ASSET_PATH + "models/standard/dragon.obj");
			scene_meshes[1].load(ASSET_PATH + "models/standard/bunny.obj");
			scene_meshes[2].load(ASSET_PATH + "models/standard/sphere.obj");
			scene_meshes[3].load(ASSET_PATH + "models/standard/ground.obj");
			scene_ubo.resize(sizeof(SceneUboData));
			
			//render to texture targets
			rtt_w = framebuffer_width;
			rtt_h = framebuffer_height;
			rtt_texture_depth.create(GL_DEPTH_COMPONENT32, rtt_w, rtt_h, false);
			rtt_textures_color.resize(rtt_num_framebuffers * rtt_num_framebuffers);
			for (unsigned int i = 0; i < rtt_num_framebuffers; i++){
				for (unsigned int j = 0; j < rtt_num_framebuffers; j++) {
					Texture2D* tex = &rtt_textures_color[i * rtt_num_framebuffers + j];
					tex->create(GL_RGB8, rtt_w, rtt_h, false);
					rtt_framebuffers.emplace_back();
					rtt_framebuffers.back().setAttachmentDepth(&rtt_texture_depth);
					rtt_framebuffers.back().setAttachmentColor(tex, 0);
				}
			}

			// fullscreen
			fullscreen_texture_sampler.setBilinear();
			fullscreen_texture_sampler.bind(0);
			fullscreen_shader.load("../shader/fullscreen_textured.vert", "../shader/fullscreen_textured.frag");
			std::vector<lab::Vertex> vertices;
			std::vector<unsigned int> indices;
			for (unsigned int i = 0; i < rtt_num_framebuffers; i++) {
				for (unsigned int j = 0; j < rtt_num_framebuffers; j++) {
					float div = 2.0f / (float)rtt_num_framebuffers;
					vertices.emplace_back(glm::vec3(div*i -1		, div*j -1, 0)		, glm::vec3(0, 0, 1), glm::vec2(0, 0));
					vertices.emplace_back(glm::vec3(div*(i + 1) -1	, div*j -1, 0)		, glm::vec3(0, 0, 1), glm::vec2(1, 0));
					vertices.emplace_back(glm::vec3(div*(i + 1) -1	, div*(j + 1) -1, 0), glm::vec3(0, 0, 1), glm::vec2(1, 1));
					vertices.emplace_back(glm::vec3(div*i -1		, div*(j + 1) -1, 0), glm::vec3(0, 0, 1), glm::vec2(0, 1));
					unsigned int vsize = (unsigned int)vertices.size();
					indices.push_back(vsize - 4);
					indices.push_back(vsize - 3);
					indices.push_back(vsize - 2);
					indices.push_back(vsize - 2);
					indices.push_back(vsize - 1);
					indices.push_back(vsize - 4);
				}
			}
			fullscreen_mesh.load(vertices, indices);
		}
		~CGLab() {
			//raii
			std::cout << "[LAB] Destroyed" << std::endl << "----------------------------------------------" << std::endl;
		}
		void renderScene(const glm::vec3& camera_position, const glm::vec3& camera_center, const glm::vec3& camera_up) {
			glViewport(0, 0, rtt_w, rtt_h);
			glEnable(GL_DEPTH_TEST);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

			//update the ubo data
			scene_ubo_data.view_matrix = glm::lookAt(camera_position, camera_center, camera_up);
			scene_ubo_data.projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);
			scene_ubo_data.camera_position = glm::vec4(camera_position, 1);

			//update the dynamic ubo object and bind it to the pipeline
			scene_ubo.update(&scene_ubo_data, 0, sizeof(SceneUboData));
			scene_ubo.bindIndexed(GL_UNIFORM_BUFFER, 0);

			//render meshes
			scene_shader.bind();
			for (int i = 0; i < 4; i++) {
				scene_shader.setUniform("id", i);
				scene_meshes[i].draw(GL_TRIANGLES);
			}
		}
		void render() {
			//update light
			static int delta = 0;
			delta = (delta + 1) % 600;
			scene_ubo_data.light_position = glm::vec4(glm::vec3(-150, 320, 0) + glm::vec3((delta <= 300) ? delta : (600 - delta), 0, 0), 1);

			//render to textures
			for (unsigned int i = 0; i < rtt_num_framebuffers; i++) {
				for (unsigned int j = 0; j < rtt_num_framebuffers; j++) {
					unsigned int index = i * rtt_num_framebuffers + j;
					rtt_framebuffers[index].bind();
					float u = 2 * 3.14f / ((i*rtt_num_framebuffers + j + 1.0f) / (float)rtt_num_framebuffers * rtt_num_framebuffers);
					glm::vec3 camera_position = glm::vec3(sin(u)*50, 20, cos(u)*50);
					renderScene(camera_position, glm::vec3(0, 20, 0), glm::vec3(0, 1, 0));
					rtt_framebuffers[index].unbind();
				}
			}

			//render to fullscreen, we need to recreate the entire rendering state (viewport + transformations (not used here as all are one) )
			glViewport(0, 0, framebuffer_width, framebuffer_height);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			fullscreen_shader.bind();
			for (unsigned int i = 0; i < rtt_num_framebuffers; i++) {
				for (unsigned int j = 0; j < rtt_num_framebuffers; j++) {
					unsigned int index = i*rtt_num_framebuffers + j;
					rtt_textures_color[index].bindAsTexture(0);
					fullscreen_mesh.drawRange(GL_TRIANGLES, 6 * index, 6);
				}
			}
		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			framebuffer_width = width;
			framebuffer_height = height;
			for (auto& framebuffer : rtt_framebuffers) framebuffer.resize(width, height);
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			//reload all shaders
			switch (key) {
			case lap::wic::Key::SPACE:
				std::cout << "-------------------------------------" << std::endl << "[LAB] reloading shaders" << std::endl;
				scene_shader.reload();
				fullscreen_shader.reload();
				break;
			case lap::wic::Key::ESCAPE: wnd.close();			break;//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
			default:
				break;
			}
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			keyPress(wnd, key, alt, control, shift, system, state, timestamp);
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	
	};
}



//debug function based on glDebugOutput
void APIENTRY debugFunctionCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
	//ignore some type of warnings (otherwise the glsl compiler might spam the console with notifications)
	if (type == GL_DEBUG_TYPE_PERFORMANCE || type == GL_DEBUG_TYPE_PORTABILITY || severity == GL_DEBUG_SEVERITY_NOTIFICATION) return;

	std::cout << "-------------------------------------------------------------------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:				std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:	std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:		std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:		std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:				std::cout << "Source: Other"; break;
	}
	std::cout << std::endl;
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	}
	std::cout << std::endl;
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:			std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:			std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:				std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:	std::cout << "Severity: notification"; break;
	}
	std::cout << std::endl;
	std::cout << "Press any key" << std::endl;
	std::terminate();
}


int main(int argc, char* argv[]) {
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Textures - render to texture";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	fp.samples_per_pixel = 4;	//request multiple samples per pixel
	cp.swap_interval = -1;
	cp.debug_context = true;
	ip.cursor_enabled = false;	//cursor is not visible and always reverts to middle of the screen after movement
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	
	//use the current context to attach a debugging function, this is only possible if the debug flags are enabled
	GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(debugFunctionCallback, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
	
	//lab object
	lab::CGLab lab(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {
		lab.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};

}