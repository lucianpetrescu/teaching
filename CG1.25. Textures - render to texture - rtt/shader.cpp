///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "shader.h"
#include <cassert>

namespace lab {
	Shader::Shader() {
		glprogram = 0;
	}

	//creates a gpu program from a vertex shader file and a fragment shader file
	Shader::Shader(const std::string &vertex_shader_file, const std::string &fragment_shader_file) {
		load(vertex_shader_file, fragment_shader_file);
	}
	//creates a gpu program from a vertex shader file, a geometry shader file and a fragment shader file
	Shader::Shader(const std::string &vertex_shader_file, const std::string &geometry_shader_file, const std::string &fragment_shader_file) {
		load(vertex_shader_file, geometry_shader_file, fragment_shader_file);
	}
	//creates a gpu program from a vertex shader file, a tess control file, a tess eval file and a fragment shader
	Shader::Shader(const std::string &vertex_shader_file, const std::string &tessellation_control_shader_file, const std::string &tessellation_evaluation_shader_file, const std::string &fragment_shader_file) {
		load(vertex_shader_file, tessellation_control_shader_file, tessellation_evaluation_shader_file, fragment_shader_file);

	}
	//creates a gpu program from a vertex shader file, a tess control file, a tess eval file, a geometry shader and a fragment shader
	Shader::Shader(const std::string &vertex_shader_file, const std::string &tessellation_control_shader_file, const std::string &tessellation_evaluation_shader_file, const std::string& geometry_shader_file, const std::string &fragment_shader_file) {
		load(vertex_shader_file, tessellation_control_shader_file, tessellation_evaluation_shader_file, geometry_shader_file, fragment_shader_file);

	}
	//creates a gpu program from a compute shader
	Shader::Shader(const std::string &compute_shader_file) {
		load(compute_shader_file);
	}

	//destructor
	Shader::~Shader() {
		if (glprogram != 0) glDeleteProgram(glprogram);
	}

	//allow moves
	Shader::Shader(Shader&& rhs) {
		assert(this != &rhs);	//most definately a user bug as this leads to undefined behavior
		uniform_locations = std::move(rhs.uniform_locations);
		contents = std::move(rhs.contents);
		glprogram = rhs.glprogram;
		rhs.glprogram = 0;
	}
	Shader& Shader::operator=(Shader&& rhs) {
		assert(this != &rhs);	//most definately a user bug as this leads to undefined behavior
		if (glprogram != 0){
			glDeleteProgram(glprogram);
			glprogram = 0;
		}
		uniform_locations = std::move(rhs.uniform_locations);
		contents = std::move(rhs.contents);
		glprogram = rhs.glprogram;
		rhs.glprogram = 0;
		return (*this);
	}

	//clones this object(creates a new OpenGL object), expensive!
	Shader Shader::clone() const {
		Shader result;
		result.contents = contents;
		result.reload();
		for (auto& p : uniform_locations) result.getUniformLocation(p.first);	//updates the locations (this is required as cloning is not driver-proof)
		return result; //nrvo
	}



	void Shader::load(const std::string &vertex_shader_file, const std::string &fragment_shader_file) {
		if (glprogram != 0) glDeleteProgram(glprogram);
		contents.clear();
		contents.push_back({ vertex_shader_file, GL_VERTEX_SHADER });
		contents.push_back({ fragment_shader_file, GL_FRAGMENT_SHADER });
		createProgram();
	}
	void Shader::load(const std::string &vertex_shader_file, const std::string &geometry_shader_file, const std::string &fragment_shader_file) {
		if (glprogram != 0) glDeleteProgram(glprogram);
		contents.clear();
		contents.push_back({ vertex_shader_file, GL_VERTEX_SHADER });
		contents.push_back({ geometry_shader_file, GL_GEOMETRY_SHADER });
		contents.push_back({ fragment_shader_file, GL_FRAGMENT_SHADER });
		createProgram();
	}
	void Shader::load(const std::string &vertex_shader_file, const std::string &tessellation_control_shader_file, const std::string &tessellation_evaluation_shader_file, const std::string &fragment_shader_file) {
		if (glprogram != 0) glDeleteProgram(glprogram);
		contents.clear();
		contents.push_back({ vertex_shader_file, GL_VERTEX_SHADER });
		contents.push_back({ tessellation_control_shader_file, GL_TESS_CONTROL_SHADER });
		contents.push_back({ tessellation_evaluation_shader_file, GL_TESS_EVALUATION_SHADER });
		contents.push_back({ fragment_shader_file, GL_FRAGMENT_SHADER });
		createProgram();
	}
	void Shader::load(const std::string &vertex_shader_file, const std::string &tessellation_control_shader_file, const std::string &tessellation_evaluation_shader_file, const std::string& geometry_shader_file, const std::string &fragment_shader_file) {
		if (glprogram != 0) glDeleteProgram(glprogram);
		contents.clear();
		contents.push_back({ vertex_shader_file, GL_VERTEX_SHADER });
		contents.push_back({ tessellation_control_shader_file, GL_TESS_CONTROL_SHADER });
		contents.push_back({ tessellation_evaluation_shader_file, GL_TESS_EVALUATION_SHADER });
		contents.push_back({ geometry_shader_file, GL_GEOMETRY_SHADER });
		contents.push_back({ fragment_shader_file, GL_FRAGMENT_SHADER });
		createProgram();
	}
	void Shader::load(const std::string &compute_shader_file) {
		if (glprogram != 0) glDeleteProgram(glprogram);
		contents.clear();
		contents.push_back({ compute_shader_file, GL_COMPUTE_SHADER });
		createProgram();
	}

	//return raw OpenGL object
	GLuint Shader::getOpenGLObject() {
		return glprogram;
	}

	//bind shader
	void Shader::bind() {
		assert(glprogram != 0);
		glUseProgram(glprogram);
	}

	//reload shader
	void Shader::reload() {
		//delete previous program
		if(glprogram !=0) glDeleteProgram(glprogram);
		//and recreate program (the contents have stayed the same)
		createProgram();
	}
	
	//private create program
	void Shader::createProgram() {
		std::cout << "[LAB] Shader Loader : loading shader " << std::endl;

		//create each shader object
		std::vector<GLuint> shaders;
		for (auto& pair : contents) {
			//load file to string
			std::string shader_code;
			std::ifstream file(pair.first.c_str(), std::ios::in);
			if (!file.good()) {
				std::cout << "[LAB] ERROR! Shader Loader: Could not find file " << pair.first << " or lacking reading rights." << std::endl;
				std::terminate();
			}
			while (file.good()) {
				std::string line;
				std::getline(file, line);
				shader_code.append(line + "\n");
			}
			file.close();


			int info_log_length = 0, compile_result = 0;

			//build a shader objects
			GLuint glshader = glCreateShader(pair.second);
			const char *shader_code_ptr = shader_code.c_str();
			const int shader_code_size = (int)shader_code.size();
			glShaderSource(glshader, 1, &shader_code_ptr, &shader_code_size);
			glCompileShader(glshader);
			glGetShaderiv(glshader, GL_COMPILE_STATUS, &compile_result);

			if (pair.second == GL_VERTEX_SHADER) std::cout << "   vertex shader " << pair.first << std::endl;
			else if (pair.second == GL_TESS_CONTROL_SHADER) std::cout << "   tess control shader " << pair.first << std::endl;
			else if (pair.second == GL_TESS_EVALUATION_SHADER) std::cout << "   tess evaluation shader " << pair.first << std::endl;
			else if (pair.second == GL_GEOMETRY_SHADER) std::cout << "   geometry shader " << pair.first << std::endl;
			else if (pair.second == GL_FRAGMENT_SHADER) std::cout << "   fragment shader " << pair.first << std::endl;
			else if (pair.second == GL_COMPUTE_SHADER) std::cout << "   compute shader " << pair.first << std::endl;
			
			//log errors if any are present ----> logged at link stage (theoretically all major compilers do this)
			//if (compile_result == GL_FALSE) {
				//glGetShaderiv(glshader, GL_INFO_LOG_LENGTH, &info_log_length);
				//std::vector<char> shader_log(info_log_length);
				//glGetShaderInfoLog(glshader, info_log_length, NULL, &shader_log[0]);
				//if(info_log_length) std::cout << "[LAB] ERROR! Shader Loader: Compilation Error " << std::endl << &shader_log[0] << std::endl;
			//}
			shaders.push_back(glshader);
		}


		//build OpenGL program object and link all the OpenGL shader objects
		int info_log_length = 0, link_result = 0;
		glprogram = glCreateProgram();
		for (auto& s : shaders) glAttachShader(glprogram, s);
		glLinkProgram(glprogram);
		glGetProgramiv(glprogram, GL_LINK_STATUS, &link_result);

		//if we get link errors log them
		if (link_result == GL_FALSE) {
			glGetProgramiv(glprogram, GL_INFO_LOG_LENGTH, &info_log_length);
			std::vector<char> program_log(info_log_length);
			glGetProgramInfoLog(glprogram, info_log_length, NULL, &program_log[0]);
			if(info_log_length) std::cout << "[LAB] ERROR! Shader Loader : Linker Error" << std::endl << &program_log[0] << std::endl;
		}

		//delete the shader objects because we do not need them any more (this should be re-written if usage of separate shader objects is intended!)
		for (auto& s : shaders) glDeleteShader(s);

		//clean up uniform locations
		uniform_locations.clear();
	}

	int Shader::getUniformLocation(const std::string& name) {
		assert(glprogram != 0);
		int location = -1;
		const auto& iter = uniform_locations.find(name);
		if (iter == uniform_locations.end()) {
			//new uniform query, find out the location and store it in the uniform map
			location = glGetUniformLocation(glprogram, name.c_str());
			if (location == -1) std::cout << "[LAB] Shader Loader : Warning!, binding to unknown or unused variable named " << name << std::endl;
			uniform_locations[name] = location;
		}
		else {
			//found it in the map
			location = iter->second;
		}
		return location;
	}

	void Shader::setUniform(const std::string& name, float value) {
		assert(glprogram != 0);
		glUniform1f(getUniformLocation(name), value);
	}
	void Shader::setUniform(const std::string& name, int value) {
		assert(glprogram != 0);
		glUniform1i(getUniformLocation(name), value);
	}
	void Shader::setUniform(const std::string& name, unsigned int value) {
		assert(glprogram != 0);
		glUniform1ui(getUniformLocation(name), value);
	}
	void Shader::setUniform(const std::string& name, glm::vec2 value) {
		assert(glprogram != 0);
		glUniform2f(getUniformLocation(name), value.x, value.y);
	}
	void Shader::setUniform(const std::string& name, glm::ivec2 value) {
		assert(glprogram != 0);
		glUniform2i(getUniformLocation(name), value.x, value.y);
	}
	void Shader::setUniform(const std::string& name, glm::uvec2 value) {
		assert(glprogram != 0);
		glUniform2ui(getUniformLocation(name), value.x, value.y);
	}
	void Shader::setUniform(const std::string& name, glm::vec3 value) {
		assert(glprogram != 0);
		glUniform3f(getUniformLocation(name), value.x, value.y, value.z);
	}
	void Shader::setUniform(const std::string& name, glm::ivec3 value) {
		assert(glprogram != 0);
		glUniform3i(getUniformLocation(name), value.x, value.y, value.z);
	}
	void Shader::setUniform(const std::string& name, glm::uvec3 value) {
		assert(glprogram != 0);
		glUniform3ui(getUniformLocation(name), value.x, value.y, value.z);
	}
	void Shader::setUniform(const std::string& name, glm::vec4 value) {
		assert(glprogram != 0);
		glUniform4f(getUniformLocation(name), value.x, value.y, value.z, value.w);
	}
	void Shader::setUniform(const std::string& name, glm::ivec4 value) {
		assert(glprogram != 0);
		glUniform4i(getUniformLocation(name), value.x, value.y, value.z, value.w);
	}
	void Shader::setUniform(const std::string& name, glm::uvec4 value) {
		assert(glprogram != 0);
		glUniform4ui(getUniformLocation(name), value.x, value.y, value.z, value.w);
	}
	void Shader::setUniform(const std::string& name, glm::mat2 value, bool transpose) {
		assert(glprogram != 0);
		glUniformMatrix2fv(getUniformLocation(name), 1, transpose ? GL_TRUE : GL_FALSE, &value[0][0]);
	}
	void Shader::setUniform(const std::string& name, glm::mat3 value, bool transpose) {
		assert(glprogram != 0);
		glUniformMatrix3fv(getUniformLocation(name), 1, transpose ? GL_TRUE : GL_FALSE, &value[0][0]);
	}
	void Shader::setUniform(const std::string& name, glm::mat3x4 value, bool transpose) {
		assert(glprogram != 0);
		glUniformMatrix3x4fv(getUniformLocation(name), 1, transpose ? GL_TRUE : GL_FALSE, &value[0][0]);
	}
	void Shader::setUniform(const std::string& name, glm::mat4 value, bool transpose) {
		assert(glprogram != 0);
		glUniformMatrix4fv(getUniformLocation(name), 1, transpose ? GL_TRUE : GL_FALSE, &value[0][0]);
	}
}

