//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//attributes
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;


//uniform data
out vec2 texcoords;

void main(){

	texcoords = in_texcoord;
	gl_Position = vec4(in_position,1);
}

