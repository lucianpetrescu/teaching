///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
#include "texture.h"


namespace lab{

	class Framebuffer {
	public:
		Framebuffer();
		~Framebuffer();
		//delete implicit copies, use clone
		Framebuffer(const Framebuffer&) = delete;
		Framebuffer& operator=(const Framebuffer&) = delete;
		//allow move operators
		Framebuffer(Framebuffer&&);
		Framebuffer& operator=(Framebuffer&&);

		//clone
		Framebuffer clone() const;
		
		//resize all attached textures but lose ALL data (creates NO mipmaps)
		void resize(unsigned int width, unsigned int height);

		//bind and unbind
		void bind();
		void unbind();
		bool getBindState() const;

		//attachments (use nullptr to clear a binding)
		void setAttachmentColor(Texture2D* texture, unsigned int index);
		void setAttachmentDepth(Texture2D* texture);
		void setAttachmentStencil(Texture2D* texture);
		void setAttachmentDepthStencil(Texture2D* texture);
		unsigned int getAttachmentColorMax() const;
		Texture2D* getAttachmentColor(unsigned int index) const;
		Texture2D* getAttachmentDepth() const;
		Texture2D* getAttachmentStencil() const;
		bool getCompleteStatus() const;

		//basics
		unsigned int getWidth() const;
		unsigned int getHeight() const;
		GLuint getGLobject() const;
	private:
		void internalCheck();
		GLuint fbo =0;
		std::map<int, Texture2D*> attachments_color;
		Texture2D* attachment_depth = nullptr;
		Texture2D* attachment_stencil = nullptr;
		bool complete_status = false, bind_state = false;
		unsigned int width = 0, height = 0;
	};
}