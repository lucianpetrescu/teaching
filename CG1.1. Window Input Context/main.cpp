///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------

#include "../dependencies/glm/glm.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"

namespace lab {
	class CGLab {
		unsigned int framecount = 0;
		glm::vec3 red = glm::vec3(1, 0, 0);
		glm::vec3 green = glm::vec3(0, 1, 0);
		glm::vec3 blue = glm::vec3(0, 0, 1);
	public:
		CGLab() {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;
		}
		~CGLab() {
			std::cout << "[LAB] Destroyed" << std::endl;
		}
		void render() {
			//set clear color (command to set state)
			int seconds = framecount / 60;
			if(seconds < 2) glClearColor(red.x, red.y, red.z, 0);
			else if (seconds < 4) glClearColor(green.x, green.y, green.z, 0);
			else glClearColor(blue.x, blue.y, blue.z, 0);
			framecount = (framecount + 1) % 360;

			//clear screen (command to clear the currently used framebuffer = default)
			glClear(GL_COLOR_BUFFER_BIT);
		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Resized to " << width << "x" << height << std::endl;
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Pressed : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
			if (key == lap::wic::Key::ESCAPE) wnd.close();	//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Released : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Repeat : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MousePress : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseRelease : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseDrag : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseMove posx=" << posx << " posy=" << posy << std::endl;
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseScroll : scrollx= " << scrollx << " scrolly=" << scrolly << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
	
	};
}



int main(int argc, char* argv[]) {
	//create a Window-Input-Context system with logging on std::cout and no thread safety (running in singlethreaded mode)
	lap::wic::WICSystem wicsystem(&std::cout, false);
	//create a window with properties
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CG1 1.Window - window, input, context, main loop";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	cp.swap_interval = -1;	//adaptive vsync, limits fps at monitor update rate (e.g 60Hz), your graphics card might ignore/override this request!
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//create the Lab object here (the Lab object will REQUIRE an ACTIVE OpenGL context, which is provided by the window object), and set callbacks to member functions 
	lab::CGLab lab;
	using namespace std::placeholders;	//a delegate binds a member function from a class instance and uses placeholders (_1, _2..) for arguments
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {

		//call render each frame
		lab.render();

		//swap buffers (always rendering to the back buffer and drawing the front buffer, otherwise we'd get tearing)
		window.swapBuffers();

		//process events -> calls callbacks for each event
		window.processEvents();

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};
}