# README #

NOTE: These are ported/written on a day-to-day basis, as a low priority project. It will take a **LOT** of time until they are all up. 

### ABOUT ###
This is the support code for various computer graphics lessons (labs), ranging from computer graphics beginner level to advanced rendering topics. The lessons are partitioned into 6 lesson groups, ranging from introductory to advanced topics:

- Computer Graphics 1 (cg1) - Introduction in Computer Graphics
- Computer Graphics 2 (cg2) - GPGPU
- Computer Graphics 3 (cg3) - Geometry and Surface Topics
- Computer Graphics 4 (cg4) - Post-processing and Image Effects
- Computer Graphics 5 (cg5) - The physics of light (for rendering)
- Computer Graphics 6 (cg6) - Animation
- Computer Graphics 7 (cg7) - Real-time Rendering
- Computer Graphics 8 (cg8) - Procedural Generation and Natural Phenomena
- Computer Graphics 9 (cg9) - Rendering Algorithms

The purpose of these lessons is to **steadily** take the reader through many useful concepts, presented in **small, heavily commented and contained** examples. Most lessons have several dependencies, which are shared in the **common** folder.  

author: Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )

### ASSETS ###

The shared assets can be found in the **assets** folder. The sources for the assets are given in **assets/Readme.md**. Because the assets are too large to keep in a code repository (and are not code) they can downloaded from this Google Drive link [https://drive.google.com/open?id=0B3yTES0pEsWXR2E0RWtJT1BSbEk](https://drive.google.com/open?id=0B3yTES0pEsWXR2E0RWtJT1BSbEk) . Just download and extract in the root folder.

### OPENGL ###

The examples are written with modern OpenGL (4.3+) and are tested on Windows and Linux and are tested on an NVIDIA GPU. I don't own a MacOS machine, but the makefile should work. Be aware that some drivers (especially on MacOS/Intel) have low support for modern OpenGL(4.3/4.4/4.5) and some code might crash because of this (e.g. driver exposes function but does not implement it, or implements it incorrectly). Unfortunately, there is nothing I can do about this. Also, if you are interested in working with the OpenGL debug output functions, be aware that some drivers like to output many warnings on this callback, which can lead to verbose output without proper filtering.


### FOR INSTRUCTORS ###

Teaching rendering as a natural physical phenomenon is elegant and precise, but it is also extremely difficult as it puts beginners in a very difficult learning environment (large processing times, mathematical apparatus, no instant gratification, code obfuscation caused by potential tracing libraries, and so on). 

Thus, the lessons take the standard approach to rendering, starting from rasterization and slowly building a rendering system from the bottom up. Therefore, the used nomenclature is common (rasterization) instead of scientific, thus the **deferred**, **reflections**, **shadows** and **lighting/illumination** terms are used instead of just using **rendering equation solver components** (see [Rendering Equation](https://en.wikipedia.org/wiki/Rendering_equation), n.b. lacks scattering), making it easy for students to distinguish between the different methods/algorithms used to enhance the capabilities of rasterization. In order to sanitize the taxonomy of laboratories a double indexing scheme is used : by normal course (cg1/2/3..) and by subject (indirect illumination I,II, etc). Large sub subjects are named Indirect Illumination - Shadows (subject - sub subject) some description of the lab.

Thus, the nomenclature behaves exactly like the rasterization rendering algorithm class itself, starting from simple, disconnected and grossly incomplete models and converging to pseudo-realistic results through the addition of several algorithms, each bringing the implemented **rendering model** closer to the rendering equation. This teaching method produces great results as it is consistent when transitioning to photorealistic rendering algorithms, as these are too are component based (scattering, (temporal) path/ray filtering, path mutation, (uni/bi)directional)

As teaching is the primary purpose of the lessons, **clarity is valued over performance and visual results**. This applies to algorithms performance, coding standards (weak enforcement) and software architecture complexity.

While it may seem unreasonable to not store common code in a common folder (e.g. buffer, texture, etc), please keep in mind that this would lead to students having difficult first encounters with some evolving pieces of code, e.g. : first contact with texture containing all of texture2D, texture3D, texture2Dmultisample, cubemaps, etc. 

### Contents ###

##### Computer Graphics I (cg1) - Introduction in Computer Graphics #####


1. Introduction - OS support window, input handling, OpenGL context, OpenGL extension loading, render loop
1. Debug - debugging OpenGL, getError and DebugCallback
1. Buffers - vertex buffers and the OpenGL object model
1. Shaders - gpu programs, Vertex Shader, Fragment Shader, and how the shaders take vertex information from the CPU (through attributes)
1. Data and Topology - index buffers, vertex array objects, attributes.
1. Data and Topology - primitive types and rasterization modes
1. Shaders II - Uniforms, the other way of sending data to the gpu (besides attributes). Uniforms and uniform buffers, vertex and fragment shaders
1. Transformations - 2D transformations and the vertex shader
1. Transformations - 3D transformation, model view projection (MVP)
1. Transformations - A 3D camera
1. Rasterization - framebuffer operations: depth test, multisampling, scissor test
1. Rasterization - face culling, winding
1. Rasterization - attribute interpolation, gradients and dithering
1. Mesh loading - what is a mesh? meshes are not models!, loading and drawing meshes from the wavefront .obj file format (only geometry is handled, no groups, no materials).
1. Direct Illumination - illuminates a mesh with a point light, using a per-vertex shading model (Gouraud) with the Lambert illumination model
1. Direct Illumination - illuminates a mesh with a point light, using a per-fragment shading model (Phong) with the Blinn-Phong illumination model
1. Direct Illumination - illuminates a mesh with a cone (spot) light, using a per-fragment shading model (Phong) with the Blinn-Phong illumination model
1. Buffers - a gpu buffer object, using uniform buffers
1. Buffers - permanent storage
1. Buffers - object-oriented ownership
1. Textures - loading images, basic OpenGL 2D textures
1. Textures - texture filtering(bilinear, trilinear, anisotropic), texture sampler objects
1. Textures - multitexturing, alpha culling, point out different types of textures (emission, specular, color, normal, ambient occlusion, etc), specularity
1. Textures - full screen texturing
1. Textures - render to texture (RTT)
1. Textures - depth textures, visualizing depth, multiple render targets (MRTs)
1. Indirect Illumination - basic OpenGL transparency
1. Indirect Illumination - fake planar reflections
1. Indirect Illumination - planar reflections
1. Indirect Illumination - cubemap textures, environment mapping with cubemaps
1. Indirect Illumination - full screen refractions and chromatic aberrration
1. Direct Illumination - rim lighting
1. Direct Illumination - two faced lighting
1. Direct Illumination - fake volumetric point and cone lights
1. Direct Illumination - colored spotlights with projected textures
1. Direct Illumination - (Shadows) shadow maps, bias, multiple samples, light pov
1. Direct Illumination - (Shadows) early termination, Poission (stratified) sampling, exponential
1. Buffers - streaming data to the GPU buffers with pixel buffer objects
1. Textures - streaming data to the GPU 2D textures with pixel buffer objects, from a different thread
1. GUI - rendering text
1. GUI - using Signed Distance Fields (SDF)
1. GUI - cursor, adjustment handle, insertion point, icons
1. GUI - widget concepts : window, anchor, scrollable, children, hint, onmove, onclick, onetc, enabled/disabled, textured, alpha textured 
1. GUI - accelerated input handling, selection
1. GUI - some common widgets (button, checkbox, radiobutton, slider, tooltip, progressbar)
1. GUI - other common widgets (panel, label, dialog box)
1. GUI - menu bars and tabs
1. GUI - plotters (1D, 2D, 3D, flow) and histograms
1. GUI - an extensible json gui file format + parser + loader
 
##### Computer Graphics 2 (cg2) - GPGPU #####

TODO: upload and port me from old format

1. Compute Shaders - portable GPGPU
1. Compute Shaders - images, image units, mandelbrot, julia
1. Compute Shaders - SSBOs, unsized buffers
1. Compute Shaders - separable blur, group shared memory
1. Compute Shader - parallel minimum / maximum
1. Compute Shader - parallel sum
1. Compute Shader - parallel map
1. Compute Shader - parallel reduce
1. Compute Shader - parallel prefix scan
1. Compute Shader - parallel sort 
1. Compute Shaders - n-body
1. Compute Shaders - Fast Fourier Transform (FFT)
1. Compute Shaders - voxelization
1. Compute Shaders - imperfect voxelization
1. Compute Shaders - marching cubes
1. Atomic Counters - minimum and maximum
1. Atomic Counters - counting, rasterization order example


##### Computer Graphics 3 (cg3) - Geometry and Surface Topics #####

TODO

1. Mesh Processing (cpu and gpu)
1. Geometry - basic geometry (cube, cone, plane, elipsis, hiperboloid, paraleloid etc)
1. Geometry - basic geometry 2 (tangents, normals, bitangents)
1. Geometry Shaders - introduction, pass through
1. Geometry Shaders - illumination per face 
1. Geometry Shaders - draw normals per vertex and per face, draw tangents
1. Geometry Shaders - frustum culling per triangle in NDC space.
1. Geometry Shaders - shrinking and growing
1. Geometry Shaders - outlines
1. Geometry Shaders - tessellation
1. Geometry Shaders - tessellation, translation and rotation surfaces
1. Geometry Shaders - shells
1. Geometry Shaders - impostors with billboards
1. Geometry Shaders - multiple viewports
1. Geometry Shaders - layered rendering
1. Transform Feedback(Stream Out) - transforming geometry
1. Transform Feedback(Stream Out) - interleaved and separated output
1. Transform Feedback(Stream Out) - particle systems, points
1. Tessellation Shaders - introduction (uniform tessellation)
1. Tessellation Shaders - tessellation spacing modes
1. Tessellation Shaders - Bezier line tessellation
1. Tessellation Shaders - Bezier patches
1. Tessellation Shaders - PN patches
1. Tessellation Shaders - Phong patches
1. Tessellation Shaders - Gregory patches
1. Tessellation Shaders - Adaptive tessellation
1. Sub-geometric Surfaces - full screen normal perturbation with dithering
1. Sub-geometric Surfaces - tangent space
1. Sub-geometric Surfaces - bump mapping
1. Sub-geometric Surfaces - parallax mapping
1. Sub-geometric Surfaces - iterative parallax mapping
1. Sub-geometric Surfaces - relaxed cone stepping
1. Sub-geometric Surfaces - normal map anti aliasing
1. Particle Systems - points, using shader storage buffers (SSBOs)
1. Particle Systems - billboards
1. Particle Systems - lighted, normal mapped billboards
1. Particle Systems - static particles (strands)
1. Particle Systems - small objects
1. Particle Systems - emitters, attractors, changers, (e.g. fireworks)
1. Particle Systems - screen space plastic collisions (e.g. snow)
1. Particle Systems - screen space elastic collisions (e.g. rain)

##### Computer Graphics 4 (cg4) - Postprocessing and Image Effects #####

TODO 

1. Post Processing - what is a kernel, box, triangle, cubic, sinc, hann, hammings, blackmann, kaiser, lanczocs (2D) 
1. Post Processing - basic filters : color invert, flip channels, sobel, grayscale, blur, sharpen, sepia
1. Post Processing - artistic filters : night vision, thermal vision
1. Post Processing - frei chen edge detection
1. Post Processing - full screen filters I : swirl, fish-eye, barrel distorsion
1. Post Processing - multi-pass filters, erosion, dilation
1. Post Processing - multi-pass filters, separated filters (gaussian, horizontal + vertical)
1. Post Processing - multi-pass filters, bilateral filter
1. Post Processing - portable GPGPU (from in Compute Shaders)
1. Post Processing - images, image units, mandelbrot, julia  (from in Compute Shaders)
1. Post Processing - SSBOs, unsized buffers  (from in Compute Shaders)
1. Post Processing - separable blur, group shared memory  (from in Compute Shaders)
1. Post Processing - tone mapping, high dynamic range (HDR)
1. Post Processing - depth visualization
1. Post Processing - depth of field
1. Post Processing - motion blur
1. Post Processing - god rays / light shafts, with debris
1. Post Processing - gamma and srgb
1. Post Processing - screen space normals
1. Post Processing - multisample 2D texture, antialiasing
1. Post Processing - morphological antialiasing, fxaa
1. Post Processing - morphological antialiasing II, mlaa
1. Post Processing - morphological antialiasing III, reprojection
1. Post Processing - temporal filtering, reprojection
1. Post Processing - Non-Photorealistic Rendering (NPR), outlining in fragment shader
1. Post Processing - NPR, Gooch shading
1. Post Processing - NPR, (car)toon shading, luminance quantisation
1. Post Processing - NPR, pixelify (retro)
1. Post Processing - NPR, paper normal mapping
1. Post Processing - NPR, stippling and hatching
1. Post Processing - NPR, painterly shading with the anisotropic Kuwahara filter
1. Post Processing - Iterative Edge Thinning - Zhang Suen
1. Post Processing - Iterative Edge Thinning - OPTA
1. Post Processing - Iterative Segmentation

##### Computer Graphics 5 (cg5) - The physics of light #####

1. Spectral power distributions (SPD) and light waves visualization
1. Participating media : absorbtion and scattering visualization
1. Huygens fresnel visualization
1. Fresnel Law
1. Microgeometry - subpixel geometry
1. Microgeometry - the half vector
1. Microgeometry - shadowing and masking
1. Reflection lobes : metals and non-metals
1. Basic BRDFs
1. A combined lighting equation
1. Advanced BRDFS, based on
[http://blog.selfshadow.com/publications/s2016-shading-course/](http://blog.selfshadow.com/publications/s2016-shading-course/)
1. BSSRDFs, usage of fragment derivatives

##### Computer Graphics 6 (cg6) - Animation #####

TODO **low priority**

1. Static Animation - translations, rotations, scaling
1. Static Animation - trajectories
1. Static Animation - quaternions
1. Static Animation - lerp, slerp
1. Static Animation - animation curves
1. Warping/Morphing - from one object to another object
1. Key-frame Animation - sprites
1. Key-frame Animation - 3D, frame interpolation
1. Key-frame Animation - facial animation example
1. Articulated figures - a tree structure
1. Articulated figures - the skeleton (joints, bones) and the tree structure
1. Articulated figures - an animated skeleton
1. Articulated figures - using frame interpolation in animated skeletons
1. Articulated figures - bvh file format
1. Articulated figures - loading models with animations from the MD5 format.

##### Computer Graphics 7 (cg7) - Real-time Rendering #####

TODO 

1. Engine - instancing
1. Engine - render state and render pass
1. Engine - technique (multiple render passes)
1. Engine - materials
1. Engine - ordered by material
1. Engine - scene and objects
1. Engine - an extensible json scene file format + parser + loader (chunk descriptor + data)
1. Engine - an asset manager
1. Engine - loading a model with meshes and textures from obj (no animations)
1. Engine - profiling memory
1. Engine - querying performance
1. Engine - selecting with ray-box intersection test
1. Engine - selection with an additional color buffer
1. Engine - selection with atomic counters
1. Engine - moving selected objects
1. Engine - camera effects
1. Engine - creating objects on selection (decals)
1. Engine - streaming data to the GPU buffers with pixel buffer objects
1. Engine - streaming data to the GPU 2D textures with pixel buffer objects
1. Engine - streaming from a different thread
1. Engine - frustum culling per object
1. Engine - frustum culling with a tree hierarchy over the scene
1. Engine - indirect rendering
1. Engine - virtual texturing **low priority**
1. Engine - virtual meshes **low priority**
1. Engine - deferred decals **low priority**
1. Indirect Illumination - dynamic planar reflections.
1. Indirect Illumination - dynamic environment mapping. Illumination with dynamic cubemaps.
1. Indirect Illumination - Precomputed Radiance Transfer(PRT) and Spherical Harmonics (SH)
1. Direct Illumination - (Deferred) classic deferred rendering (with a single geometry pass)
1. Direct Illumination - (Deferred) light pre-pass deferred rendering (with multiple geometry passes)
1. Direct Illumination - (Deferred) deferred antialiasing, SRAA / GBAA
1. Direct Illumination - (Shadows) cascaded shadow maps 
1. Direct Illumination - (Shadows) filtered with deferred
1. Direct Illumination - (Shadows) point light shadows
1. Direct Illumination - (Deferred) storing lights with per pixel linked lists
1. Direct Illumination - (Deferred) Tiled Deferred
1. Direct Illumination - (Deferred) Clustered Deferred
1. Indirect Illumination - order independent transparency (OIT) with linked lists
1. Indirect Illumination - (Deferred) scattering, SSS, volumetric effects
1. Indirect Illumination - ambient occlusion/transmittance maps
1. Indirect Illumination - Screen-Space Ambient Occlusion, SSAO/HBAO/GBAO
1. Indirect Illumination - (Global Illumination) light maps (static objects)
1. Indirect Illumination - (Global Illumination) Reflective Shadow Maps (RSM)
1. Indirect Illumination - (Global Illumination) Screen Space Ray Tracing (SSRT)
1. Indirect Illumination - (Global Illumination) Screen Space Cone Tracing (SSCT)
1. Indirect Illumination - (Global Illumination) Screen Space Photon Mapping (SSPM)  


##### Computer Graphics 8 (cg8) - Procedural Generation and Natural Phenomena #####

TODO: upload and port me from old format, **low priority**

1. Texture Synthesis - simple patterns, brick pattern
1. Texture Synthesis - perlin noise
1. Texture Synthesis - stars, perlin noise application
1. Planetary System - a primer in 3d generation
1. Terrain - polygon based
1. Terrain - erosion
1. Terrain - displacement or more than one axis
1. Fog - constant vs generation
1. Sky - cubemap (skybox), skydome, full generation, scattering
1. Lightning
1. Water - water surface, composing noise-based flux maps
1. Rivers - watershed segmentation based
1. Rain - particles
1. Fire - particles
1. Smoke - particles
1. Clouds - billboards
1. Clouds - volumetric
1. Grass - billboards
1. Trees and Forests - rule based generation
1. Buildings - rule based generation
1. Cities - grid and density 
1. Cities - full generation



##### Computer Graphics 9 (cg9) - Rendering Algorithms #####

TODO: upload and port me from old format **low priority**

1. Rasterization 
1. Radiosity
1. Accelerated Tracing - KD-tree
1. Improved Sample Generation
1. Ray Casting
1. Ray Tracing
1. (Voxel) Cone Tracing
1. Path Tracing
1. Bidirectional Path-Tracing
1. Photon Mapping
1. Many Lights Methods


### LICENSE ###
based on [Zlib License](https://en.wikipedia.org/wiki/Zlib_License)

Copyright(c) <2013> <Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )>

This software is provided 'as-is', without any express or implied
warranty.In no event will the authors be held liable for any damages
arising from the use of this software.
 
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions :

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software.If you use this software
in a product, an acknowledgement in the product documentation would be
appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.