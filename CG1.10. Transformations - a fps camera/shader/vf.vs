#version 450

//get data from attribute pipe #0, interpret it as vec2 
layout(location = 0) in vec3 in_pos;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;


void main(){

	// TRANSFORMATIONS ARE BASED ON CONVENTIONS
	// we'll always use the column-ordered (column-major) and the left handed coordinate system conventions

	//object space (where the vertex is modelled)
	vec4 os_pos = vec4(in_pos,1);
	
	//world space / scene space .. where all objects lie in the scene		
	vec4 ws_pos = model_matrix * os_pos;

	//view/camera space ... the position of the vertex in the coordinate system defined by the (camera front, camera right, camera up) coordinate system
	vec4 vs_pos = view_matrix * ws_pos;

	//projection space
	vec4 ps_pos = projection_matrix * vs_pos;

	//send it down the pipeline to NDC perspective divide => fragments => window coordinates
	gl_Position = ps_pos;
}
