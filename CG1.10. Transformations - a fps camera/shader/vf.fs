#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

uniform vec3 color;

void main(){
	colorout = color;
}
