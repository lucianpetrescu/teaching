//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//attributes
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;


//object id
uniform int id = W0;
//static data, 
layout(column_major, std140, binding = 0) uniform UboData {
	mat4 model_matrix[2];
	vec4 scene_colors[2];
	mat4 view_matrix;
	mat4 projection_matrix;
};

//output to fragment shader
out vec4 color;

void main(){

	color = scene_colors[id];

	gl_Position = projection_matrix * view_matrix * model_matrix[id] * vec4(in_position,1);
}

