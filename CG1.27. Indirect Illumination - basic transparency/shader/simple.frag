//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec4 colorout;

//input from vetex shader
in vec4 color;

void main(){
	colorout = color;
}

