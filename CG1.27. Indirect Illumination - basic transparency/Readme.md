###Screenshots###

Transparency can be implemented with low correctness in OpenGL by using blending. The blending mechanism has a lot of practical utility, but it should be noted that transparency is an ordered operation, while the rasterization algorithm does not provide an explicit order. Blending is an image composition operator, not a fully tridimensional operator.

without blending
![](img/output.png)
various types of blending:
![](img/output_2.png)
![](img/output_3.png)
![](img/output_4.png)
