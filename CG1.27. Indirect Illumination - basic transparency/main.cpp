﻿///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/rotate_vector.hpp"
#include "../dependencies/glm/gtx/transform.hpp"
#include "../dependencies/glm/gtx/transform2.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"
#include <ctime>
#include "shader.h"
#include "camera.h"
#include "mesh.h"

const std::string ASSET_PATH = "../../assets/";

namespace lab {
	class CGLab {
		//original framebuffer
		unsigned int framebuffer_width = 0, framebuffer_height = 0;

		//scene
		Shader shader;
		Mesh mesh;
		Buffer ubo{ GL_DYNAMIC_STORAGE_BIT };
		struct UboData {
			glm::mat4 scene_transformations[2];
			glm::vec4 scene_colors[2];
			glm::mat4 view_matrix;
			glm::mat4 projection_matrix;
		}ubodata;

		//mode
		bool blending_active = false;

	public:
		CGLab(unsigned int width, unsigned int height) {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;
			//window
			framebuffer_width = width;
			framebuffer_height = height;

			//state
			glClearColor(0.f, 0.f, 0.f, 0.0f);
			glEnable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);

			//scene
			shader.load("../shader/simple.vert", "../shader/simple.frag");
			mesh.load(ASSET_PATH + "models/standard/plane_xoy.obj");
			ubo.resize(sizeof(UboData));
			ubodata.scene_transformations[0] = glm::translate(glm::mat4(1), glm::vec3(-10, -10, 0));
			ubodata.scene_transformations[1] = glm::translate(glm::mat4(1), glm::vec3( 10, 10, 1));
			ubodata.view_matrix = glm::lookAt(glm::vec3(0, 10, -100), glm::vec3(0, 10, 0), glm::vec3(0, 1, 0));
			ubodata.projection_matrix = glm::perspective(90.0f, (float)width / (float)height, 0.1f, 1000.0f);
			ubodata.scene_colors[0] = glm::vec4(1, 0, 0, 0.5);
			ubodata.scene_colors[1] = glm::vec4(0, 1, 0, 0.5);
		}
		~CGLab() {
			//raii
			std::cout << "[LAB] Destroyed" << std::endl << "----------------------------------------------" << std::endl;
		}
		void render() {
			glViewport(0, 0, framebuffer_width, framebuffer_height);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

			//blending
			if (blending_active) {
				glDisable(GL_DEPTH_TEST);
				glEnable(GL_BLEND);
			}
			else {
				glEnable(GL_DEPTH_TEST);
				glDisable(GL_BLEND);
			}

			//send scene information
			ubo.update(&ubodata, 0, sizeof(UboData));
			ubo.bindIndexed(GL_UNIFORM_BUFFER, 0);
			shader.bind();
			for (int i = 0; i < 2; i++) {
				shader.setUniform("id", i);
				mesh.draw(GL_TRIANGLES);
			}
		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			framebuffer_width = width;
			framebuffer_height = height;
			ubodata.projection_matrix = glm::perspective(90.0f, (float)width / (float)height, 0.1f, 1000.0f);
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			
			static GLenum src_factor = GL_ONE, dst_factor = GL_ONE;
			
			//basic keys
			switch (key) {
			case lap::wic::Key::SPACE:
				std::cout << "-------------------------------------" << std::endl << "[LAB] reloading shaders" << std::endl;
				shader.reload();
			break;
			case lap::wic::Key::B: blending_active = !blending_active; break;
			case lap::wic::Key::ESCAPE: wnd.close();			break;//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
				
			//equation
			case lap::wic::Key::A:	glBlendEquation(GL_FUNC_ADD);				std::cout << "   equation = FUNC_ADD" << std::endl;	break;
			case lap::wic::Key::S:	glBlendEquation(GL_FUNC_SUBTRACT);			std::cout << "   equation = FUNC_SUBTRACT" << std::endl;	break;
			case lap::wic::Key::R:	glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);	std::cout << "   equation = FUNC_REVERSE_SUBTRACT" << std::endl;	break;
			case lap::wic::Key::I:	glBlendEquation(GL_MIN);					std::cout << "   equation = MIN" << std::endl;	break;
			case lap::wic::Key::X:	glBlendEquation(GL_MAX);					std::cout << "   equation = MAX" << std::endl;	break;

			//func params
			case lap::wic::Key::NUM0:	src_factor = GL_ZERO;					std::cout << "   source = GL_ZERO" << std::endl;				break;
			case lap::wic::Key::NUM1:	src_factor = GL_ONE;					std::cout << "   source = GL_ONE" << std::endl;					break;
			case lap::wic::Key::NUM2:	src_factor = GL_SRC_COLOR;				std::cout << "   source = GL_SRC_COLOR" << std::endl;			break;
			case lap::wic::Key::NUM3:	src_factor = GL_ONE_MINUS_SRC_COLOR;	std::cout << "   source = GL_ONE_MINUS_SRC_COLOR" << std::endl;	break;
			case lap::wic::Key::NUM4:	src_factor = GL_DST_COLOR;				std::cout << "   source = GL_DST_COLOR" << std::endl;			break;
			case lap::wic::Key::NUM5:	src_factor = GL_ONE_MINUS_DST_COLOR;	std::cout << "   source = GL_ONE_MINUS_DST_COLOR" << std::endl;	break;
			case lap::wic::Key::NUM6:	src_factor = GL_SRC_ALPHA;				std::cout << "   source = GL_SRC_ALPHA" << std::endl;			break;
			case lap::wic::Key::NUM7:	src_factor = GL_ONE_MINUS_SRC_ALPHA;	std::cout << "   source = GL_ONE_MINUS_SRC_ALPHA" << std::endl;	break;
			case lap::wic::Key::NUM8:	src_factor = GL_DST_ALPHA;				std::cout << "   source = GL_DST_ALPHA" << std::endl;			break;
			case lap::wic::Key::NUM9:	src_factor = GL_ONE_MINUS_DST_ALPHA;	std::cout << "   source = GL_ONE_MINUS_DST_ALPHA" << std::endl;	break;
			
			case lap::wic::Key::NUMPAD0:src_factor = GL_ZERO;					std::cout << "   destination = GL_ZERO" << std::endl;				break;
			case lap::wic::Key::NUMPAD1:src_factor = GL_ONE;					std::cout << "   destination = GL_ONE" << std::endl;				break;
			case lap::wic::Key::NUMPAD2:src_factor = GL_SRC_COLOR;				std::cout << "   destination = GL_SRC_COLOR" << std::endl;			break;
			case lap::wic::Key::NUMPAD3:src_factor = GL_ONE_MINUS_SRC_COLOR;	std::cout << "   destination = GL_ONE_MINUS_SRC_COLOR" << std::endl;break;
			case lap::wic::Key::NUMPAD4:src_factor = GL_DST_COLOR;				std::cout << "   destination = GL_DST_COLOR" << std::endl;			break;
			case lap::wic::Key::NUMPAD5:src_factor = GL_ONE_MINUS_DST_COLOR;	std::cout << "   destination = GL_ONE_MINUS_DST_COLOR" << std::endl;break;
			case lap::wic::Key::NUMPAD6:src_factor = GL_SRC_ALPHA;				std::cout << "   destination = GL_SRC_ALPHA" << std::endl;			break;
			case lap::wic::Key::NUMPAD7:src_factor = GL_ONE_MINUS_SRC_ALPHA;	std::cout << "   destination = GL_ONE_MINUS_SRC_ALPHA" << std::endl;break;
			case lap::wic::Key::NUMPAD8:src_factor = GL_DST_ALPHA;				std::cout << "   destination = GL_DST_ALPHA" << std::endl;			break;
			case lap::wic::Key::NUMPAD9:src_factor = GL_ONE_MINUS_DST_ALPHA;	std::cout << "   destination = GL_ONE_MINUS_DST_ALPHA" << std::endl;break;
			default:
				break;
			}
			//apply changes
			glBlendFunc(src_factor, dst_factor);
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			keyPress(wnd, key, alt, control, shift, system, state, timestamp);
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	
	};
}



//debug function based on glDebugOutput
void APIENTRY debugFunctionCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
	//ignore some type of warnings (otherwise the glsl compiler might spam the console with notifications)
	if (type == GL_DEBUG_TYPE_PERFORMANCE || type == GL_DEBUG_TYPE_PORTABILITY || severity == GL_DEBUG_SEVERITY_NOTIFICATION) return;

	std::cout << "-------------------------------------------------------------------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:				std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:	std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:		std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:		std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:				std::cout << "Source: Other"; break;
	}
	std::cout << std::endl;
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	}
	std::cout << std::endl;
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:			std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:			std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:				std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:	std::cout << "Severity: notification"; break;
	}
	std::cout << std::endl;
}


int main(int argc, char* argv[]) {
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Indirect Illumination - basic transparency";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	fp.samples_per_pixel = 4;	//request multiple samples per pixel
	cp.swap_interval = -1;
	cp.debug_context = true;
	ip.cursor_enabled = false;	//cursor is not visible and always reverts to middle of the screen after movement
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	
	//use the current context to attach a debugging function, this is only possible if the debug flags are enabled
	GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(debugFunctionCallback, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
	
	//lab object
	lab::CGLab lab(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {
		lab.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};

}