#version 450

//get data from attribute pipe #0, interpret it as vec2 
layout(location = 0) in vec2 in_pos;

uniform mat3 transformation;

void main(){

	// TRANSFORMATIONS ARE BASED ON CONVENTIONS
	// we'll always use the column-ordered (column-major) and the left handed coordinate system conventions in the
	// following lessons, as these are the common OpenGL conventions. Be aware that D3D uses the row-ordered (row-major)
	// and right handed coordinate system conventions.

	//affine transform the vertex, from its original (object space = space in which it was designed/created) to a new space.
	//the matrix is column-ordered thus the order of operations is from right to left
	vec3 transformed_pos = transformation * vec3(in_pos, 1);

	//if the matrix were in row-order the order of operations would be left to right and we'd write the last line like:
	//vec3 transformed_pos = vec3(pos, 1) * transformation.

	//send it down the pipeline
	gl_Position = vec4(transformed_pos.xy, 0, 1);
}
