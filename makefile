###########################################################################################################################################
#													CONTENTS

#only directories starting with CG, sort in natural version, replace
LABS = $(shell ls -vd CG*/| sed 's/ /\\ /g')

#make can barely handle spaces, and only in wildcards, so we use substitution wildcards to get around this. 
#Note the "\" before each space in the original LABS list.
#Note2: this is not needed with the current nomenclature
spaceto? = $(subst \ ,?,$1)
?tospace = $(subst ?,\ ,$1)			
LABS := $(call spaceto?,$(LABS))
			
define \n


endef


###########################################################################################################################################
#													BUILD TARGETS
#make might confuse "all" "clean" "distclean" etc targets for actual filenames, PHONY is used to prevent this confusion.
.PHONY: all build debug release clean help install distclean

all: build

debug:
	@echo $(\n)
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Debug only ------------------------------
	@echo ------------------------------------------------------------------------------------
	$(foreach  x,$(LABS), @echo $(\n) @$(MAKE) debug --no-print-directory -C $(call ?tospace,$(x)) ${\n})
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
release:
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Release only ----------------------------
	@echo ------------------------------------------------------------------------------------
	$(foreach  x,$(LABS), @echo $(\n) @$(MAKE) release --no-print-directory -C $(call ?tospace,$(x)) ${\n})
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
build: 
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Debug and Release -----------------------
	@echo ------------------------------------------------------------------------------------
	$(foreach  x,$(LABS), @echo $(\n) @$(MAKE) all --no-print-directory -C $(call ?tospace,$(x)) ${\n})
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
clean:
	@echo ------------------------------------------------------------------------------------
	@echo --------------------------------Cleaning all projects ------------------------------
	@echo ------------------------------------------------------------------------------------
	$(foreach  x,$(LABS), @echo $(\n) @$(MAKE) clean --no-print-directory -C $(call ?tospace,$(x)) ${\n})
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
distclean: clean


###########################################################################################################################################
# 														HELP and INSTALL
help:
	@echo $(\n)
	@echo ---This makefile calls the makefiles of all labs in this repository  ---
	@echo $(\n)
	@echo use -make build- or -make all- to build both debug and release executables
	@echo use -make debug- to build only in debug mode
	@echo use -make release- to build only in release mode
	@echo use -make clean- to clean all gnu/gcc related binaries
list:
	@echo $(\n)
	@echo --- The list of labs :  ---
	@echo $(\n)
	$(foreach  x,$(LABS), @echo $(call ?tospace,$(x)) ${\n})
install:
	@echo nothing to install
