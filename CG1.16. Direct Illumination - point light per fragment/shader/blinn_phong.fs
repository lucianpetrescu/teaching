//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

//material
uniform vec3 material_color;
uniform vec4 material_constants;

//light and material uniforms
uniform vec3 light_color;
uniform vec3 light_position;
uniform vec3 light_attenuation;

//camera
uniform vec3 camera_position;

//input from vetex shader
in highp vec3 position_ws, normal_ws;

void main(){

	// light source to position
	vec3 L = normalize(light_position - position_ws);
	// fragment normal
	vec3 N = normalize(normal_ws);						//normalize here again, why?, linear not spherical interpolation of fragment attributes
	
	//attenuation
	float dist = distance(light_position, position_ws);
	float attenuation = 1.0f / (light_attenuation.x + light_attenuation.y * dist + light_attenuation.z * dist * dist);

	// constants
	float Ka = material_constants.x;
	float Kd = material_constants.y;
	float Ks = material_constants.z;
	float Specularity = material_constants.w;
	
	//total illumination
	vec3 illumination = Ka * light_color + Kd * max(dot(L,N),0) * light_color;
	if(dot(L,N)>=0){
		vec3 R = reflect(-L,N);	//the reflected vector has to be incident!
		vec3 V = normalize(camera_position - position_ws);
		//illumination += pow(max(dot(R,V),0),10) *light_color;
		illumination += Ks * pow(max(dot(R,V),0),Specularity) * light_color; 
	}

	//output color
	colorout = illumination * material_color;
}

