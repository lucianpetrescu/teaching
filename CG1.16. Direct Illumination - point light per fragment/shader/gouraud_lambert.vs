//this shader illuminates geometry with the lambert reflection model and with gouraud shading
#version 450

//attributes
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

//transformations
uniform mat4 model_matrix, model_normal_matrix, view_matrix, projection_matrix;

//light and material uniforms
uniform vec3 light_color;
uniform vec3 light_position;
uniform vec3 light_attenuation;

//material
uniform vec4 material_constants;

//output to fragment shader
out highp vec3 vs_illumination;

void main(){

	//we need to work in a single coordinate system (using world space)
	//light_position is in world space already
	vec3 position_ws = (model_matrix * vec4(in_position, 1)).xyz;
	//vec3 normal_ws = normalize(mat3(model_normal_matrix) * in_normal);
	vec3 normal_ws = normalize(mat3(model_matrix) * in_normal);

	//position to light source
	vec3 L = normalize(light_position - position_ws);
	vec3 N = normalize(normal_ws);
	
	//attenuation
	float dist = distance(light_position, position_ws);
	float attenuation = 1.0f / (light_attenuation.x + light_attenuation.y * dist + light_attenuation.z * dist * dist);

	// constants
	float Ka = material_constants.x;
	float Kd = material_constants.y;
	
	//total illumination
	vs_illumination = Ka * light_color + Kd * max(dot(L,N),0) * light_color * attenuation;

	//send vertex to rasterizer
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position,1);
}

