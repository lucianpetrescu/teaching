###Screenshots###

NOTE: the color banding on the ground is not a bug, it happens because the 256 color hues provided by the 24-bit color depth (8 bit red, 8 bit green, 8 bit blue) are not enough to represent the resulting floor gradient.

gouraud shading with attenuation
![results](img/output.png)
phong shading with attenuation. Notice the specular highlights, and how they move with the viewer.
![results](img/output_2.png)
