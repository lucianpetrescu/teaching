###Screenshots###

NOTE: the color banding on the ground is not a bug, it happens because the 256 color hues provided by the 24-bit color depth (8 bit red, 8 bit green, 8 bit blue) are not enough to represent the resulting floor gradient.

![results](img/output.png)

