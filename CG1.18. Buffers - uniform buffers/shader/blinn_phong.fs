//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;


//object id
uniform int id;
//static data
layout(column_major, std140, binding = 0) uniform UboStaticData {
	mat4 model_matrix[4];
	vec4 material_constants[4];
	vec4 material_color[4];
	vec4 light_direction;
	vec4 light_angles;
	vec4 light_color;
	vec4 light_attenuation;
};
//dynamic data
layout(column_major, std140, binding = 1) uniform UboDynamicData {
	mat4 view_matrix, projection_matrix;
	vec4 light_position;
	uvec4 light_falloff;
	vec4 camera_position;
};

//input from vetex shader
in vec3 position_ws, normal_ws;

void main(){

	//position to light source
	vec3 L = normalize(light_position.xyz - position_ws);
	vec3 N = normalize(normal_ws);

	// constants
	float Ka = material_constants[id].x;
	float Kd = material_constants[id].y;
	float Ks = material_constants[id].z;
	float specularity = material_constants[id].w;
	vec3 ambient = vec3(0,0,0);
	vec3 diffuse = vec3(0,0,0);
	vec3 specular =vec3(0,0,0);
	
	//spot
	vec3 D = normalize(light_direction.xyz);
	float cosu = dot(-L,D);
	float cos_inner = cos(light_angles.x);
	float cos_outer = cos(light_angles.y);
	
	//if in spot
	if( cosu >= cos_outer){

		//attenuation
		float dist = distance(light_position.xyz, position_ws);
		float attenuation = 1.0f / (light_attenuation.x + light_attenuation.y * dist + light_attenuation.z * dist * dist);
			
		//falloff
		float falloff = 1.0f;
		if(light_falloff.x > 0 && cosu < cos_inner){
			falloff = clamp((cosu - cos_outer) / (cos_inner-cos_outer), 0.0, 1.0);
		}

		//total illumination
		ambient = Ka * light_color.xyz;
		diffuse = Kd * max(dot(L,N),0) * light_color.xyz * attenuation * falloff;
		specular = vec3(0,0,0);
		if(dot(L,N)>=0){
			vec3 R = reflect(-L, N);
			vec3 V = normalize(camera_position.xyz - position_ws);
			specular = Ks * pow ( max(dot(R,V),0), specularity ) * light_color.xyz * attenuation * falloff;
		}
	}else{
		ambient = Ka* light_color.xyz;
	}

	//final color
	colorout = material_color[id].xyz * (ambient + diffuse + specular);
}

