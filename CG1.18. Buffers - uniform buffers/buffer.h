///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
// module needs OpenGL and wic provides it
#include "../dependencies/lap_wic/lap_wic.hpp"

namespace lab{

	class Buffer {
	public:
		//ctor
		// storage hint has to be a reasonable combination between: GL_STREAM_DRAW, GL_STREAM_READ, GL_STREAM_COPY, GL_STATIC_DRAW, GL_STATIC_READ, GL_STATIC_COPY, GL_DYNAMIC_DRAW, GL_DYNAMIC_READ, or GL_DYNAMIC_COPY
		Buffer(GLenum storage_hint);
		// storage hint has to be a reasonable combination between: GL_STREAM_DRAW, GL_STREAM_READ, GL_STREAM_COPY, GL_STATIC_DRAW, GL_STATIC_READ, GL_STATIC_COPY, GL_DYNAMIC_DRAW, GL_DYNAMIC_READ, or GL_DYNAMIC_COPY
		Buffer(size_t size, GLenum storage_hint);
		// storage hint has to be a reasonable combination between: GL_STREAM_DRAW, GL_STREAM_READ, GL_STREAM_COPY, GL_STATIC_DRAW, GL_STATIC_READ, GL_STATIC_COPY, GL_DYNAMIC_DRAW, GL_DYNAMIC_READ, or GL_DYNAMIC_COPY
		Buffer(void* data, size_t offset, size_t size, GLenum storage_hint);
		Buffer(const Buffer&) = delete;
		Buffer(Buffer&&) = delete;
		Buffer& operator=(const Buffer&) = delete;
		Buffer& operator=(Buffer&&) = delete;
		~Buffer();

		//to and from gpu
		void resize(size_t buffersize);
		void copy(void* dst, size_t gpubufferoffset, size_t datasize) const;
		//the following condition applies : (bufferoffset + datasize) < buffersize 
		void update(void* data, size_t gpubufferoffset, size_t datasize);
		void update(void* data);

		//binds
		void bind(GLenum target);
		void bindIndexed(GLenum target, unsigned int index);
		void bindIndexed(GLenum target, unsigned int index, size_t offset, size_t size);

		//maps
		void* map(size_t offset, GLenum access);
		void unmap();

		//raw access
		GLuint getGLobject() const;
		size_t getSize() const;
		GLenum getStorageHint() const;
	private:
		GLuint globject = 0;
		size_t size = 0;
		GLenum storage_hint = 0;
	};
}
