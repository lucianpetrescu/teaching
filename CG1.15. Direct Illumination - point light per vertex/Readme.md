###Screenshots###


wireframe, gouraud shading works best on highly detailed geometry 
![results](img/output.png)
solid view, gouraud shading works best on highly detailed geometry
![results](img/output_2.png)
directional lighting on horizontal surface creates a strong gradient. If you intend to do this
in production code you should handle such situations (e.g. tessellation, dithering, etc)
![results](img/output_3.png)
