//this shader illuminates geometry with the lambert reflection model and with gouraud shading
#version 450

//attributes
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

//transformations
uniform mat4 model_matrix;
uniform mat4 model_normal_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

//light and material uniforms
uniform vec3 light_color;
uniform vec3 light_position;
uniform vec2 material_constants;

//output to fragment shader
out vec3 vs_illumination;

void main(){

	//we need to work in a single coordinate system (using world space)
	//light_position is in world space already
	vec3 position_ws = (model_matrix * vec4(in_position, 1)).xyz;
	vec3 normal_ws = mat3(model_normal_matrix) * in_normal;
	//vec3 normal_ws = mat3(model_matrix) * in_normal;

	// position to light source
	vec3 L = normalize(light_position - position_ws);
	// vertex normal
	vec3 N = normalize(normal_ws);
	
	// constants
	float Ka = material_constants.x;
	float Kd = material_constants.y;
	
	//total illumination
	vs_illumination = Ka * light_color + Kd * dot(L,N) * light_color;

	//send vertex to rasterizer
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position,1);
}

