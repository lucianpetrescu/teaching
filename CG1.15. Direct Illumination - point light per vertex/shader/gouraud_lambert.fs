//this shader illuminates geometry with the lambert reflection model and with gouraud shading
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

uniform vec3 color;
in vec3 vs_illumination;

void main(){
	colorout = vs_illumination * color;
}

