//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

//texture 
layout(binding =0) uniform sampler2D sampler_texture;

//uniform data
uniform int color_mode;
uniform int id; 
layout(column_major, std140, binding = 0) uniform UboData {
	vec4 material_constants[2];
	vec4 light_color;
	mat4 model_matrix[2];
	vec4 light_position;
	mat4 view_matrix, projection_matrix;
	vec4 camera_position;
};

//input from vetex shader
in vec3 position_ws, normal_ws;
in vec2 texcoords;

void main(){

	//position to light source
	vec3 L = normalize(light_position.xyz - position_ws);
	vec3 N = normalize(normal_ws);

	// constants
	vec4 mc = material_constants[id];
	float Ka = mc.x;
	float Kd = mc.y;
	float Ks = mc.z;
	float specularity = mc.w;
	
	//total illumination
	vec3 ambient = Ka * light_color.xyz;
	vec3 diffuse = Kd * max(dot(L,N),0) * light_color.xyz;
	vec3 specular = vec3(0,0,0);
	if(dot(L,N)>=0){
		vec3 R = reflect(-L, N);
		vec3 V = normalize(camera_position.xyz - position_ws);
		specular = Ks * pow ( max(dot(R,V),0), specularity ) * light_color.xyz;
	}

	//find color of the material
	vec3 material_color = texture(sampler_texture, texcoords).xyz;
	// combine with illumination
	vec3 pixel_color = material_color * (ambient + diffuse + specular);

	//output mode
	if(color_mode == 1){
		colorout = pixel_color;
	}
	else{
		colorout = vec3(texcoords, 0);
	}
}

