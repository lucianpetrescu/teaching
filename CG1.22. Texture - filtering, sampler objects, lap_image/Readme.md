###Screenshots###

filtering is the process through which the information in the texture map is applied to a mapped geometric location.

NOTE: observe the near and far pixels on the ground (texture with grass). This should be done at fullscreen resolution.

filtering : near
![](img/output.png)

filtering : bilinear
![](img/output_2.png)

filtering : trilinear
![](img/output_3.png)

filtering : trilinear anisotropic 4x
![](img/output_4.png)

filtering : trilinear anisotropic 16x
NOTE: while there is barely any noticeable difference between 16x and 4x, 16x has its uses. Anisotropic filtering is **very** cheap, as it is hardware performed. Don't compare it to a standard anisotropic filter like EWA.
![](img/output_5.png)