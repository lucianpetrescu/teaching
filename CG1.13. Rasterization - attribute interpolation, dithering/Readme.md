###Screenshots###

red cube = bad winding, green cube = good winding

no dithering, long view range, no artifacts
![results](img/output.png)
no dithering, medium view range, no artifacts
![results](img/output_2.png)
no dithering, close view range, minor banding observable
![results](img/output_3.png)
no dithering, near view range, strong banding artifacts
![results](img/output_4.png)



using dithering, long view range, no artifacts
![results](img/output.png)
using dithering, medium view range, no artifacts
![results](img/output_2.png)
using dithering, close view range, no artifacts
![results](img/output_3.png)
using dithering, near view range, no artifacts
![results](img/output_4.png)