
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

//uniforms
uniform uint bayer_dithering_mode;

//from vertex shader (interpolated!)
in vec3 vs_color;

// dithering functions
// more informaton at https://en.wikipedia.org/wiki/Ordered_dithering
// note: masks are [0,range] instead of [1,range+1]
// basically the screen is mapped to a tiled NxN dithering kernel/mask/matrix and each pixel color is disturbed with the dithering noise
// this method changes the nature of the noise, from STRUCTURED NOISE (banding) to PSEUDO-RANDOM NOISE (dithering)

vec3 dither2x2(vec3 origcolor){
	const int mask[2][2]={
	{0, 2},
	{3, 1}};
	ivec2 maskcoord = ivec2(mod(gl_FragCoord.xy, vec2(2,2)));
	float dither = mask[maskcoord.x][maskcoord.y] / 4.0f / 255.0f;
	return origcolor + vec3(dither, dither, dither);
}
vec3 dither3x3(vec3 origcolor){
	const int mask[3][3] = {
	{0, 7, 3},
	{6, 5, 2},
	{4, 1, 8}};
	ivec2 maskcoord = ivec2(mod(gl_FragCoord.xy, vec2(3,3)));
	float dither = mask[maskcoord.x][maskcoord.y] / 9.0f / 255.0f;
	return origcolor + vec3(dither, dither, dither);
}
vec3 dither4x4(vec3 origcolor){
	const int mask[4][4] = {
	{0, 8, 2, 10},
	{12, 4, 14, 6},
	{3, 11, 1, 9},
	{15, 7, 13, 5}};
	ivec2 maskcoord = ivec2(mod(gl_FragCoord.xy, vec2(4,4)));
	float dither = mask[maskcoord.x][maskcoord.y] / 16.0f / 255.0f;
	return origcolor + vec3(dither, dither, dither);
}
vec3 dither8x8(vec3 origcolor){
	const int mask[8][8] = {
	{ 0, 32, 8, 40, 2, 34, 10, 42}, 
	{48, 16, 56, 24, 50, 18, 58, 26},
	{12, 44, 4, 36, 14, 46, 6, 38}, 
	{60, 28, 52, 20, 62, 30, 54, 22},
	{ 3, 35, 11, 43, 1, 33, 9, 41}, 
	{51, 19, 59, 27, 49, 17, 57, 25},
	{15, 47, 7, 39, 13, 45, 5, 37},
	{63, 31, 55, 23, 61, 29, 53, 21} }; 
	ivec2 maskcoord = ivec2(mod(gl_FragCoord.xy, vec2(8,8)));
	float dither = mask[maskcoord.x][maskcoord.y] / 64.0f / 255.0f;
	return origcolor + vec3(dither, dither, dither);
}

void main(){
	switch(bayer_dithering_mode){
	case 0: colorout = vs_color;			break;	//no dithering
	case 1: colorout = dither2x2(vs_color);	break;	//2x2
	case 2: colorout = dither3x3(vs_color);	break;	//3x3
	case 3: colorout = dither4x4(vs_color);	break;	//4x4
	case 4: colorout = dither8x8(vs_color);	break;	//8x8
	}
}

