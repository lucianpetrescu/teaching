
#version 450

//get data from attribute pipe #0, interpret it as vec2 
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_color;

//uniforms
uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

//to fragment shader (interpolated)
out vec3 vs_color;

void main(){

	vs_color = in_color;
	
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position,1);
}

