///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/transform.hpp"
#include "../dependencies/glm/gtx/transform2.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"
#include "shader.h"
#include "camera.h"

namespace lab {
	class CGLab {
		Shader* shader = nullptr;
		GLuint vbo1 = 0, vbo2 =0, ibo = 0, vao = 0;
		glm::mat4 model_matrix, projection_matrix;
		Camera camera;
		unsigned int framebuffer_width = 0, framebuffer_height = 0;

		//toggles
		bool toggle_wireframe = false;
		unsigned int bayer_dithering_mode = 0;
	public:
		CGLab(unsigned int width, unsigned int height) {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;

			//state
			glClearColor(0.2f, 0.2f, 0.2f, 0);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glFrontFace(GL_CCW);

			//shader
			shader = new Shader("../shader/interpolation.vs", "../shader/interpolation.fs");

			//the model matrix takes the vertices from their definition/object space into the scene/world space
			//change it with keys
			model_matrix = glm::mat4(1);

			//view matrix is controlled by camera
			camera.set(glm::vec3(10, 20, 50), glm::vec3(0, 20, 0), glm::vec3(0, 1, 0));

			//the projection matrix is defined by the framebuffer viewport aspect (width / height)
			framebuffer_width = width;
			framebuffer_height = height;
			projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);

			//----------------------------------------------------------------------------------------------------
			//vertex buffer - with positions
			float vertices[12] = {
				  0, 0,	 0,
				  0, 0,	100,
				100, 0, 100,
				100, 0,	 0 };
			glCreateBuffers(1, &vbo1);	
			glNamedBufferData(vbo1, sizeof(float) * 12, vertices, GL_STATIC_DRAW);
			
			//another vertex buffer - with colors
			float colors[12] = {
				1, 1, 1,
				1, 1, 1,
				0, 0, 0,
				0, 0, 0 };
			glCreateBuffers(1, &vbo2);
			glNamedBufferData(vbo2, sizeof(float) * 12, colors, GL_STATIC_DRAW);
			
			//index buffer
			unsigned int indices[6] = { 
				0, 1, 2,
				2, 3, 0};
			glCreateBuffers(1, &ibo);	
			glNamedBufferData(ibo, sizeof(unsigned int) * 6, indices, GL_STATIC_DRAW);

			//vertex array buffer
			glCreateVertexArrays(1, &vao);	
			//attribute #0 comes from vbo1 (on binding point #5)
			glEnableVertexArrayAttrib(vao, 0);
			glVertexArrayVertexBuffer(vao, 5, vbo1, 0, sizeof(float) * 3);
			glVertexArrayAttribBinding(vao, 0, 5);						
			glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
			//attribute #1 comes from vbo2 (on binding point #3)
			glEnableVertexArrayAttrib(vao, 1);
			glVertexArrayVertexBuffer(vao, 3, vbo2, 0, sizeof(float) * 3);
			glVertexArrayAttribBinding(vao, 1, 3);
			glVertexArrayAttribFormat(vao, 1, 3, GL_FLOAT, GL_FALSE, 0);
			//ibo
			glVertexArrayElementBuffer(vao, ibo);						
		}
		~CGLab() {
			delete shader;
			glDeleteBuffers(1, &vbo1);
			glDeleteBuffers(1, &vbo2);
			glDeleteBuffers(1, &ibo);
			glDeleteVertexArrays(1, &vao);
			std::cout << "[LAB] Destroyed" << std::endl << "----------------------------------------------" << std::endl;
		}
		void render() {
			//viewport
			glViewport(0, 0, framebuffer_width, framebuffer_height);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			//render mode
			if (toggle_wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			
			//use shader
			shader->bind();
			shader->setUniform("bayer_dithering_mode", bayer_dithering_mode );
			shader->setUniform("model_matrix", model_matrix);
			shader->setUniform("view_matrix", camera.getViewMatrix());
			shader->setUniform("projection_matrix", projection_matrix);
			glBindVertexArray(vao);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			framebuffer_width = width;
			framebuffer_height = height;
			projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			//reload all shaders
			switch (key) {
			case lap::wic::Key::SPACE:
				std::cout << "-------------------------------------" << std::endl << "[LAB] reloading shader" << std::endl;
				shader->reload();
				break;
			case lap::wic::Key::ESCAPE: wnd.close();			break;//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
			case lap::wic::Key::A:
			case lap::wic::Key::LEFT:
				camera.translateRight(-0.5f);
				break;
			case lap::wic::Key::D:
			case lap::wic::Key::RIGHT:
				camera.translateRight(0.5f);
				break;
			case lap::wic::Key::W:
			case lap::wic::Key::UP:
				camera.translateForward(0.5f);
				break;
			case lap::wic::Key::S:
			case lap::wic::Key::DOWN:
				camera.translateForward(-0.5f);
				break;
			case lap::wic::Key::R:
				camera.translateUpward(0.5f);
				break;
			case lap::wic::Key::F:
				camera.translateUpward(-0.5f);
				break;

			//toggle rasterization mode
			case lap::wic::Key::NUM1:
				toggle_wireframe = !toggle_wireframe;
				break;
			//toggle dithering
			case lap::wic::Key::NUM2:
				bayer_dithering_mode = (bayer_dithering_mode + 1) % 5;
				break;

			default:
				break;
			}
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			keyPress(wnd, key, alt, control, shift, system, state, timestamp);
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
			float dx = wnd.getWindowProperties().width / 2.0f - posx;
			float dy = wnd.getWindowProperties().height / 2.0f - posy;
			if (state.mod_shift) {
				camera.rotateTPSoY(dx / 10.0f, 40);
				camera.rotateTPSoX(dy / 10.0f, 40);
			}
			else {
				camera.rotateFPSoY(dx / 10.0f);
				camera.rotateFPSoX(dy / 10.0f);
			}
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	
	};
}



int main(int argc, char* argv[]) {
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Rasterization - attribute interpolation";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	fp.samples_per_pixel = 4;	//request multiple samples per pixel
	cp.swap_interval = -1;
	ip.cursor_enabled = false;	//cursor is not visible and always reverts to middle of the screen after movement
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//lab object
	lab::CGLab lab(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {
		lab.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};
}