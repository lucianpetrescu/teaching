///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/rotate_vector.hpp"
#include "../dependencies/glm/gtx/transform.hpp"
#include "../dependencies/glm/gtx/transform2.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"
#include "shader.h"
#include "camera.h"
#include "mesh.h"
#include "texture.h"

const std::string ASSET_PATH = "../../assets/";

namespace lab {
	class CGLab {
		unsigned int framebuffer_width = 0, framebuffer_height = 0;
		Camera camera;
		Mesh mesh[2];
		Texture2D texture[12];
		TextureSampler texture_sampler;
		Shader shader;

		//toggles
		bool toggle_pause = false;
		unsigned int toggle_output = 1;

		// use a single dynamic ubo, for conveniece
		Buffer ubo{ GL_DYNAMIC_STORAGE_BIT | GL_MAP_READ_BIT };
		struct UboData {
			glm::vec4 material_constants[4] = {
				glm::vec4(0.05f, 0.80f, 0.15f, 16.0f),
				glm::vec4(0.05f, 0.80f, 0.15f, 16.0f),
				glm::vec4(0.05f, 0.80f, 0.15f, 16.0f),
				glm::vec4(0.05f, 0.80f, 0.15f, 1.0f)
			};
			glm::vec4 light_color = glm::vec4(1, 1, 1, 0);
			glm::mat4 model_matrix[4] = {
				glm::translate(glm::mat4(1), glm::vec3(-50, 0, 0)),
				glm::mat4(1),
				glm::translate(glm::mat4(1), glm::vec3( 50, 0, 0)),
				glm::mat4(1),
			};
			glm::vec4 light_position = glm::vec4(-1, -1, -1, 0);	//directional lighting
			glm::mat4 view_matrix, projection_matrix;
			glm::vec4 camera_position;
		}ubodata;


	public:
		CGLab(unsigned int width, unsigned int height) {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;
			//window
			framebuffer_width = width;
			framebuffer_height = height;

			//state
			glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
			glEnable(GL_DEPTH_TEST);

			//shader
			shader.load("../shader/blinn_phong_multi_textured.vert", "../shader/blinn_phong_multi_textured.frag");

			//transformations
			camera.set(glm::vec3(10, 20, 50), glm::vec3(0, 20, 0), glm::vec3(0, 1, 0));
			ubodata.projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);

			//load some objects
			mesh[0].load(ASSET_PATH + "models/bamboo/bamboo.obj");
			texture[0].load(ASSET_PATH + "models/bamboo/bamboo_color.jpg", 3);				//R+G+B
			texture[1].load(ASSET_PATH + "models/bamboo/bamboo_specular.jpg", 1);			//R=specularity
			texture[2].load(ASSET_PATH + "models/bamboo/bamboo_alpha.jpg", 1);				//R=alpha
			texture[3].load(ASSET_PATH + "models/bamboo/bamboo_normals.jpg", 2);			//R=x, G=y, B=z(reconstructable)
			texture[4].load(ASSET_PATH + "models/bamboo/bamboo_displacement.jpg", 1);		//R=displacement
			texture[5].load(ASSET_PATH + "models/bamboo/bamboo_ambient_occlusion.jpg", 1);	//R=ao
			mesh[1].load(ASSET_PATH + "models/dirt/dirt.obj");
			texture[6].load(ASSET_PATH + "models/dirt/dirt_color.jpg", 3);					//R+G+B
			texture[7].load(ASSET_PATH + "models/dirt/dirt_specular.jpg", 1);				//R=specularity
			texture[8].load(ASSET_PATH + "models/dirt/dirt_alpha.jpg", 1);					//R=alpha
			texture[9].load(ASSET_PATH + "models/dirt/dirt_normals.jpg", 2);				//R=x, G=y, B=z(reconstructable)
			texture[10].load(ASSET_PATH + "models/dirt/dirt_displacement.jpg", 1);			//R=displacement
			texture[11].load(ASSET_PATH + "models/dirt/dirt_ambient_occlusion.jpg", 1);		//R=ao

			//uniform buffer objects
			ubo.resize(sizeof(UboData));

			//texture sampler (bind it all the units which will be used)
			texture_sampler.setTrilinearAnisotropic(16);
			texture_sampler.bind(0);
			texture_sampler.bind(1);
			texture_sampler.bind(2);
			texture_sampler.bind(3);
			texture_sampler.bind(4);
			texture_sampler.bind(5);
		}
		~CGLab() {
			//raii
			std::cout << "[LAB] Destroyed" << std::endl << "----------------------------------------------" << std::endl;
		}
		void render() {
			//non-rendering related
			static int delta = 0;
			if (!toggle_pause) delta = (delta + 1) % 600;
			
			//state
			glViewport(0, 0, framebuffer_width, framebuffer_height);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

			//update the ubo data and send it to the gpu
			ubodata.light_position = glm::vec4(glm::rotateY(glm::vec3(100.0f, 200.0f, 100.0f), glm::radians((float)delta * 10)),1);
			ubodata.view_matrix = camera.getViewMatrix();
			ubodata.camera_position = glm::vec4(camera.getPostion(), 1);
			ubo.update(&ubodata);
			ubo.bindIndexed(GL_UNIFORM_BUFFER, 0);

			//use shader
			shader.bind();
			shader.setUniform("toggle_output", (int)toggle_output);
			
			//bamboos
			for (int i = 0; i < 6; i++) texture[i].bindAsTexture(i);
			for (int i = 0; i < 3; i++) {
				shader.setUniform("id", i);
				mesh[0].draw(GL_TRIANGLES);
			}

			//ground
			for (int i = 0; i < 6; i++) texture[i+6].bindAsTexture(i);
			shader.setUniform("id", 3);
			mesh[1].draw(GL_TRIANGLES);

			//check each frame
			if (glGetError() != GL_NO_ERROR) std::cout << "[LAB] WARNING: OpenGL errors were found " << std::endl;
		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			framebuffer_width = width;
			framebuffer_height = height;
			ubodata.projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			//reload all shaders
			switch (key) {
			case lap::wic::Key::SPACE:
				std::cout << "-------------------------------------" << std::endl << "[LAB] reloading shader" << std::endl;
				shader.reload();
				break;
			case lap::wic::Key::ESCAPE: wnd.close();			break;//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
			case lap::wic::Key::A:
			case lap::wic::Key::LEFT:
				camera.translateRight(-0.5f);
				break;
			case lap::wic::Key::D:
			case lap::wic::Key::RIGHT:
				camera.translateRight(0.5f);
				break;
			case lap::wic::Key::W:
			case lap::wic::Key::UP:
				camera.translateForward(0.5f);
				break;
			case lap::wic::Key::S:
			case lap::wic::Key::DOWN:
				camera.translateForward(-0.5f);
				break;
			case lap::wic::Key::R:
				camera.translateUpward(0.5f);
				break;
			case lap::wic::Key::F:
				camera.translateUpward(-0.5f);
				break;
			//different types of filtering
			case lap::wic::Key::NUM1:
				toggle_output = 1;
				std::cout << "Showing TEXCOORDS" << std::endl;
				break;
			case lap::wic::Key::NUM2:
				toggle_output = 2;
				std::cout << "Showing COLOR" << std::endl;
				break;
			case lap::wic::Key::NUM3:
				toggle_output = 3;
				std::cout << "Showing SPECULAR" << std::endl;
				break;
			case lap::wic::Key::NUM4:
				toggle_output = 4;
				std::cout << "Showing ALPHA" << std::endl;
				break;
			case lap::wic::Key::NUM5:
				toggle_output = 5;
				std::cout << "Showing NORMALS" << std::endl;
				break;
			case lap::wic::Key::NUM6:
				toggle_output = 6;
				std::cout << "Showing DISPLACEMENT" << std::endl;
				break;
			case lap::wic::Key::NUM7:
				toggle_output = 7;
				std::cout << "Showing AMBIENT OCCLUSION" << std::endl;
				break;
			case lap::wic::Key::NUM8:
				toggle_output = 8;
				std::cout << "Showing illuminated" << std::endl;
				break;
			//pause light
			case lap::wic::Key::P:
				toggle_pause = !toggle_pause;
				break;
			default:
				break;
			}
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			keyPress(wnd, key, alt, control, shift, system, state, timestamp);
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
			float dx = wnd.getWindowProperties().width / 2.0f - posx;
			float dy = wnd.getWindowProperties().height / 2.0f - posy;
			if (state.mod_shift) {
				camera.rotateTPSoY(dx / 10.0f, 40);
				camera.rotateTPSoX(dy / 10.0f, 40);
			}
			else {
				camera.rotateFPSoY(dx / 10.0f);
				camera.rotateFPSoX(dy / 10.0f);
			}
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	
	};
}



//debug function based on glDebugOutput
void APIENTRY debugFunctionCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
	//ignore some type of warnings (otherwise the glsl compiler might spam the console with notifications)
	if (type == GL_DEBUG_TYPE_PERFORMANCE || type == GL_DEBUG_TYPE_PORTABILITY || severity == GL_DEBUG_SEVERITY_NOTIFICATION) return;

	std::cout << "-------------------------------------------------------------------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:				std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:	std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:		std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:		std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:				std::cout << "Source: Other"; break;
	}
	std::cout << std::endl;
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	}
	std::cout << std::endl;
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:			std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:			std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:				std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:	std::cout << "Severity: notification"; break;
	}
	std::cout << std::endl;
	std::cout << "Press any key" << std::endl;
	std::cin.get();
}



int main(int argc, char* argv[]) {
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Textures - multitexturing, alpha culling, types of textures";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	fp.samples_per_pixel = 4;	//request multiple samples per pixel
	cp.swap_interval = -1;
	cp.debug_context = true;
	ip.cursor_enabled = false;	//cursor is not visible and always reverts to middle of the screen after movement
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//use the current context to attach a debugging function, this is only possible if the debug flags are enabled
	GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(debugFunctionCallback, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
	
	//lab object
	lab::CGLab lab(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {
		lab.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};

}