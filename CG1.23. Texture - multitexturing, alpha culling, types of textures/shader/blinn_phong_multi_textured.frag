#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

//texture 
layout(binding = 0) uniform sampler2D sampler_color;
layout(binding = 1) uniform sampler2D sampler_specular;
layout(binding = 2) uniform sampler2D sampler_alpha;
layout(binding = 3) uniform sampler2D sampler_normals;
layout(binding = 4) uniform sampler2D sampler_displacement;
layout(binding = 5) uniform sampler2D sampler_ambient_occlusion;

//uniform data
uniform int toggle_output;
uniform int id; 
layout(column_major, std140, binding = 0) uniform UboData {
	vec4 material_constants[4];
	vec4 light_color;
	mat4 model_matrix[4];
	vec4 light_position;
	mat4 view_matrix, projection_matrix;
	vec4 camera_position;
};

//input from vetex shader
in vec3 position_ws, normal_ws;
in vec2 texcoords;

void main(){

	colorout = vec3(0,0,0);

	switch(toggle_output){
	case 1:	//TEXCOORDS
		colorout = vec3(texcoords, 0);
		break;
	case 2:	//COLOR
		colorout = texture(sampler_color, texcoords).xyz;
		break;
	case 3:	//SPECULAR
		colorout = texture(sampler_specular, texcoords).xxx;
		break;
	case 4:	//ALPHA
		colorout = texture(sampler_alpha, texcoords).xxx;
		break;
	case 5:	//NORMALS 
		//normal components are in [-1, 1] interval, not representable in the [0,255] interval, they need packing and unpacking
		vec2 nread = texture(sampler_normals, texcoords).xy;
		vec2 nexpand = 2*nread-1;
		vec3 n = vec3(nexpand.x, nexpand.y, sqrt(1-nexpand.x*nexpand.x-nexpand.y*nexpand.y));
		vec3 noutput = (n+1)/2;
		colorout = noutput;
		break;
	case 6:	//DISPLACEMENT
		colorout = texture(sampler_displacement, texcoords).xxx;
		break;
	case 7:	//AMBIENT OCCLUSION
		colorout = texture(sampler_ambient_occlusion, texcoords).xxx;
		break;
	case 8:	//COLOR + ALPHA + SPECULAR + AO + ILLUMINATION
		
		//check if this fragment is renderable (alpha test), DISCARD kills the fragment.
		float alpha = texture(sampler_alpha, texcoords).x;
		if(alpha < 0.1) discard;

		//position to light source
		vec3 L = normalize(light_position.xyz - position_ws);
		vec3 N = normalize(normal_ws);

		// constants, use textures
		vec4 mc = material_constants[id];
		float Ka = mc.x;
		float Kd = mc.y;
		float Ks = mc.z;
		float specularity =mc.w;
	
		//total illumination
		float ao = texture(sampler_ambient_occlusion, texcoords).x;
		vec3 ambient = ao * Ka * light_color.xyz;
		vec3 diffuse = Kd * max(dot(L,N),0) * light_color.xyz;
		vec3 specular = vec3(0,0,0);
		if(dot(L,N)>=0){
			vec3 R = reflect(-L, N);
			vec3 V = normalize(camera_position.xyz - position_ws);
			float tex_spec = texture(sampler_specular, texcoords).x;
			specular = tex_spec * Ks * pow ( max(dot(R,V),0), specularity ) * light_color.xyz;
		}

		// combine with illumination
		vec3 material_color = texture(sampler_color,texcoords).xyz;
		colorout = material_color * (ambient + diffuse + specular);
		break;
	default:
		break;
	}

}

