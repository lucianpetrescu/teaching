#version 450

//attributes
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoords;


//uniform data
uniform int id; 
layout(column_major, std140, binding = 0) uniform UboData {
	vec4 material_constants[4];
	vec4 light_color;
	mat4 model_matrix[4];
	vec4 light_position;
	mat4 view_matrix, projection_matrix;
	vec4 camera_position;
};


//output to fragment shader
out vec3 position_ws, normal_ws;
out vec2 texcoords;

void main(){

	position_ws = (model_matrix[id] * vec4(in_position, 1)).xyz;
	normal_ws = normalize(mat3(model_matrix[id]) * in_normal);
	texcoords = in_texcoords;

	gl_Position = projection_matrix * view_matrix * model_matrix[id] * vec4(in_position,1);
}

