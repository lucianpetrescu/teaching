###Screenshots###

there are different types of texture maps, which can (and are) used to enhance the realism of a scene. In this lab we will introduce some of them.

texture coordinates
![](img/output.png)

color/diffuse/albedo maps
![](img/output_2.png)

specular maps
![](img/output_3.png)

alpha maps
![](img/output_4.png)

normal maps
![](img/output_5.png)

displacement maps
![](img/output_6.png)

ambient occlusion maps
![](img/output_7.png)

the final illuminated scene (using color, specular, alpha and ambient occlusion maps)
![](img/output_8.png)