///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"
#include "shader.h"

namespace lab {
	class CGLab {
		Shader* shader;
		GLuint vbo, ibo, vao;
	public:
		CGLab() {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;

			//state
			glClearColor(0, 0, 0, 0);
			glPointSize(15.0f);

			//shader
			shader = new Shader("../shader/vf.vs", "../shader/vf.fs");

			//buffers
			//vertex buffer object <- stores vertices (it does not say HOW these vertices are interpreted from a topological standpoint)
			float vertices[12] = { 0,0,  0,0.8f,  0.8f,0,  -0.8f,-0.8f,  -0.8f,0,  0,-0.8f };
			glCreateBuffers(1, &vbo);	
			glNamedBufferData(vbo, sizeof(float) * 12, vertices, GL_STATIC_DRAW);

			//index buffer object <- stores indices (WHICH indices in the vertex buffer interpreted. Still does not say HOW)
			unsigned int indices[6] = { 0, 1, 2, 5, 4, 3 };
			glCreateBuffers(1, &ibo);	
			glNamedBufferData(ibo, sizeof(unsigned int) * 6, indices, GL_STATIC_DRAW);

			//vao <-vertex array object, stores drawing state (in order for OpenGL to draw primitives it needs to know where can it find the vertices, the indices, how to interpret the vertices, where to send them etc)
			const bool modernvaos = true;
			if (!modernvaos) {
				//OLD WAY : attribute index -> buffer
				glGenVertexArrays(1, &vao);										//generate the vao
				glBindVertexArray(vao);											//bind the vao
				glBindBuffer(GL_ARRAY_BUFFER, vbo);								//bind the vertex buffer to the vao GL_ARRAY_BUFFER binding point (vao knows that vertices will be read from here)
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);						//bind the index buffer to the vao GL_ELEMENT_ARRAY_BUFFER binding point (vao knows that indices will be read from here)
				glEnableVertexAttribArray(0);									//enable the attribute index #0
				glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);	//specify how the vertex data is interpreted for attribute index #0
																				// on index attribute index 0, send two values of type float, unnormalized
																				// the distance between vertex entries is 2*sizeof(float) and the first read is performed at offset 0
			}
			else {
				//NEW WAY : attribute index -> buffer index -> buffer
				glCreateVertexArrays(1, &vao);									// create the vao
				glEnableVertexArrayAttrib(vao, 0);								// enable the attribute pipe #0 in vao.
				glVertexArrayVertexBuffer(vao, 5, vbo, 0, sizeof(float) * 2);	// bind the vbo to the vao, on the buffer index #5 (not attribute binding point!)
																				// the first read is performed at offset 0, and the distance between reads is 2*sizeof(float)
				glVertexArrayAttribBinding(vao, 0, 5);							// make attribute pipe #0 use data from the buffer index #5
				glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, GL_FALSE, 0);	// define HOW the vertex buffer bound to the buffer index #5 and linked to the attribute pipe #0 is interpreted
																				// Each vertex has 2 values of type float, the values are not normalized and the first read is from offset 0
				glVertexArrayElementBuffer(vao, ibo);							// vao will read indices from ibo
			}
		}
		~CGLab() {
			delete shader;
			glDeleteBuffers(1, &vbo);
			glDeleteBuffers(1, &ibo);
			glDeleteVertexArrays(1, &vao);
			std::cout << "[LAB] Destroyed" << std::endl << "----------------------------------------------" << std::endl;
		}
		void render() {
			//state
			glClear(GL_COLOR_BUFFER_BIT);

			//shader
			shader->bind();

			//in order to draw something we first bind the vao, to specify the source of the vertices, the source of the indices, how to interpret vertices, where is this data sent, etc
			glBindVertexArray(vao);

			//draw without indices (draw uses implict order -> 1, 2, 3, 4, 5, 6), use triangly topology
			glDrawArrays(GL_TRIANGLES, 0, 6);

			//draw with indices (draw uses order specified by index buffer -> 0, 1, 2, 5, 4, 3), use point topology
			glDrawElements(GL_POINTS, 6, GL_UNSIGNED_INT, 0);

		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			//reload all shaders
			if (key == lap::wic::Key::SPACE) {
				std::cout << "-------------------------------------" << std::endl << "[LAB] reloading shader" << std::endl;
				shader->reload();
			}
			if (key == lap::wic::Key::ESCAPE) wnd.close();	//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	
	};
}



int main(int argc, char* argv[]) {
	//create a Window-Input-Context system with logging on std::cout and no thread safety (running in singlethreaded mode)
	lap::wic::WICSystem wicsystem(&std::cout, false);
	//create a window with properties
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Data and Topology I - index buffer, vertex buffers, vertex array buffers, attributes";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	cp.swap_interval = -1;	//adaptive vsync, limits fps at monitor update rate (e.g 60Hz), your graphics card might ignore/override this request!
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//create the Lab object here (the Lab object will REQUIRE an ACTIVE OpenGL context, which is provided by the window object), and set callbacks to member functions 
	lab::CGLab lab;
	using namespace std::placeholders;	//a delegate binds a member function from a class instance and uses placeholders (_1, _2..) for arguments
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {

		//call render each frame
		lab.render();

		//swap buffers (always rendering to the back buffer and drawing the front buffer, otherwise we'd get tearing)
		window.swapBuffers();

		//process events -> calls callbacks for each event
		window.processEvents();

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};
}