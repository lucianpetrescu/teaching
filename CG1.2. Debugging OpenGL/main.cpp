///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"


//debug function based on glGetError
void debugFunctionGetError() {
	GLenum result = glGetError();
	switch (result) {
	case GL_NO_ERROR:
		std::cout << "glGetError() = GL_NO_ERROR(" << GL_NO_ERROR << std::endl;
		break;
	case GL_INVALID_ENUM:
		std::cout << "glGetError() = GL_INVALID_ENUM" << GL_INVALID_ENUM << std::endl;
		break;
	case GL_INVALID_VALUE:
		std::cout << "glGetError() = GL_INVALID_VALUE" << GL_INVALID_VALUE << std::endl;
		break;
	case GL_INVALID_OPERATION:
		std::cout << "glGetError() = GL_INVALID_OPERATION" << GL_INVALID_OPERATION << std::endl;
		break;
	case GL_STACK_OVERFLOW:
		std::cout << "glGetError() = GL_STACK_OVERFLOW" << GL_STACK_OVERFLOW << std::endl;
		break;
	case GL_STACK_UNDERFLOW:
		std::cout << "glGetError() = GL_STACK_UNDERFLOW" << GL_STACK_UNDERFLOW << std::endl;
		break;
	case GL_OUT_OF_MEMORY:
		std::cout << "glGetError() = GL_OUT_OF_MEMORY" << GL_OUT_OF_MEMORY << std::endl;
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		std::cout << "glGetError() = GL_OUT_OF_MEMORY" << GL_INVALID_FRAMEBUFFER_OPERATION << std::endl;
		break;
	}
	std::cout << "Press any key" << std::endl;
	std::cin.get();
}

//debug function based on glDebugOutput
void APIENTRY debugFunctionCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam){
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
		case GL_DEBUG_SOURCE_API:				std::cout << "Source: API"; break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		std::cout << "Source: Window System"; break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER:	std::cout << "Source: Shader Compiler"; break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:		std::cout << "Source: Third Party"; break;
		case GL_DEBUG_SOURCE_APPLICATION:		std::cout << "Source: Application"; break;
		case GL_DEBUG_SOURCE_OTHER:				std::cout << "Source: Other"; break;
	}
	std::cout << std::endl;
	switch (type)
	{
		case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
		case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
		case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
		case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
		case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
		case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
		case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	}
	std::cout << std::endl;
	switch (severity)
	{
		case GL_DEBUG_SEVERITY_HIGH:			std::cout << "Severity: high"; break;
		case GL_DEBUG_SEVERITY_MEDIUM:			std::cout << "Severity: medium"; break;
		case GL_DEBUG_SEVERITY_LOW:				std::cout << "Severity: low"; break;
		case GL_DEBUG_SEVERITY_NOTIFICATION:	std::cout << "Severity: notification"; break;
	}
	std::cout << std::endl;
	std::cout << "Press any key" << std::endl;
	std::cin.get();
}


namespace lab {
	class CGLab {
	public:
		CGLab() {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;
		}
		~CGLab() {
			std::cout << "[LAB] Destroyed" << std::endl;
		}
		void render() {
			//set clear color (command to set state)
			glClearColor(1, 0, 0, 0);

			//ERROR, wrong paramter
			glClear(GL_ALL_SHADER_BITS);

			//glGetError based
			debugFunctionGetError();

			//custom messages can be pushed
			glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_OTHER, 123, GL_DEBUG_SEVERITY_HIGH, -1, "this is a custom error message using opengl debug API");
			glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_PERFORMANCE, 999, GL_DEBUG_SEVERITY_MEDIUM, -1, "the second custom error message");
			glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_PORTABILITY, 9876, GL_DEBUG_SEVERITY_LOW, -1, "this should not be abused like this");
			
		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Resized to " << width << "x" << height << std::endl;
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Pressed : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
			if (key == lap::wic::Key::ESCAPE) wnd.close();	//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Released : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Repeat : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MousePress : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseRelease : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseDrag : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseMove posx=" << posx << " posy=" << posy << std::endl;
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseScroll : scrollx= " << scrollx << " scrolly=" << scrolly << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
		}
	
	};
}


int main(int argc, char* argv[]) {
	//create a Window-Input-Context system with logging on std::cout and no thread safety (running in singlethreaded mode)
	lap::wic::WICSystem wicsystem(&std::cout, false);
	//create a window with properties
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Debug - glGetError, glDebugMessageCallback";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	cp.swap_interval = -1;		//adaptive vsync, limits fps at monitor update rate (e.g 60Hz), your graphics card might ignore/override this request!
	cp.debug_context = true;	//debug context
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//create the Lab object here (the Lab object will REQUIRE an ACTIVE OpenGL context, which is provided by the window object), and set callbacks to member functions 
	lab::CGLab lab;
	using namespace std::placeholders;	//a delegate binds a member function from a class instance and uses placeholders (_1, _2..) for arguments
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));

	//use the current context to attach a debugging function, this is only possible if the debug flags are enabled
	GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT){
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(debugFunctionCallback, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
	
	//main loop
	while (window.isOpened()) {

		//call render each frame
		lab.render();

		//swap buffers (always rendering to the back buffer and drawing the front buffer, otherwise we'd get tearing)
		window.swapBuffers();

		//process events -> calls callbacks for each event
		window.processEvents();

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};
}