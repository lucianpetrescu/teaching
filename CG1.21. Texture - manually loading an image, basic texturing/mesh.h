///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
// module neede math and glm provides it
#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/transform.hpp"
#include "../dependencies/glm/gtx/transform2.hpp"
// module needs buffers
#include "buffer.h"

namespace lab{

	// what does a vertex contain?
	// this is an interleaved data format. why? R: cache.
	struct Vertex{
		glm::vec3 position;				//vertex position attribute
		glm::vec3 normal;				//vertex normal attribute
		glm::vec2 texcoord;				//vertex texcoord attribute
		Vertex();
		Vertex(const Vertex&) = default;
		Vertex(Vertex&&) = default;
		Vertex& operator=(const Vertex &rhs) = default;
		Vertex& operator=(Vertex&& rhs) = default;
		Vertex(float px, float py, float pz);
		Vertex(const glm::vec3& p);
		Vertex(float px, float py, float pz, float nx, float ny, float nz);
		Vertex(const glm::vec3& p, const glm::vec3& n);
		Vertex(float px, float py, float pz, float tcx, float tcy);
		Vertex(const glm::vec3&p, const glm::vec2& tc);
		Vertex(float px, float py, float pz, float nx, float ny, float nz, float tcx, float tcy);
		Vertex(const glm::vec3& p, const glm::vec3& n, const glm::vec2& tc);
	};

	// AABB
	class AABB {
	public:
		AABB();
		glm::vec3 inf;	//inferior
		glm::vec3 sup;	//superior
	};

	// this is a mesh
	class Mesh {
	public:
		//ctors
		Mesh();
		Mesh(const std::string& objfilename, bool center = false, float scale = 1.0f);
		Mesh(std::vector<Vertex>& vertices, std::vector<unsigned int>& indices, bool center = false, float scale = 1.0f);

		//forbid direct copies (use clone)
		Mesh(const Mesh&) = delete;
		Mesh& operator=(const Mesh&) = delete;
		//allow moves
		Mesh(Mesh&&);
		Mesh& operator=(Mesh&&);
		//destructor
		~Mesh();

		//clone
		Mesh clone() const;

		//update
		void load(const std::string& objfilename, bool center = false, float scale = 1.0f);
		void load(std::vector<Vertex>& vertices, std::vector<unsigned int>& indices, bool center = false, float scale = 1.0f);

		//draws
		void draw(GLenum topology, size_t instances = 1) const;
		void drawRange(GLenum topology, size_t offset, size_t count, size_t instances = 1) const;

		//members access
		const AABB& getAABB() const;
		const Buffer& getVertexBuffer() const;
		const Buffer& getIndexBuffer() const;
		GLuint getVertexArrayObject() const;
		size_t getNumIndices() const;
		const glm::vec3& getCenter() const;
		const glm::vec3& getSize() const;

	private:
		AABB aabb;
		glm::vec3 center = glm::vec3(0,0,0);
		glm::vec3 size = glm::vec3(0, 0, 0);
		Buffer vbo{GL_DYNAMIC_STORAGE_BIT}, ibo{ GL_DYNAMIC_STORAGE_BIT };
		GLuint vao = 0;
		size_t indices_count =0;
	};
}
