///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------
 

#include "camera.h"
 
namespace lab{
	Camera::Camera() {
		//default camera
		position = glm::vec3(0, 0, 50);
		forward = glm::vec3(0, 0, -1);
		up = glm::vec3(0, 1, 0);
		right = glm::vec3(1, 0, 0);
	}
	Camera::Camera(const glm::vec3 &in_position, const glm::vec3 &in_center, const glm::vec3 &in_up) {
		//set camera
		set(in_position, in_center, in_up);
	}
	Camera::~Camera() {
	}

	void Camera::set(const glm::vec3 &in_position, const glm::vec3 &in_center, const glm::vec3 &in_up) {
		//update camera
		position = in_position;
		forward = glm::normalize(in_center - in_position);
		right = glm::cross(forward, in_up);
		up = glm::cross(right, forward);
	}

	void Camera::translateForward(float distance) {
		position = position + glm::normalize(glm::vec3(forward.x, 0, forward.z))*distance;
	}
	void Camera::translateUpward(float distance) {
		position = position + glm::normalize(glm::vec3(0, up.y, 0))*distance;
	}
	void Camera::translateRight(float distance) {
		position = position + glm::normalize(glm::vec3(right.x, 0, right.z))*distance;
	}

	void Camera::rotateFPSoX(float angle) {
		if ((forward.y > 0.9 && angle>0) || (forward.y < -0.9 && angle<0)) return;
		forward = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, right)*glm::vec4(forward, 1))));
	}

	void Camera::rotateFPSoY(float angle) {
		forward = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, up)*glm::vec4(forward, 1))));
		right = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, up)*glm::vec4(right, 1))));
	}
	void Camera::rotateTPSoX(float angle, float distance) {
		translateForward(distance);
		rotateFPSoX(angle);
		translateForward(-distance);
	}
	void Camera::rotateTPSoY(float angle, float distance) {
		translateForward(distance);
		rotateFPSoY(angle);
		translateForward(-distance);
	}

	const glm::mat4& Camera::getViewMatrix() {
		transformation = glm::lookAt(position, position + glm::normalize(forward), up);
		return transformation;
	}
	const glm::vec3& Camera::getPostion() const {
		return position;
	}
	const glm::vec3& Camera::getUpward() const {
		return up;
	}
	const glm::vec3& Camera::getForward() const {
		return forward;
	}
	const glm::vec3& Camera::getRight() const {
		return right;
	}
}