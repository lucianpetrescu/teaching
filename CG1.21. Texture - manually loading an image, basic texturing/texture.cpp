///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------

#include "texture.h"
#include <fstream>
#include <iostream>
#include <cassert>

namespace lab {
	
	///--------------------------------------------------------------------------------------------------------------------------
	//forward definition
	void loadBMPFile(const std::string &filename, unsigned int &in_width, unsigned int &in_height, std::vector<unsigned char>& data);
	size_t helperComputeMemoryUsageInBytes(GLuint globject);

	//ctors and operators
	Texture2D::Texture2D() {
	}
	Texture2D::Texture2D(const std::string& filename){
		load(filename);
	}
	Texture2D::Texture2D(GLint in_internal_format, unsigned int data_width, unsigned int data_height) {
		create(in_internal_format, data_width, data_height);
	}
	Texture2D::Texture2D(GLint in_internal_format, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data) {
		create(in_internal_format, data_width, data_height);
		update(data_format, data_type, data);
	}
	Texture2D::~Texture2D() {
		if(globject != 0) glDeleteTextures(1, &globject);
	}
	//allow moves
	Texture2D::Texture2D(Texture2D&& rhs) {
		assert(this != &rhs);	//user bug 
		width = rhs.width;
		height = rhs.height;
		memory = rhs.memory;
		internal_format = rhs.internal_format;
		globject = rhs.globject;

		rhs.width = 0;
		rhs.height = 0;
		rhs.memory = 0;
		rhs.internal_format = 0;
		rhs.globject = 0;
	}
	Texture2D& Texture2D::operator=(Texture2D&& rhs) {
		assert(this != &rhs);	//user bug 

		if (globject != 0) {
			glDeleteTextures(1, &globject);
			globject = 0;
		}
		width = rhs.width;
		height = rhs.height;
		memory = rhs.memory;
		internal_format = rhs.internal_format;
		globject = rhs.globject;

		rhs.width = 0;
		rhs.height = 0;
		rhs.memory = 0;
		rhs.internal_format = 0;
		rhs.globject = 0;

		return (*this);
	}

	//clone
	Texture2D Texture2D::clone() const {
		Texture2D result;
		result.create(internal_format, width, height);
		glCopyImageSubData(globject, GL_TEXTURE_2D, 0, 0, 0, 0, result.globject, GL_TEXTURE_2D, 0, 0, 0, 0, width, height, 1);
		return result;
	}

	//re-create or resize
	void Texture2D::create(GLint in_internal_format, unsigned int data_width, unsigned int data_height) {
		create(in_internal_format, data_width, data_height, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	}
	void Texture2D::load(const std::string& filename) {
		std::vector<unsigned char> data;
		unsigned int data_width, data_height;
		loadBMPFile(filename, data_width, data_height, data);
		create(GL_RGB8, data_width, data_height);	
		update(GL_RGB, GL_UNSIGNED_BYTE, data.data());
	}
	void Texture2D::create(GLint in_internal_format, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data) {
		// no storage space changes? -> quick exit
		if (in_internal_format == internal_format && data_width == width && data_height == height)  return;
		
		//the storage contract does not exist or it can't be used anymore, another storage space has to be allocated from scratch.
		if (globject != 0) glDeleteTextures(1, &globject);
		glCreateTextures(GL_TEXTURE_2D, 1, &globject);
		internal_format = in_internal_format;
		width = data_width;
		height = data_height;

		//allocate storage
		glTextureStorage2D(globject, 1, internal_format, width, height);	//allocate memory for base level

		//compute memory usage 
		memory = helperComputeMemoryUsageInBytes(globject);

		//put data in the allocated storage
		if(data) update(data_format, data_type, data);

		//basic filtering properties
		glTextureParameteri(globject, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTextureParameteri(globject, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTextureParameteri(globject, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTextureParameteri(globject, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}
	void Texture2D::resize(unsigned int in_width, unsigned int in_height) {
		//recreate with new width and height
		create(internal_format, in_width, in_height);
	}

	//data transfers
	void Texture2D::update(GLenum data_format, GLenum data_type, const GLvoid* data) {
		assert(globject != 0);
		//guarantee alignment
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		//update
		glTextureSubImage2D(globject, 0, 0, 0, width, height, data_format, data_type, data);
	}
	void Texture2D::update(unsigned int offset_x, unsigned int offset_y, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data){
		assert(globject != 0);
		//guarantee alignment
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		//update
		glTextureSubImage2D(globject, 0, offset_x, offset_y, data_width, data_height, data_format, data_type, data);
	}
	void Texture2D::readToCPU(GLenum outdata_format, GLenum outdata_type, GLsizei outdata_buffer_size, void* outdata) const {
		assert(globject != 0);
		//guarantee alignment
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		//read
		glGetTextureImage(globject, 0, outdata_format, outdata_type, outdata_buffer_size, outdata);
	}

	//usage
	void Texture2D::bindAsTexture(unsigned int texture_unit) const {
		assert(globject != 0);
		glBindTextureUnit(texture_unit, globject);
	}
	void Texture2D::bindAsImage(unsigned int image_unit, GLenum access, unsigned int level, GLenum format) const{
		assert(globject != 0);
		glBindImageTexture(image_unit, globject, level, GL_FALSE, 0, access, format);
	}

	//access
	GLint Texture2D::getInternalFormat() const {
		return internal_format;
	}
	unsigned int Texture2D::getWidth() const {
		return width;
	}
	unsigned int Texture2D::getHeight() const {
		return height;
	}
	unsigned int Texture2D::getGLobject() const {
		return globject;
	}

	//memory
	size_t Texture2D::getMemory() const {
		return memory;
	}


	///--------------------------------------------------------------------------------------------------------------------------
	// doest not support compression
	void loadBMPFile(const std::string &filename, unsigned int &width, unsigned int &height, std::vector<unsigned char>& data) {
		//BMP header data structures
		struct header {
			unsigned char type[2];
			int f_lenght;
			short rezerved1;
			short rezerved2;
			int offBits;
		};
		struct header_info {
			int size;
			int width;
			int height;
			short planes;
			short bitCount;
			int compresion;
			int sizeImage;
			int xPelsPerMeter;
			int yPelsPerMeter;
			int clrUsed;
			int clrImportant;
		};

		//load data from file
		std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary);
		if (!file.good()) {
			std::cout << "[LAB] Texture Loader: ERROR, could not find " << filename << " , or lacking reading rights!" << std::endl;
			std::terminate();
		}
		else {
			std::cout << "[LAB] Texture Loader : loaded file  " << filename << std::endl;
		}

		//read the headers
		header h; header_info h_info;
		file.read((char*)&(h.type[0]), sizeof(char));
		file.read((char*)&(h.type[1]), sizeof(char));
		file.read((char*)&(h.f_lenght), sizeof(int));
		file.read((char*)&(h.rezerved1), sizeof(short));
		file.read((char*)&(h.rezerved2), sizeof(short));
		file.read((char*)&(h.offBits), sizeof(int));
		file.read((char*)&(h_info), sizeof(header_info));

		//allocate memory
		data.resize(h_info.width*h_info.height * 3);

		//check padding
		long padd = 0;
		if ((h_info.width * 3) % 4 != 0) padd = 4 - (h_info.width * 3) % 4;

		//save size
		width = h_info.width;
		height = h_info.height;

		//read data
		long pointer;
		unsigned char r, g, b;
		for (unsigned int i = 0; i < height; i++)
		{
			for (unsigned int j = 0; j < width; j++)
			{
				file.read((char*)&b, 1);	//bgr -> rgb
				file.read((char*)&g, 1);
				file.read((char*)&r, 1);

				pointer = (i*width + j) * 3;
				data[pointer] = r;
				data[pointer + 1] = g;
				data[pointer + 2] = b;
			}

			file.seekg(padd, std::ios_base::cur);
		}
		file.close();
	}


	//internal help (
	size_t helperComputeMemoryUsageInBytes(GLuint globject){
		//if the texture is in a compressed format then we can query it directly
		GLint iscompressed = 0;
		glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_COMPRESSED, &iscompressed);
		if (iscompressed == GL_TRUE) {
			GLint compressed_size = 0;
			glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &compressed_size);
			if(compressed_size !=0) return compressed_size;
		}
		
		//else, we need to query the globject properties. This has to be done manually, as there is no help api available, and yes, 
		// in terms of monitoring support OpenGL is clearly inferior to other APIs.
		GLint format, w, h;
		glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_WIDTH, &w);
		glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_HEIGHT, &h);
		glGetTextureLevelParameteriv(globject, 0, GL_TEXTURE_INTERNAL_FORMAT, &format);

		switch (format) {
		//some of these are guesses on size, all follow the same  bytes_per_pixel * w * h logic
		//uncompressed
		case GL_DEPTH_COMPONENT:	return (size_t)(4 * w * h);
		case GL_DEPTH_STENCIL:		return (size_t)(4 * w * h);
		case GL_RED:				return (size_t)(1 * w * h);
		case GL_RG:					return (size_t)(2 * w * h);
		case GL_RGB:				return (size_t)(3 * w * h);
		case GL_RGBA:				return (size_t)(4 * w * h);
		case GL_DEPTH_COMPONENT16:	return (size_t)(2 * w * h);
		case GL_DEPTH_COMPONENT24:  return (size_t)(3 * w * h);
		case GL_DEPTH_COMPONENT32:  return (size_t)(4 * w * h);
		case GL_DEPTH_COMPONENT32F: return (size_t)(4 * w * h);
		case GL_DEPTH24_STENCIL8:	return (size_t)(4 * w * h);
		case GL_DEPTH32F_STENCIL8:	return (size_t)(5 * w * h);
		case GL_STENCIL_INDEX1:		return (size_t)(0.125 * w * h);
		case GL_STENCIL_INDEX4:		return (size_t)(0.5 * w * h);
		case GL_STENCIL_INDEX8:		return (size_t)(1 * w * h);
		case GL_STENCIL_INDEX16:	return (size_t)(2 * w * h);
		case GL_R8:					return (size_t)(1 * w * h);
		case GL_R8_SNORM:			return (size_t)(1 * w * h);
		case GL_R16:				return (size_t)(2 * w * h);
		case GL_R16_SNORM:			return (size_t)(2 * w * h);
		case GL_RG8:				return (size_t)(2 * w * h);
		case GL_RG8_SNORM:			return (size_t)(2 * w * h);
		case GL_RG16:				return (size_t)(4 * w * h);
		case GL_RG16_SNORM:			return (size_t)(4 * w * h);
		case GL_R3_G3_B2:			return (size_t)(1 * w * h);
		case GL_RGB4:				return (size_t)(1.5 * w * h);
		case GL_RGB5:				return (size_t)(1.875 * w * h);
		case GL_RGB8:				return (size_t)(3 * w * h);
		case GL_RGB8_SNORM:			return (size_t)(3 * w * h);
		case GL_RGB10:				return (size_t)(3.75 * w * h);
		case GL_RGB12:				return (size_t)(4.5 * w * h);
		case GL_RGB16_SNORM:		return (size_t)(6 * w * h);
		case GL_RGBA2:				return (size_t)(1 * w * h);
		case GL_RGBA4:				return (size_t)(2 * w * h);
		case GL_RGB5_A1:			return (size_t)(2 * w * h);
		case GL_RGBA8:				return (size_t)(4 * w * h);
		case GL_RGBA8_SNORM:		return (size_t)(4 * w * h);
		case GL_RGB10_A2:			return (size_t)(4 * w * h);
		case GL_RGB10_A2UI:			return (size_t)(4 * w * h);
		case GL_RGBA12:				return (size_t)(6 * w * h);
		case GL_RGBA16:				return (size_t)(8 * w * h);
		case GL_SRGB8:				return (size_t)(3 * w * h);
		case GL_SRGB8_ALPHA8:		return (size_t)(4 * w * h);
		case GL_R16F:				return (size_t)(2 * w * h);
		case GL_RG16F:				return (size_t)(2 * w * h);
		case GL_RGB16F:				return (size_t)(6 * w * h);
		case GL_RGBA16F:			return (size_t)(8 * w * h);
		case GL_R32F:				return (size_t)(4 * w * h);
		case GL_RG32F:				return (size_t)(8 * w * h);
		case GL_RGB32F:				return (size_t)(12 * w * h);
		case GL_RGBA32F:			return (size_t)(16 * w * h);
		case GL_R11F_G11F_B10F:		return (size_t)(4 * w * h);
		case GL_RGB9_E5:			return (size_t)(4 * w * h);
		case GL_R8I:				return (size_t)(1 * w * h);
		case GL_R8UI:				return (size_t)(1 * w * h);
		case GL_R16I:				return (size_t)(2 * w * h);
		case GL_R16UI:				return (size_t)(2 * w * h);
		case GL_R32I:				return (size_t)(4 * w * h);
		case GL_R32UI:				return (size_t)(4 * w * h);
		case GL_RG8I:				return (size_t)(2 * w * h);
		case GL_RG8UI:				return (size_t)(2 * w * h);
		case GL_RG16I:				return (size_t)(4 * w * h);
		case GL_RG16UI:				return (size_t)(4 * w * h);
		case GL_RG32I:				return (size_t)(16 * w * h);
		case GL_RG32UI:				return (size_t)(16 * w * h);
		case GL_RGB8I:				return (size_t)(3 * w * h);
		case GL_RGB8UI:				return (size_t)(3 * w * h);
		case GL_RGB16I:				return (size_t)(6 * w * h);
		case GL_RGB16UI:			return (size_t)(6 * w * h);
		case GL_RGB32I:				return (size_t)(12 * w * h);
		case GL_RGB32UI:			return (size_t)(12 * w * h);
		case GL_RGBA8I:				return (size_t)(4 * w * h);
		case GL_RGBA8UI:			return (size_t)(4 * w * h);
		case GL_RGBA16I:			return (size_t)(8 * w * h);
		case GL_RGBA16UI:			return (size_t)(8 * w * h);
		case GL_RGBA32I:			return (size_t)(16 * w * h);
		case GL_RGBA32UI:			return (size_t)(16 * w * h);

		//theoretically these cases should be handled by the if(compressed) return compressedsize at the beginning of this function, some drivers might not offer support..
		case GL_COMPRESSED_RED:		return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RG:		return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB:		return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RGBA:	return (size_t)(1 * w * h);
		case 0x83F0:				return (size_t)(0.5 * w * h);
		case 0x83F1:				return (size_t)(0.5 * w * h);
		case 0x83F2:				return (size_t)(1 * w * h);
		case 0x83F3:				return (size_t)(1 * w * h);
		case 0x8C4C:				return (size_t)(0.5 * w * h);
		case 0x8C4D:				return (size_t)(0.5 * w * h);
		case 0x8C4E:				return (size_t)(1 * w * h);
		case 0x8C4F:				return (size_t)(1 * w * h);
		case 0x8C48:				return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RED_RGTC1:				return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_SIGNED_RED_RGTC1:		return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RG_RGTC2:				return (size_t)(1 * w * h);
		case GL_COMPRESSED_SIGNED_RG_RGTC2:			return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT:	return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT:	return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGBA_BPTC_UNORM:			return (size_t)(1 * w * h);
		case GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM:	return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB8_ETC2:				return (size_t)(1 * w * h);
		case GL_COMPRESSED_SRGB8_ETC2:				return (size_t)(1 * w * h);
		case GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2:	return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2:	return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RGBA8_ETC2_EAC:			return (size_t)(1 * w * h);
		case GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC:	return (size_t)(1 * w * h);
		case GL_COMPRESSED_R11_EAC:					return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_SIGNED_R11_EAC:			return (size_t)(0.5 * w * h);
		case GL_COMPRESSED_RG11_EAC:				return (size_t)(1 * w * h);
		case GL_COMPRESSED_SIGNED_RG11_EAC:			return (size_t)(1 * w * h);
		case 0x93B0: return (size_t)(0.5 * w * h);
		case 0x93B1: return (size_t)(0.8 * w * h);
		case 0x93B2: return (size_t)(0.64 * w * h);
		case 0x93B3: return (size_t)(0.53 * w * h);
		case 0x93B4: return (size_t)(0.445 * w * h);
		case 0x93B5: return (size_t)(0.4 * w * h);
		case 0x93B6: return (size_t)(0.333 * w * h);
		case 0x93B7: return (size_t)(0.25 * w * h);
		case 0x93B8: return (size_t)(0.325 * w * h);
		case 0x93B9: return (size_t)(0.266 * w * h);
		case 0x93BA: return (size_t)(0.2 * w * h);
		case 0x93BB: return (size_t)(0.16 * w * h);
		case 0x93BC: return (size_t)(0.133 * w * h);
		case 0x93BD: return (size_t)(0.111 * w * h);
		case 0x93D0: return (size_t)(0.5 * w * h);
		case 0x93D1: return (size_t)(0.11 * w * h);
		case 0x93D2: return (size_t)(0.64 * w * h);
		case 0x93D3: return (size_t)(0.53 * w * h);
		case 0x93D4: return (size_t)(0.445 * w * h);
		case 0x93D5: return (size_t)(0.4 * w * h);
		case 0x93D6: return (size_t)(0.333 * w * h);
		case 0x93D7: return (size_t)(0.25 * w * h);
		case 0x93D8: return (size_t)(0.32 * w * h);
		case 0x93D9: return (size_t)(0.266 * w * h);
		case 0x93DA: return (size_t)(0.2 * w * h);
		case 0x93DB: return (size_t)(0.16 * w * h);
		case 0x93DC: return (size_t)(0.133 * w * h);
		case 0x93DD: return (size_t)(0.111 * w * h);
		default:
			//unsupported format (compatibility or a non-core non-ubiquous non-astc extension), leave everything to default
			break;
		}
		return 0;
	}
}