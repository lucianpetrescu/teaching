###Screenshots###

NOTE: the rendering artifacts are expected, next time filtering

texturing brings realism to rendering.
![](img/output.png)

texturing uses texture coordinates which map images to geometric surfaces. The UVs are visualized in this image. 
![](img/output_1.png)

while the image might not this much if this view is used in the video serious artifacts are noticeable. This is related to **filtering**/**sampling**, which will be the main topic of the next lab. It is not enough just to map an image to a geometry surface, the image also has to be **sampled**/**filtered** properly.
![](img/output_2.png)

texturing artifacts at the far side of the ground plane, filtering related.
![](img/output_3.png)
