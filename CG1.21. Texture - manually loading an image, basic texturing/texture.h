///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------

#pragma once
// module needs OpenGL and wic provides it
#include "../dependencies/lap_wic/lap_wic.hpp"

namespace lab{

	///--------------------------------------------------------------------------------------------------------------------------
	class Texture2D {
	public:
		//ctors and operators
		Texture2D();
		Texture2D(const std::string& filename);
		Texture2D(GLint internal_format, unsigned int data_width, unsigned int data_height);
		Texture2D(GLint internal_format, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data);
		//disallow implicit copies and assignments, use the explicit clone
		Texture2D(const Texture2D&) = delete;
		Texture2D& operator=(const Texture2D&) = delete;
		//allow moves
		Texture2D(Texture2D&&);
		Texture2D& operator=(Texture2D&&);
		~Texture2D();

		//clone
		Texture2D clone() const;

		//re-create or resize
		void load(const std::string& filename);
		void create(GLint internal_format, unsigned int data_width, unsigned int data_height);
		void create(GLint internal_format, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data);
		void resize(unsigned int width, unsigned int height);
		
		//updates ( they do not reallocate, they just transfer data)
		void update(GLenum data_format, GLenum data_type, const GLvoid* data);
		void update(unsigned int offset_x, unsigned int offset_y, unsigned int data_width, unsigned int data_height, GLenum data_format, GLenum data_type, const GLvoid* data);
		void readToCPU(GLenum outdata_format, GLenum outdata_type, GLsizei outdata_buffer_size, void* outdata) const;

		//usage
		void bindAsTexture(unsigned int texture_unit) const;
		void bindAsImage(unsigned int image_unit, GLenum access, unsigned int level, GLenum format) const;

		//access
		GLint getInternalFormat() const;
		unsigned int getWidth() const;
		unsigned int getHeight() const;
		unsigned int getGLobject() const;

		//memory (not exact!)
		size_t getMemory() const;

	private:
		unsigned int width = 0;
		unsigned int height = 0;
		size_t memory = 0;
		GLint internal_format = 0;
		GLuint globject =0;
	};
}