///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"


namespace lab {
	class CGLab {
		GLuint bufferid;
		GLuint bufferstoredid;
	public:
		CGLab() {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;

			//generate some data
			float data[10] = { 1,2,3,4,5,6,7,8,9,10 };

			//this small example shows the OpenGL object model with buffer operations. Other objects (textures, framebuffers, renderbuffers, queries, etc)
			//have very similar commands.


			//SIMPLE GENERATION AND UPLOAD
			//generate a buffer name
			glGenBuffers(1, &bufferid);
			//we need to bind the buffer for OpenGL to create it (right now bufferid is just a name)
			glBindBuffer(GL_ARRAY_BUFFER, bufferid);
			//upload data to the GPU, ignore the storage hint for now
			glNamedBufferData(bufferid, sizeof(float)*10, data, GL_STATIC_DRAW);
			//update only a portion of the buffer 
			glNamedBufferSubData(bufferid, 5 * sizeof(float), 5 * sizeof(float), data);		//now buffer data is 1,2,3,4,5,1,2,3,4,5
			glNamedBufferSubData(bufferid, 5 * sizeof(float), 5 * sizeof(float), &data[5]);	//now buffer data is 1,2,3,4,5,6,7,8,9,10
			assert(glGetError() == GL_NO_ERROR);

			
			//MAPPING
			//map the buffer back to the cpu
			float* ptr = (float*)glMapNamedBuffer(bufferid, GL_READ_WRITE);
			for (int i = 0; i < 10; i++) assert(data[i] == ptr[i]);
			//unmap the buffer from the cpi
			glUnmapNamedBuffer(bufferid);
			

			//BINDING POINTS
			//bind the buffer to a target (= a special GPU mapping point, we'll get to that in the future)
			glBindBuffer(GL_ARRAY_BUFFER, bufferid);
			//unbind the buffer from the target
			glBindBuffer(GL_ARRAY_BUFFER, 0);


			//CREATION AND STORAGE
			//create is similar to glGenBuffers but it also creates the buffer object.
			glCreateBuffers(1, &bufferstoredid);
			//upload data and make the storage (memory allocation) immutable, the last parameter enables various buffer usage modes.
			glNamedBufferStorage(bufferstoredid, 5 * sizeof(float), data, GL_MAP_WRITE_BIT | GL_MAP_READ_BIT);
			
			assert(glGetError() == GL_NO_ERROR);


			//QUERIES
			GLint param;
			glGetNamedBufferParameteriv(bufferid, GL_BUFFER_SIZE, &param);
			std::cout << "The first buffer has " << param << " size" << std::endl;
			glGetNamedBufferParameteriv(bufferstoredid, GL_BUFFER_IMMUTABLE_STORAGE, &param);
			if(param == GL_TRUE) std::cout << "The second buffer has immutable storage "<< std::endl;

			assert(glGetError() == GL_NO_ERROR);
		}
		~CGLab() {
			//delete the buffers
			glDeleteBuffers(1, &bufferid);
			glDeleteBuffers(1, &bufferstoredid);
		}
		void render() {
			//set clear color (command to set state)
			glClearColor(1, 0, 0, 0);
			glClear(GL_COLOR_BUFFER_BIT);

			//do nothing here
		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			if (key == lap::wic::Key::ESCAPE) wnd.close();	//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	
	};
}


int main(int argc, char* argv[]) {
	//create a Window-Input-Context system with logging on std::cout and no thread safety (running in singlethreaded mode)
	lap::wic::WICSystem wicsystem(&std::cout, false);
	//create a window with properties
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Buffers - the OpenGL object model";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	cp.swap_interval = -1;		//adaptive vsync, limits fps at monitor update rate (e.g 60Hz), your graphics card might ignore/override this request!
	cp.debug_context = true;	//debug context
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//create the Lab object here (the Lab object will REQUIRE an ACTIVE OpenGL context, which is provided by the window object), and set callbacks to member functions 
	lab::CGLab lab;
	using namespace std::placeholders;	//a delegate binds a member function from a class instance and uses placeholders (_1, _2..) for arguments
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));

	//main loop
	while (window.isOpened()) {

		//call render each frame
		lab.render();

		//swap buffers (always rendering to the back buffer and drawing the front buffer, otherwise we'd get tearing)
		window.swapBuffers();

		//process events -> calls callbacks for each event
		window.processEvents();

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

}