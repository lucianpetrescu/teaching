//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;
layout(binding =0) uniform sampler2D sampler_color1;
layout(binding =1) uniform sampler2D sampler_color2;
layout(binding =2) uniform sampler2D sampler_color3;
layout(binding =3) uniform sampler2D sampler_depth;
in vec2 texcoords;

//uniform
uniform int mode;

void main(){
	if(mode == 1) colorout = texture(sampler_color1,texcoords).xyz;
	else if(mode == 2) colorout = texture(sampler_color2,texcoords).xyz;
	else if(mode == 3) colorout = texture(sampler_color3,texcoords).xyz;
	else colorout = pow(texture(sampler_depth,texcoords).xxx, vec3(255.0f, 255.0f, 255.0f));
}

