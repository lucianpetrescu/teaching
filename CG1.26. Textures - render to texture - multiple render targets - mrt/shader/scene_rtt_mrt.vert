//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//attributes
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;


//object id
uniform int id;
//static data, 
layout(column_major, std140, binding = 0) uniform UboData {
	mat4 model_matrix[4];
	vec4 material_constants[4];
	vec4 material_color[4];
	vec4 light_direction;
	vec4 light_angles;
	vec4 light_color;
	vec4 light_attenuation;
	mat4 view_matrix, projection_matrix;
	vec4 light_position;
	vec4 light_falloff;
	vec4 camera_position;
};

//output to fragment shader
out vec3 position_ws, normal_ws;

void main(){

	position_ws = (model_matrix[id] * vec4(in_position,1)).xyz;
	normal_ws = normalize(mat3(model_matrix[id]) * in_normal);

	gl_Position = projection_matrix * view_matrix * model_matrix[id] * vec4(in_position,1);
}

