###Screenshots###

Framebuffers are useful for storing visual output in a specified format. This was discussed in the previous lab. Framebuffers can also have multiple render targets (MRT), where each target can store formatted information.

In this lab, the same scene is rendered from multiple viewpoints.

first target is color
![](img/output.png)
2nd target is illumination
![](img/output_2.png)
3rd target is normal information
![](img/output_3.png)
4th target is depth (which is exponentiated in order for it to be easily perceived)
![](img/output_4.png)

A single parameter controls the number of views.
