///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/type_ptr.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"

namespace lab{

	class Camera{
	public:
		Camera();
		Camera(const glm::vec3 &position, const glm::vec3 &center, const glm::vec3 &up);
		~Camera();
		//set
		void set(const glm::vec3 &position, const glm::vec3 &center, const glm::vec3 &up);

		//translations
		void translateForward(float distance);
		void translateUpward(float distance);
		void translateRight(float distance);
		//rotations in first person
		void rotateFPSoX(float angle);
		void rotateFPSoY(float angle);
		void rotateFPSoZ(float angle);
		//rotations in third person
		void rotateTPSoX(float angle, float distance);
		void rotateTPSoY(float angle, float distance);
		void rotateTPSoZ(float angle, float distance);
		//get view matrix
		const glm::mat4& getViewMatrix();
		//gets
		const glm::vec3& getPostion() const;
		const glm::vec3& getUpward() const;
		const glm::vec3& getForward() const;
		const glm::vec3& getRight() const;

	private:
		glm::mat4 transformation;
		glm::vec3 position;
		glm::vec3 up;
		glm::vec3 forward;
		glm::vec3 right;
	};
}