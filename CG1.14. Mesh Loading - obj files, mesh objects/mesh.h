///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
// module neede math and glm provides it
#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/transform.hpp"
#include "../dependencies/glm/gtx/transform2.hpp"
// module needs OpenGL and wic provides it
#include "../dependencies/lap_wic/lap_wic.hpp"

namespace lab{

	// what does a vertex contain?
	// this is an interleaved data format. why? R: cache.
	struct Vertex{
		glm::vec3 position;				//vertex position attribute
		glm::vec3 normal;				//vertex normal attribute
		glm::vec2 texcoord;				//vertex texcoord attribute
		Vertex();
		Vertex(float px, float py, float pz);
		Vertex(const glm::vec3& p);
		Vertex(float px, float py, float pz, float nx, float ny, float nz);
		Vertex(const glm::vec3& p, const glm::vec3& n);
		Vertex(float px, float py, float pz, float tcx, float tcy);
		Vertex(const glm::vec3&p, const glm::vec2& tc);
		Vertex(float px, float py, float pz, float nx, float ny, float nz, float tcx, float tcy);
		Vertex(const glm::vec3& p, const glm::vec3& n, const glm::vec2& tc);
		Vertex& operator=(const Vertex &rhs);
	};

	// AABB
	class AABB {
	public:
		glm::vec3 inf;	//inferior
		glm::vec3 sup;	//superior
	};

	// this is a mesh
	class Mesh {
	public:
		//ctors
		Mesh();
		Mesh(const std::string& objfilename);
		Mesh(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices);

		//forbid
		Mesh(const Mesh&) = delete;
		Mesh(Mesh&&) = delete;
		Mesh& operator=(const Mesh&) = delete;
		Mesh& operator=(Mesh&&) = delete;
		~Mesh();
		
		//update
		void load(const std::string& objfilename);
		void load(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices);

		//binds
		void bindVertexBufferToTarget(GLenum target, unsigned int index = 0);
		void bindIndexBufferToTarget(GLenum target, unsigned int index = 0);
		
		//draws
		void draw(GLenum topology, unsigned int instances = 1);
		void drawRange(GLenum topology, unsigned int offset, unsigned int count, unsigned int instances = 1);

		//members access
		const AABB& getAABB();
		GLuint getVertexBuffer();
		GLuint getIndexBuffer();
		GLuint getVertexArrayObject();
		unsigned int getNumIndices();
	private:
		AABB aabb;
		GLuint vbo, ibo, vao;
		unsigned int indices_count;
	};
}
