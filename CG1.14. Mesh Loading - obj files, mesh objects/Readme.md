###Screenshots###

loaded 3 meshes

wireframe view (notice that back face culling is used)
![results](img/output.png)
solid view, hard to see without lighting, we'll solve this next time
![results](img/output_2.png)
positions visualized
![results](img/output_3.png)
normals visualized
![results](img/output_4.png)
texture coordinates visualized, black = obj file didn't come with texcoords.
![results](img/output_5.png)