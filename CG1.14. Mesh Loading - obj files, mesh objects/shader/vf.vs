
#version 450

//get data from attribute pipe #0, interpret it as vec3
layout(location = 0) in vec3 in_position;
//get data from attribute pipe #1, interpret it as vec3
layout(location = 1) in vec3 in_normal;
//get data from attribute pipe #2, interpret it as vec2
layout(location = 2) in vec2 in_texcoord;

uniform mat4 model_matrix;
uniform mat4 model_normal_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

out vec3 vs_position;
out vec3 vs_normal;
out vec2 vs_texcoord;

void main(){

	// TRANSFORMATIONS ARE BASED ON CONVENTIONS
	// we'll always use the column-ordered (column-major) and the left handed coordinate system conventions

	vs_position = mat3(model_matrix) * in_position;
	vs_normal = mat3(model_normal_matrix) * in_normal;
	vs_texcoord = in_texcoord;

	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position,1);
}

