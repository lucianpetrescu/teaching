
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

uniform vec3 color;
uniform uint mode;

in vec3 vs_position;
in vec3 vs_normal;
in vec2 vs_texcoord;

void main(){
	switch(mode){
	case 0: colorout = color; break;
	case 1: colorout = vs_position/10.0f; break;
	case 2: colorout = (vs_normal+1)/2.0f; break;
	case 3: colorout = vec3(vs_texcoord, 0); break;
	}
}

