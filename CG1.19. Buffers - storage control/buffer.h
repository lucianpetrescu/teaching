///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#pragma once
// module needs OpenGL and wic provides it
#include "../dependencies/lap_wic/lap_wic.hpp"

namespace lab{

	class Buffer {
	public:
		//storage_flags must be a combination of of GL_DYNAMIC_STORAGE_BIT, GL_MAP_READ_BIT, GL_MAP_WRITE_BIT, GL_MAP_PERSISTENT_BIT, GL_MAP_COHERENT_BIT or GL_CLIENT_STORAGE_BIT
		Buffer(GLenum storage_flags);
		//storage_flags must be a combination of of GL_DYNAMIC_STORAGE_BIT, GL_MAP_READ_BIT, GL_MAP_WRITE_BIT, GL_MAP_PERSISTENT_BIT, GL_MAP_COHERENT_BIT or GL_CLIENT_STORAGE_BIT
		Buffer(size_t size, GLenum storage_flags);
		//storage_flags must be a combination of of GL_DYNAMIC_STORAGE_BIT, GL_MAP_READ_BIT, GL_MAP_WRITE_BIT, GL_MAP_PERSISTENT_BIT, GL_MAP_COHERENT_BIT or GL_CLIENT_STORAGE_BIT
		Buffer(void* data, size_t offset, size_t size, GLenum storage_flags);
		Buffer(const Buffer&) = delete;
		Buffer(Buffer&&) = delete;
		Buffer& operator=(const Buffer&) = delete;
		Buffer& operator=(Buffer&&) = delete;
		~Buffer();

		//resizes the GPU buffer
		void resize(size_t size);
		//copies data from the GPU buffer to the cpu
		void copy(void* dst, size_t bufferoffset, size_t datasize) const;
		//updates the contents of the GPU buffer, does NOT change size.
		void update(void* data, size_t bufferoffset, size_t datasize);
		void update(void* data);

		//binds
		void bind(GLenum target);
		void bindIndexed(GLenum target, unsigned int index);
		void bindIndexed(GLenum target, unsigned int index, size_t offset, size_t size);

		//map, while a buffer lacking a GL_MAP_READ_BIT / GL_MAP_WRITE_BIT storage hint can be mapped, reading/writing to it will be done with SEVERELY reduced performance.
		//access can be one of GL_READ_ONLY, GL_WRITE_ONLY, or GL_READ_WRITE. 
		void* map(size_t offset, GLenum access);
		void unmap();

		//raw access
		GLuint getGLobject() const;
		size_t getSize() const;
		GLenum getStorageFlags() const;
		bool getIsMapped() const;
	private:
		GLuint globject = 0;
		size_t size = 0;
		GLenum storage_flags = 0;
		bool mapped = false;
	};
}
