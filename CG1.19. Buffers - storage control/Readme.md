###Screenshots###

NOTE: the screenshots don't contain actual lab video as the topic is explicit storage.

explicit storage for buffer objects. The operation might seem less efficient than the one required by implicit storage buffer objects, but actually the cost is the same (or even less), as the glBufferData implementation first orphans (lazy delete) the existing storage space and then allocates new storage space.
![explicit storage for buffer objects](img/output.png)

buffers with explicit storage use a different type of flags than the glBufferData buffers. 
![explicit storage uses flags, which are different ](img/output.png)
