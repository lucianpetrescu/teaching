//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;
uniform sampler2D sampler_color;
in vec2 texcoords;

void main(){
	colorout = texture(sampler_color,texcoords).xyz;
	//colorout = vec3(1,0,0);
}

