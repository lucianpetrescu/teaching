###Screenshots###

here the different frames of an animated .gif image are shown in order on a full screen textured quad, defined directly in Normalized Device Coordinates (NDC)

![](img/output.png)
![](img/output_2.png)

