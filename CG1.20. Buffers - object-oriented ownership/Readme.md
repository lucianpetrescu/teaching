###Screenshots###

NOTE: the screenshots don't contain actual lab video as the topic is ownership

OpenGL objects need to be manually moved to maintain consistent OpenGL state. Some state can't be cloned (e.g. mappings) as clones create new gpu objects. 
![](img/output.png)
![](img/output_2.png)

Shader objects have their entire state copied.
![](img/output_3.png)
![](img/output_4.png)

Meshes use vertex array objects (VAOs), which are gpu state objects. Cloning VAOs requires re-creating the same type of bindings for the new buffer objects.
![](img/output_5.png)
![](img/output_6.png)
