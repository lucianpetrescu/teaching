//this shader illuminates geometry with the blinn reflection model and with gouraud shading
#version 450

//output data on pipe 0 (default output on framebuffer)
layout(location = 0) out vec3 colorout;

//incoming color
in vec3 vs_color;

void main(){
	colorout = vs_color;
}

