///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "mesh.h"
#include <cstdint>
#include <sstream>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <cassert>

using namespace glm;
namespace lab{

	///--------------------------------------------------------------------------------------------------------------------------
	//vertex format
	Vertex::Vertex(){
		position = normal = vec3(0, 0, 0);
		texcoord = vec2(0, 0);
	}
	Vertex::Vertex(float px, float py, float pz) {
		position = vec3(px, py, pz);
		normal = vec3(0, 0, 0);
		texcoord = vec2(0, 0);
	}
	Vertex::Vertex(const glm::vec3& p) {
		position = p;
		normal = vec3(0, 0, 0);
		texcoord = vec2(0, 0);
	}
	Vertex::Vertex(float px, float py, float pz, float nx, float ny, float nz) {
		position = vec3(px, py, pz);
		normal = vec3(nx, ny, nz);
		texcoord = vec2(0, 0);
	}
	Vertex::Vertex(const glm::vec3& p, const glm::vec3& n) {
		position = p;
		normal = n;
		texcoord = vec2(0, 0);
	}
	Vertex::Vertex(float px, float py, float pz, float tcx, float tcy) {
		position = vec3(px, py, pz);
		normal = vec3(0, 0, 0);
		texcoord = vec2(tcx, tcy);
	}
	Vertex::Vertex(const glm::vec3&p, const glm::vec2& tc) {
		position = p;
		normal = vec3(0, 0, 0);
		texcoord = tc;
	}
	Vertex::Vertex(float px, float py, float pz, float nx, float ny, float nz, float tcx, float tcy) {
		position = vec3(px, py, pz);
		normal = vec3(nx, ny, nz);
		texcoord = vec2(tcx, tcy);
	}
	Vertex::Vertex(const glm::vec3& p, const glm::vec3& n, const glm::vec2& tc) {
		position = p;
		normal = n;
		texcoord = tc;
	}




	///--------------------------------------------------------------------------------------------------------------------------
	//forward define all formats here, for now only OBJ
	void loadObjFile(const std::string &filename, std::vector<Vertex> &vertices, std::vector<unsigned int> &indices);


	///--------------------------------------------------------------------------------------------------------------------------
	Mesh::Mesh() {
		vao = 0;
		indices_count = 0;
		aabb.inf = aabb.sup = glm::vec3(0, 0, 0);
	}
	Mesh::~Mesh() {
		if (vao != 0) glDeleteVertexArrays(1, &vao);
	}
	Mesh::Mesh(const std::string& objfilename) {
		load(objfilename);
	}
	Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices) {
		load(vertices, indices);
	}
	Mesh::Mesh(Mesh&& rhs) {
		assert(this != &rhs);	//most definately a user bug as this leads to undefined behavior
		aabb = std::move(rhs.aabb);
		vbo = std::move(rhs.vbo);
		ibo = std::move(rhs.ibo);
		vao = rhs.vao;
		indices_count = rhs.indices_count;
		rhs.vao = 0;
		rhs.indices_count = 0;
	}
	Mesh& Mesh::operator=(Mesh&& rhs) {
		assert(this != &rhs);	//most definately a user bug as this leads to undefined behavior
		if (vao != 0){
			glDeleteVertexArrays(1, &vao);
			vao = 0;
		}
		aabb = std::move(rhs.aabb);
		vbo = std::move(rhs.vbo);
		ibo = std::move(rhs.ibo);
		vao = rhs.vao;
		indices_count = rhs.indices_count;
		rhs.vao = 0;
		rhs.indices_count = 0;
		return (*this);
	}

	//clone
	Mesh Mesh::clone() const {
		Mesh result;
		result.aabb = aabb;
		result.vbo = std::move(vbo.clone());
		result.ibo = std::move(ibo.clone());
		result.indices_count = indices_count;
		//vertex array needs to be manually cloned, as all the contents (state bindings) need to be recreated for the new buffers
		glCreateVertexArrays(1, &result.vao);
		glEnableVertexArrayAttrib(result.vao, 0);
		glEnableVertexArrayAttrib(result.vao, 1);
		glEnableVertexArrayAttrib(result.vao, 2);
		glVertexArrayVertexBuffer(result.vao, 5, result.vbo.getGLobject(), 0, sizeof(Vertex));				//INTERLEAVED:
		glVertexArrayAttribBinding(result.vao, 0, 5);														//attribute 0 gets data from the #5 binding
		glVertexArrayAttribBinding(result.vao, 1, 5);														//attribute 1 gets data from the #5 binding
		glVertexArrayAttribBinding(result.vao, 2, 5);														//attribute 2 gets data from the #5 binding
		glVertexArrayAttribFormat(result.vao, 0, 3, GL_FLOAT, GL_FALSE, 0);									// send positions on attribute 0, from offset 0
		glVertexArrayAttribFormat(result.vao, 1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));					// send normals on attribute 1, from offset glm::vec3
		glVertexArrayAttribFormat(result.vao, 2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(glm::vec3));				// send texcoords on attribute 2, from offset glm::vec3 * 2
		glVertexArrayElementBuffer(result.vao, result.ibo.getGLobject());
		return result; //nrvo
	}


	void Mesh::load(const std::string& objfilename) {
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		loadObjFile(objfilename, vertices, indices);
		load(vertices, indices);
	}

	void Mesh::load(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices) {

		//vertex buffer
		vbo.resize(sizeof(Vertex) * vertices.size());
		vbo.update((void*)vertices.data(), 0, sizeof(Vertex) * vertices.size());
		
		//index buffer
		ibo.resize(sizeof(unsigned int) * indices.size());
		ibo.update((void*)indices.data(), 0, sizeof(unsigned int) * indices.size());

		//clean previous
		if (vao != 0) glDeleteVertexArrays(1, &vao);
		//vertex array buffer
		glCreateVertexArrays(1, &vao);
		glEnableVertexArrayAttrib(vao, 0);
		glEnableVertexArrayAttrib(vao, 1);
		glEnableVertexArrayAttrib(vao, 2);
		glVertexArrayVertexBuffer(vao, 5, vbo.getGLobject(), 0, sizeof(Vertex));						//INTERLEAVED:
		glVertexArrayAttribBinding(vao, 0, 5);															//attribute 0 gets data from the #5 binding
		glVertexArrayAttribBinding(vao, 1, 5);															//attribute 1 gets data from the #5 binding
		glVertexArrayAttribBinding(vao, 2, 5);															//attribute 2 gets data from the #5 binding
		glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, 0);									// send positions on attribute 0, from offset 0
		glVertexArrayAttribFormat(vao, 1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));					// send normals on attribute 1, from offset glm::vec3
		glVertexArrayAttribFormat(vao, 2, 2, GL_FLOAT, GL_FALSE, 2*sizeof(glm::vec3));					// send texcoords on attribute 2, from offset glm::vec3 * 2
		glVertexArrayElementBuffer(vao, ibo.getGLobject());
		indices_count = indices.size();

		//compute aabb
		float nl = std::numeric_limits<float>::max();
		aabb.inf = vec3(nl, nl, nl);
		aabb.sup = vec3(-nl, -nl, -nl);
		for (auto& v : vertices) {
			aabb.inf = glm::min(aabb.inf, v.position);
			aabb.sup = glm::max(aabb.sup, v.position);
		}
	}

	void Mesh::draw(GLenum topology, size_t instances) const {
		assert(vao!=0);
		glBindVertexArray(vao);
		glDrawElementsInstanced(topology, (GLsizei)indices_count, GL_UNSIGNED_INT, 0, (GLsizei)instances);
	}
	void Mesh::drawRange(GLenum topology, size_t offset, size_t count, size_t instances) const {
		assert(vao!=0);
		glBindVertexArray(vao);
		glDrawElementsInstanced(topology, (GLsizei)count, GL_UNSIGNED_INT, (const void*)(uintptr_t)offset, (GLsizei)instances);
	}

	const AABB& Mesh::getAABB() const {
		return aabb;
	}
	const Buffer& Mesh::getVertexBuffer() const {
		return vbo;
	}
	const Buffer& Mesh::getIndexBuffer() const {
		return ibo;
	}
	GLuint Mesh::getVertexArrayObject() const {
		return vao;
	}
	size_t Mesh::getNumIndices() const {
		return indices_count;
	}




	///--------------------------------------------------------------------------------------------------------------------------
	//format parsers, for now only OBJ
	void tokenize(const std::string &source, std::vector<std::string> &tokens) {
		tokens.clear();
		std::string aux = source;
		for (unsigned int i = 0; i<aux.size(); i++) if (aux[i] == '\t' || aux[i] == '\n') aux[i] = ' ';
		std::stringstream ss(aux, std::ios::in);
		while (ss.good()){
			std::string s;
			ss >> s;
			if (s.size()>0) tokens.push_back(s);
		}
	}
	void tokenizeObjFace(const std::string &source, std::vector<std::string> &tokens){
		std::string aux = source;
		for (unsigned int i = 0; i<aux.size(); i++) if (aux[i] == '\\' || aux[i] == '/') aux[i] = ' ';
		tokenize(aux, tokens);
	}


	// no high order surfaces, materials, lines, NURBS, groups, smoothing
	// Format: http://paulbourke.net/dataformats/obj/
	// slow but easy to read
	void loadObjFile(const std::string &filename, std::vector<Vertex> &vertices, std::vector<unsigned int> &indices){
		//citim din fisier
		std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary);
		if (!file.good()){
			std::cout << "[LAB] Mesh Loader: Error, could not find " << filename << " , or lacking reading rights!" << std::endl;
			std::terminate();
		}
		else {
			std::cout << "[LAB] Mesh Loader : loaded file  " << filename << std::endl;
		}

		std::string line;
		std::vector<std::string> tokens, facetokens;
		std::vector<glm::vec3> positions;		positions.reserve(1000);
		std::vector<glm::vec3> normals;		normals.reserve(1000);
		std::vector<glm::vec2> texcoords;		texcoords.reserve(1000);
		while (std::getline(file, line)){
			//tokenize line
			tokenize(line, tokens);

			//skip empty line
			if (tokens.size() == 0) continue;

			//skip comments
			if (tokens.size()>0 && tokens[0].at(0) == '#') continue;

			//save position
			if (tokens.size()>3 && tokens[0] == "v") positions.push_back(glm::vec3(std::stof(tokens[1]), std::stof(tokens[2]), std::stof(tokens[3])));

			//save normal
			if (tokens.size()>3 && tokens[0] == "vn") normals.push_back(glm::vec3(std::stof(tokens[1]), std::stof(tokens[2]), std::stof(tokens[3])));

			//save texture coordinates
			if (tokens.size()>2 && tokens[0] == "vt") texcoords.push_back(glm::vec2(std::stof(tokens[1]), std::stof(tokens[2])));

			//face
			if (tokens.size() >= 4 && tokens[0] == "f"){

				//determine face format (v v/t v//n v/t/n) = (1 2 3 4)
				unsigned int face_format = 0;
				if (tokens[1].find("//") != std::string::npos) face_format = 3;
				tokenizeObjFace(tokens[1], facetokens);
				if (facetokens.size() == 3) face_format = 4; // position/texcoords/normals
				else{
					if (facetokens.size() == 2){
						if (face_format != 3) face_format = 2;	//postion/texcoords
					}
					else{
						face_format = 1; //only position
					}
				}

				//first index
				unsigned int index_of_first_vertex_of_face = -1;


				for (unsigned int num_token = 1; num_token<tokens.size(); num_token++){
					if (tokens[num_token].at(0) == '#') break;					
					tokenizeObjFace(tokens[num_token], facetokens);
					if (face_format == 1){
						//only position
						int p_index = std::stoi(facetokens[0]);
						if (p_index>0) p_index -= 1;								//obj has 1...n indices
						else p_index = (unsigned int) positions.size() + p_index;	//negative index

						vertices.push_back(Vertex(positions[p_index].x, positions[p_index].y, positions[p_index].z));
					}
					else if (face_format == 2){
						//position and texcoord
						int p_index = std::stoi(facetokens[0]);
						if (p_index>0) p_index -= 1;							//obj has 1...n indices
						else p_index = (unsigned int)positions.size() + p_index;//negative index

						int t_index = std::stoi(facetokens[1]);
						if (t_index>0) t_index -= 1;							//obj has 1...n indices
						else t_index = (unsigned int)texcoords.size() + t_index;//negative index

						vertices.push_back(Vertex(positions[p_index].x, positions[p_index].y, positions[p_index].z, texcoords[t_index].x, texcoords[t_index].y));
					}
					else if (face_format == 3){
						//position and normal
						int p_index = std::stoi(facetokens[0]);
						if (p_index>0) p_index -= 1;							//obj has 1...n indices
						else p_index = (unsigned int)positions.size() + p_index;//negative index

						int n_index = std::stoi(facetokens[1]);
						if (n_index>0) n_index -= 1;							//obj has 1...n indices
						else n_index = (unsigned int)normals.size() + n_index;	//negative index

						vertices.push_back(Vertex(positions[p_index].x, positions[p_index].y, positions[p_index].z, normals[n_index].x, normals[n_index].y, normals[n_index].z));
					}
					else{
						//position, normal and texcoord
						int p_index = std::stoi(facetokens[0]);
						if (p_index>0) p_index -= 1;								//obj has 1...n indices
						else p_index = (unsigned int)positions.size() + p_index;	//index negativ

						int t_index = std::stoi(facetokens[1]);
						if (t_index>0) t_index -= 1;								//obj has 1...n indices
						else t_index = (unsigned int)normals.size() + t_index;		//index negativ

						int n_index = std::stoi(facetokens[2]);
						if (n_index>0) n_index -= 1;								//obj has 1...n indices
						else n_index = (unsigned int)normals.size() + n_index;		//index negativ

						vertices.push_back(Vertex(positions[p_index].x, positions[p_index].y, positions[p_index].z, normals[n_index].x, normals[n_index].y, normals[n_index].z, texcoords[t_index].x, texcoords[t_index].y));
					}

					//add indices
					if (num_token<4){
						if (num_token == 1) index_of_first_vertex_of_face = (unsigned int)vertices.size() - 1;
						//make triangles f 0 1 2 3 (4 indices, first is used by f)
						indices.push_back((unsigned int)vertices.size() - 1);
					}
					else{
						indices.push_back(index_of_first_vertex_of_face);
						indices.push_back((unsigned int)vertices.size() - 2);
						indices.push_back((unsigned int)vertices.size() - 1);
					}
				}//end for
			}//end face

		}//end while
	}

}
