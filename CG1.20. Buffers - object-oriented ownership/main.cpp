///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/rotate_vector.hpp"
#include "../dependencies/glm/gtx/transform.hpp"
#include "../dependencies/glm/gtx/transform2.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"
#include "shader.h"
#include "camera.h"
#include "mesh.h"

const std::string ASSET_PATH = "../../assets/";

namespace lab {
	class CGLab {
		unsigned int framebuffer_width = 0, framebuffer_height = 0;
		Camera camera;
		std::vector<Mesh> meshes;
		Shader shader1, shader2;

		//toggles
		bool toggle_rasterization_mode = true;
		bool toggle_shader = true;
		bool toggle_pause = false;

		//uniform buffers, put dynamic data into one ubo and update it per frame, put all static data in another ubo and update it once
		// first object has the static draw hint (upload once, use often), the second the dynamic draw hint (upload often, use often)
		Buffer ubostatic{ GL_DYNAMIC_STORAGE_BIT }, ubodynamic{ GL_DYNAMIC_STORAGE_BIT };

		//---------------------------------------- IMPORTANT ----------------------------------------
		// ubos can only use the packed, shared and std140 storage formats. We'll use std140.
		// please read the packing information before trying to modify this structure : 
		//		 https://www.opengl.org/registry/doc/glspec45.core.pdf#page=158
		// because of these rules we will padd many fields to vec4s
		// NOTE: the purpose here is clarity not efficiency, it is obvious that the data can be more
		// efficiently packed.
		//-------------------------------------------------------------------------------------------
		
		//static data
		struct UboStaticData {
			glm::mat4 model_matrix[4] = {
				glm::rotate(glm::translate(glm::mat4(1), glm::vec3(-60, 0, 0)), 60.0f, glm::vec3(0, 1, 0)),
				glm::mat4(1),
				glm::translate(glm::mat4(1), glm::vec3(40, 0, 0)),
				glm::translate(glm::mat4(1), glm::vec3(0, -20, 0))
			};
			glm::vec4 material_constants[4] = {
				glm::vec4(0.05f, 0.48f, 0.47f, 16.0f),
				glm::vec4(0.05f, 0.85f, 0.1f, 2.0f),	
				glm::vec4(0.05f, 0.1f, 0.85f, 64.0f),	
				glm::vec4(0.05f, 0.85f, 0.1f, 1.0f)		
			};
			glm::vec4 material_color[4] = {
				glm::vec4(1, 0, 0, 0),
				glm::vec4(0, 1, 0, 0),
				glm::vec4(0, 0, 1, 0),
				glm::vec4(0.6, 0.6, 0.6, 0)
			};
			glm::vec4 light_direction = glm::vec4(0, -1, 0, 0);
			glm::vec4 light_angles = glm::vec4(glm::radians(4.0f), glm::radians(7.0f), 0, 0);
			glm::vec4 light_color = glm::vec4(1, 1, 1, 0);										
			glm::vec4 light_attenuation = glm::vec4(1, 0.0001f, 0.000002f, 0);					
		} ubostaticdata;

		//dynamic data
		struct UboDynamicData {
			glm::mat4 view_matrix, projection_matrix;
			glm::vec4 light_position = glm::vec4(-150, 220, 0, 0);
			glm::vec4 light_falloff = glm::vec4(0, 0, 0, 0);
			glm::vec4 camera_position;
		}ubodynamicdata;


	public:
		CGLab(unsigned int width, unsigned int height) {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;
			//window
			framebuffer_width = width;
			framebuffer_height = height;

			//state
			glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glFrontFace(GL_CCW);

			//shader
			shader1.load("../shader/blinn_gouraud.vs", "../shader/blinn_gouraud.fs");
			shader2.load("../shader/blinn_phong.vs", "../shader/blinn_phong.fs");
			
			//transformations
			camera.set(glm::vec3(10, 20, 50), glm::vec3(0, 20, 0), glm::vec3(0, 1, 0));
			ubodynamicdata.projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);

			//load some objects
			meshes.resize(4);
			meshes[0].load(ASSET_PATH + "models/standard/dragon.obj");
			meshes[1].load(ASSET_PATH + "models/standard/bunny.obj");
			meshes[2].load(ASSET_PATH + "models/standard/sphere.obj");
			meshes[3].load(ASSET_PATH + "models/standard/ground.obj");

			//uniform buffer objects
			ubostatic.resize(sizeof(UboStaticData));
			ubodynamic.resize(sizeof(UboDynamicData));

			//upload and bind the static ubo once
			ubostatic.update(&ubostaticdata, 0, sizeof(UboStaticData));
			ubostatic.bindIndexed(GL_UNIFORM_BUFFER, 0);
		}
		~CGLab() {
			//raii
			std::cout << "[LAB] Destroyed" << std::endl << "----------------------------------------------" << std::endl;
		}
		void render() {
			//non-rendering related
			static int delta = 0;
			if (!toggle_pause) delta = (delta + 1) % 600;
			
			//state
			glViewport(0, 0, framebuffer_width, framebuffer_height);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			if(toggle_rasterization_mode) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			else glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			//update the ubo data
			ubodynamicdata.view_matrix = camera.getViewMatrix();
			ubodynamicdata.light_position = glm::vec4(glm::vec3(-150, 320, 0)+glm::vec3((delta <= 300) ? delta : (600 - delta), 0, 0), 1);
			ubodynamicdata.camera_position = glm::vec4(camera.getPostion(), 1);

			//update the dynamic ubo object and bind it to the pipeline
			ubodynamic.update(&ubodynamicdata, 0, sizeof(UboDynamicData));
			ubodynamic.bindIndexed(GL_UNIFORM_BUFFER, 1);

			//chose shader
			Shader* shader;
			if (toggle_shader) shader = &shader1;
			else shader = &shader2;
			shader->bind();
			//instead of binding all the uniforms we send only the object id, we read ALL the required data from the ubos.
			for ( int i = 0; i < 4; i++) {
				shader->setUniform("id", i);
				meshes[i].draw(GL_TRIANGLES);
			}

		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			framebuffer_width = width;
			framebuffer_height = height;
			ubodynamicdata.projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			//reload all shaders
			switch (key) {
			case lap::wic::Key::SPACE:
				std::cout << "-------------------------------------" << std::endl << "[LAB] reloading shader" << std::endl;
				shader1.reload();
				shader2.reload();
				break;
			case lap::wic::Key::ESCAPE: wnd.close();			break;//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
			case lap::wic::Key::A:
			case lap::wic::Key::LEFT:
				camera.translateRight(-0.5f);
				break;
			case lap::wic::Key::D:
			case lap::wic::Key::RIGHT:
				camera.translateRight(0.5f);
				break;
			case lap::wic::Key::W:
			case lap::wic::Key::UP:
				camera.translateForward(0.5f);
				break;
			case lap::wic::Key::S:
			case lap::wic::Key::DOWN:
				camera.translateForward(-0.5f);
				break;
			case lap::wic::Key::R:
				camera.translateUpward(0.5f);
				break;
			case lap::wic::Key::F:
				camera.translateUpward(-0.5f);
				break;
			//toggle rasterization mode
			case lap::wic::Key::NUM1:
				toggle_rasterization_mode = !toggle_rasterization_mode;
				break;
			//toggle shader
			case lap::wic::Key::NUM2:
				toggle_shader = !toggle_shader;
				break;
			//toggle falloff
			case lap::wic::Key::O:
				ubodynamicdata.light_falloff = glm::vec4(1) - ubodynamicdata.light_falloff;
				break;
			//pause light
			case lap::wic::Key::P:
				toggle_pause = !toggle_pause;
				break;
			case lap::wic::Key::F1:
			{
				static bool toggle_fullscreen = false;
				toggle_fullscreen = !toggle_fullscreen;
				static unsigned int oldw, oldh, oldpx, oldpy;
				if (toggle_fullscreen) {
					oldw = wnd.getWindowProperties().width;
					oldh = wnd.getWindowProperties().height;
					oldpx = wnd.getWindowProperties().position_x;
					oldpy = wnd.getWindowProperties().position_y;
					wnd.setFullscreen();
				}
				else {
					wnd.setWindowed(oldw, oldh, oldpx, oldpy);
				}
				
			}
			default:
				break;
			}
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			keyPress(wnd, key, alt, control, shift, system, state, timestamp);
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
			float dx = wnd.getWindowProperties().width / 2.0f - posx;
			float dy = wnd.getWindowProperties().height / 2.0f - posy;
			if (state.mod_shift) {
				camera.rotateTPSoY(dx / 10.0f, 40);
				camera.rotateTPSoX(dy / 10.0f, 40);
			}
			else {
				camera.rotateFPSoY(dx / 10.0f);
				camera.rotateFPSoX(dy / 10.0f);
			}
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	
	};
}



//debug function based on glDebugOutput
void APIENTRY debugFunctionCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam) {
	//ignore some type of warnings (otherwise the glsl compiler might spam the console with notifications)
	if (type == GL_DEBUG_TYPE_PERFORMANCE || type == GL_DEBUG_TYPE_PORTABILITY || severity == GL_DEBUG_SEVERITY_NOTIFICATION) return;

	std::cout << "-------------------------------------------------------------------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:				std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:	std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:		std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:		std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:				std::cout << "Source: Other"; break;
	}
	std::cout << std::endl;
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	}
	std::cout << std::endl;
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:			std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:			std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:				std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:	std::cout << "Severity: notification"; break;
	}
	std::cout << std::endl;
	std::cout << "Press any key" << std::endl;
	std::cin.get();
}


int main(int argc, char* argv[]) {

	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Buffers - explicit storage.";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	fp.samples_per_pixel = 4;	//request multiple samples per pixel
	cp.swap_interval = -1;
	cp.debug_context = true;
	ip.cursor_enabled = false;	//cursor is not visible and always reverts to middle of the screen after movement
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//use the current context to attach a debugging function, this is only possible if the debug flags are enabled
	GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(debugFunctionCallback, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}

	//lab object
	lab::CGLab lab(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {
		lab.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};
	
}