###Screenshots###

depth test off:
![results](img/output.png)
depth test on:
![results](img/output_2.png)
z-fighting:
![results](img/output_5.png)
multisampling off:
![results](img/output_3.png)
multisampling on:
![results](img/output_4.png)
scissor example:
![results](img/output_6.png)

 