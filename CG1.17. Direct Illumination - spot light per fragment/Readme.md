###Screenshots###

NOTE: the color banding on the ground is not a bug, it happens because the 256 color hues provided by the 24-bit color depth (8 bit red, 8 bit green, 8 bit blue) are not enough to represent the resulting floor gradient.

gouraud shading with the blinn reflection model, illumination visible on the objects but not on the plane. Why? The spotlight intersects the fragments of the ground but not the vertices => the gouraud shading model completely fails!
![results](img/output.png)
gouraud shading with the blinn reflection model, wireframe showing the spotlight not intersecting ground vertices
![results](img/output_2.png)
gouraud shading with the blinn reflection model, the ground was tessellated => some vertices are intersected. Very low quality results on the edge of the lighted area.
![results](img/output_3.png)
gouraud shading with the blinn reflection model, the ground was tessellated, the spot light now has a falloff function, still artifacts at transition area
![results](img/output_4.png)
phong shading with the blinn reflection model. Crisp results, ground does not need to be tessellated.
![results](img/output_5.png)
phong shading with the blinn reflection model. Crisp results, ground does not need to be tessellated. Added a falloff function.
![results](img/output_6.png)

