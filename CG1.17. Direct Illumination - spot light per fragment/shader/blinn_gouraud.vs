//this shader illuminates geometry with the blinn reflection model and with gouraud shading
#version 450

//attributes
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

//transformations
uniform mat4 model_matrix, model_normal_matrix, view_matrix, projection_matrix;

//light and material uniforms
uniform vec3 light_color;
uniform vec3 light_position;
uniform vec3 light_attenuation;
uniform vec3 light_direction;
uniform vec2 light_angles;
uniform int light_falloff;

//eye/camera
uniform vec3 camera_position;

//material
uniform vec4 material_constants;
uniform vec3 material_color;

//output to fragment shader
out vec3 vs_color;

void main(){

	vec3 position_ws = (model_matrix * vec4(in_position, 1)).xyz;
	vec3 normal_ws = normalize(mat3(model_normal_matrix) * in_normal);

	//position to light source
	vec3 L = normalize(light_position - position_ws);
	vec3 N = normalize(normal_ws);

	// constants
	float Ka = material_constants.x;
	float Kd = material_constants.y;
	float Ks = material_constants.z;
	float specularity = material_constants.w;
	vec3 ambient = vec3(0,0,0);
	vec3 diffuse = vec3(0,0,0);
	vec3 specular =vec3(0,0,0);


	//spot
	vec3 D = normalize(light_direction);
	float cosu = dot(-L,D);
	float cos_inner = cos(light_angles.x);
	float cos_outer = cos(light_angles.y);
	
	//if in spot
	if( cosu >= cos_outer){

		//attenuation
		float dist = distance(light_position, position_ws);
		float attenuation = 1.0f / (light_attenuation.x + light_attenuation.y * dist + light_attenuation.z * dist * dist);
			
		//falloff
		float falloff = 1.0f;
		if(light_falloff > 0 && cosu < cos_inner){
			falloff = clamp( (cosu - cos_outer) / (cos_inner-cos_outer), 0.0, 1.0);		//linear interpolation
		}

		//total illumination
		ambient = Ka * light_color;
		diffuse = Kd * max(dot(L,N),0) * light_color * attenuation * falloff;
		specular = vec3(0,0,0);
		if(dot(L,N)>=0){
			vec3 R = reflect(-L, N);
			vec3 V = normalize(camera_position - position_ws);
			specular = Ks * pow ( max(dot(R,V),0), specularity ) * light_color * attenuation * falloff;
		}
	}else{
		ambient = Ka* light_color;
	}

	//final color
	vs_color = material_color * (ambient + diffuse + specular);

	//send vertex to rasterizer
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position,1);
}

