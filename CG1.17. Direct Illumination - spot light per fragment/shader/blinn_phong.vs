//this shader illuminates geometry with the blinn reflection model and with phong (per fragment) shading
#version 450

//attributes
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoord;

//transformations
uniform mat4 model_matrix, model_normal_matrix, view_matrix, projection_matrix;

//output to fragment shader
out vec3 position_ws, normal_ws;

void main(){

	position_ws = (model_matrix * vec4(in_position, 1)).xyz;
	normal_ws = normalize(mat3(model_matrix) * in_normal);

	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position,1);
}

