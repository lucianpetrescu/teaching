///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/rotate_vector.hpp"
#include "../dependencies/glm/gtx/transform.hpp"
#include "../dependencies/glm/gtx/transform2.hpp"
#include "../dependencies/lap_wic/lap_wic.hpp"
#include "shader.h"
#include "camera.h"
#include "mesh.h"

const std::string ASSET_PATH = "../../assets/";

namespace lab {
	class CGLab {
		unsigned int framebuffer_width = 0, framebuffer_height = 0;
		glm::mat4 model_matrix1, model_matrix2, model_matrix3, model_matrix4, projection_matrix;
		Camera camera;
		Mesh mesh1, mesh2, mesh3, mesh4simple, mesh4tess;
		Shader shader1, shader2;

		//material : Ka, Kd, Ks, specularity/shininess/specular exponent
		glm::vec4 material_constants1 = glm::vec4(0.05f, 0.48f, 0.47f, 16.0f);	//equally diffuse and specular, medium specularity
		glm::vec4 material_constants2 = glm::vec4(0.05f, 0.85f, 0.1f, 2.0f);	//highly diffuse
		glm::vec4 material_constants3 = glm::vec4(0.05f, 0.1f, 0.85f, 64.0f);	//highly specular, high specularity
		glm::vec4 material_constants4 = glm::vec4(0.05f, 0.85f, 0.1f, 1.0f);	//highly diffuse

		//light
		glm::vec3 light_position = glm::vec3(-150, 220, 0);
		//glm::vec3 light_direction = glm::normalize(glm::vec3(2, -5, 2));
		glm::vec3 light_direction = glm::normalize(glm::vec3(0, -1, 0));
		glm::vec2 light_angles = glm::vec2(glm::radians(8.0f), glm::radians(15.0f));	//full lighting untill 8 degrees, falloff until 15 degrees
		glm::vec3 light_color = glm::vec3(1, 1, 1);					//white light
		glm::vec3 light_attenuation = glm::vec3(1, 0.0001f, 0.000002f);	//attenuation

		//toggles
		bool toggle_rasterization_mode = true;
		bool toggle_shader = false;
		bool toggle_light_falloff = false;
		bool toggle_pause = false;
		bool toggle_ground = false;
	public:
		CGLab(unsigned int width, unsigned int height) {
			std::cout << std::endl << "----------------------------------------------" << std::endl << " [LAB] Created" << std::endl;
			//window
			framebuffer_width = width;
			framebuffer_height = height;

			//state
			glClearColor(0.2f, 0.2f, 0.2f, 0.0f);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glFrontFace(GL_CCW);

			//shader
			shader1.load("../shader/blinn_gouraud.vs", "../shader/blinn_gouraud.fs");
			shader2.load("../shader/blinn_phong.vs", "../shader/blinn_phong.fs");

			//transformations
			camera.set(glm::vec3(10, 20, 50), glm::vec3(0, 20, 0), glm::vec3(0, 1, 0));
			projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);

			//load some objects
			model_matrix1 = glm::rotate(glm::translate(glm::mat4(1), glm::vec3(-60, 0, 0)), 60.0f, glm::vec3(0, 1, 0));
			mesh1.load(ASSET_PATH + "models/standard/dragon.obj");
			model_matrix2 = glm::mat4(1);
			mesh2.load(ASSET_PATH + "models/standard/bunny.obj");
			model_matrix3 = glm::translate(glm::mat4(1), glm::vec3(40, 0, 0));
			mesh3.load(ASSET_PATH + "models/standard/sphere.obj");
			model_matrix4 = glm::translate(glm::mat4(1), glm::vec3(0, -20, 0));
			//load 2 grounds : one is a simple plane, the other is a highly tesselated surface
			mesh4simple.load(ASSET_PATH + "models/standard/plane.obj");
			mesh4tess.load(ASSET_PATH + "models/standard/ground.obj");

		}
		~CGLab() {
			//raii
			std::cout << "[LAB] Destroyed" << std::endl << "----------------------------------------------" << std::endl;
		}
		void render() {
			//state
			glViewport(0, 0, framebuffer_width, framebuffer_height);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			if(toggle_rasterization_mode) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			else glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			//chose shader
			Shader* shader;
			if (toggle_shader) shader = &shader1;
			else shader = &shader2;
						
			//use shader and set view and projection
			shader->bind();
			shader->setUniform("view_matrix", camera.getViewMatrix());
			shader->setUniform("projection_matrix", projection_matrix);

			//light parameters
			static int delta = 0;
			if (!toggle_pause) delta = (delta + 1) % 600;
			shader->setUniform("light_position", light_position + glm::vec3( (delta<=300)?delta:(600-delta),0,0));	//light rotates
			shader->setUniform("light_direction", light_direction);
			shader->setUniform("light_color", light_color);
			shader->setUniform("light_attenuation", light_attenuation);
			shader->setUniform("light_angles", light_angles);
			if (toggle_light_falloff) shader->setUniform("light_falloff", (int)1);
			else shader->setUniform("light_falloff", (int)0);

			//eye
			shader->setUniform("camera_position", camera.getPostion());
			
			//draw objects (material + transformation)
			shader->setUniform("material_color", glm::vec3(1, 0, 0));
			shader->setUniform("material_constants", material_constants1);
			shader->setUniform("model_matrix", model_matrix1);
			shader->setUniform("model_normal_matrix", glm::transpose(glm::inverse(model_matrix1)));
			mesh1.draw(GL_TRIANGLES);

			shader->setUniform("material_color", glm::vec3(0, 1, 0));
			shader->setUniform("material_constants", material_constants2);
			shader->setUniform("model_matrix", model_matrix2);
			shader->setUniform("model_normal_matrix", glm::transpose(glm::inverse(model_matrix2)));
			mesh2.draw(GL_TRIANGLES);

			shader->setUniform("material_color", glm::vec3(0, 0, 1));
			shader->setUniform("material_constants", material_constants3);
			shader->setUniform("model_matrix", model_matrix3);
			shader->setUniform("model_normal_matrix", glm::transpose(glm::inverse(model_matrix3)));
			mesh3.draw(GL_TRIANGLES);

			shader->setUniform("material_color", glm::vec3(0.6, 0.6, 0.6));
			shader->setUniform("material_constants", material_constants4);
			shader->setUniform("model_matrix", model_matrix4);
			shader->setUniform("model_normal_matrix", glm::transpose(glm::inverse(model_matrix4)));
			if(toggle_ground) mesh4tess.draw(GL_TRIANGLES);
			else mesh4simple.draw(GL_TRIANGLES);
		}

		void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
			framebuffer_width = width;
			framebuffer_height = height;
			projection_matrix = glm::perspective(90.0f, framebuffer_width / (float)framebuffer_height, 0.1f, 1000.0f);
		}
		void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			//reload all shaders
			switch (key) {
			case lap::wic::Key::SPACE:
				std::cout << "-------------------------------------" << std::endl << "[LAB] reloading shader" << std::endl;
				shader1.reload();
				shader2.reload();
				break;
			case lap::wic::Key::ESCAPE: wnd.close();			break;//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed
			case lap::wic::Key::A:
			case lap::wic::Key::LEFT:
				camera.translateRight(-0.5f);
				break;
			case lap::wic::Key::D:
			case lap::wic::Key::RIGHT:
				camera.translateRight(0.5f);
				break;
			case lap::wic::Key::W:
			case lap::wic::Key::UP:
				camera.translateForward(0.5f);
				break;
			case lap::wic::Key::S:
			case lap::wic::Key::DOWN:
				camera.translateForward(-0.5f);
				break;
			case lap::wic::Key::R:
				camera.translateUpward(0.5f);
				break;
			case lap::wic::Key::F:
				camera.translateUpward(-0.5f);
				break;
			//toggle rasterization mode
			case lap::wic::Key::NUM1:
				toggle_rasterization_mode = !toggle_rasterization_mode;
				break;
			//toggle shader
			case lap::wic::Key::NUM2:
				toggle_shader = !toggle_shader;
				break;
			//toggle ground
			case lap::wic::Key::NUM3:
				toggle_ground = !toggle_ground;
				break;
			//toggle falloff
			case lap::wic::Key::O:
				toggle_light_falloff = !toggle_light_falloff;
				break;
			//pause light
			case lap::wic::Key::P:
				toggle_pause = !toggle_pause;
				break;
			case lap::wic::Key::F1:
			{
				static bool toggle_fullscreen = false;
				toggle_fullscreen = !toggle_fullscreen;
				static unsigned int oldw, oldh, oldpx, oldpy;
				if (toggle_fullscreen) {
					oldw = wnd.getWindowProperties().width;
					oldh = wnd.getWindowProperties().height;
					oldpx = wnd.getWindowProperties().position_x;
					oldpy = wnd.getWindowProperties().position_y;
					wnd.setFullscreen();
				}
				else {
					wnd.setWindowed(oldw, oldh, oldpx, oldpy);
				}
				
			}
			default:
				break;
			}
		}
		void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
			keyPress(wnd, key, alt, control, shift, system, state, timestamp);
		}
		void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
		void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
			float dx = wnd.getWindowProperties().width / 2.0f - posx;
			float dy = wnd.getWindowProperties().height / 2.0f - posy;
			if (state.mod_shift) {
				camera.rotateTPSoY(dx / 10.0f, 40);
				camera.rotateTPSoX(dy / 10.0f, 40);
			}
			else {
				camera.rotateFPSoY(dx / 10.0f);
				camera.rotateFPSoX(dy / 10.0f);
			}
		}
		void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		}
	
	};
}



int main(int argc, char* argv[]) {
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "CGLab. Lighting - Phong/Gouraud shading with a spot light source. Tessellation and lighting.";						
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	fp.samples_per_pixel = 4;	//request multiple samples per pixel
	cp.swap_interval = -1;
	ip.cursor_enabled = false;	//cursor is not visible and always reverts to middle of the screen after movement
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//lab object
	lab::CGLab lab(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&lab::CGLab::resize, &lab, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&lab::CGLab::keyPress, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&lab::CGLab::keyRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&lab::CGLab::keyRepeat, &lab, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&lab::CGLab::mousePress, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&lab::CGLab::mouseRelease, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&lab::CGLab::mouseDrag, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&lab::CGLab::mouseMove, &lab, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&lab::CGLab::mouseScroll, &lab, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));
	
	//main loop
	while (window.isOpened()) {
		lab.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};

}