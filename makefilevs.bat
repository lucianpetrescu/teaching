@echo off
rem -----------------------------------------------------------------------------------------
rem this pseudo-makefile uses Microsfot Visual Studio 2015. If this IDE is unavailable then the
rem MinGW-enabled makefile can be used. This requires a working 64bit, cpp11 enabled MinGW.
rem -----------------------------------------------------------------------------------------

:Start
rem targets
if "%~1" == "all" goto AllTarget
if "%~1" == "build" goto AllTarget
if "%~1" == "debug" goto DebugTarget
if "%~1" == "release" goto ReleaseTarget
if "%~1" == "clean" goto CleanTarget
if "%~1" == "help" goto HelpTarget
if "%~1" == "list" goto ListTarget
if "%~1" == "install" goto InstallTarget
goto AllTarget

:DebugTarget
	@echo.
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Debug only ------------------------------
	@echo ------------------------------------------------------------------------------------
	for /D %%i in ("CG*") do (
		if exist %%i (
			chdir %%i
			@echo.
			if exist "makefilevs.bat" call "makefilevs.bat" debug 
			chdir ..
		) else (
			@echo ERROR, %%i does NOT exist
			@echo.
			goto End
		)
	)
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
goto End

:ReleaseTarget
	@echo.
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Release only ----------------------------
	@echo ------------------------------------------------------------------------------------
	for /D %%i in ("CG*") do (
		if exist %%i (
			chdir %%i
			@echo.
			if exist "makefilevs.bat" call "makefilevs.bat" release 
			chdir ..
		) else (
			@echo ERROR, %%i does NOT exist
			@echo.
			goto End
		)
	)
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
goto End

:AllTarget
	@echo.
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Debug and Release -----------------------
	@echo ------------------------------------------------------------------------------------
	for /D %%i in ("CG*") do (
		if exist %%i (
			chdir %%i
			@echo.
			if exist "makefilevs.bat" call "makefilevs.bat" all 
			chdir ..
		) else (
			@echo ERROR, %%i does NOT exist
			@echo.
			goto End
		)
	)
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
goto End

:CleanTarget
	@echo.
	@echo ------------------------------------------------------------------------------------
	@echo --------------------------------Cleaning all projects ------------------------------
	@echo ------------------------------------------------------------------------------------
	for /D %%i in ("CG*") do (
		if exist %%i (
			chdir %%i
			@echo.
			if exist "makefilevs.bat" call "makefilevs.bat" clean 
			chdir ..
		) else (
			@echo ERROR, %%i does NOT exist
			@echo.
			goto End
		)
	)
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
goto End

:HelpTarget
	@echo.
	@echo ---This makefile calls the makefiles of all labs in this repository  ---
	@echo.
	@echo use -make build- or -make all- to build both debug and release executables
	@echo use -make debug- to build only in debug mode
	@echo use -make release- to build only in release mode
	@echo use -make clean- to clean all visual studio related binaries and additional files (.suo, .sdf, .db, .pdb, etc)
goto End

:ListTarget
	@echo.
	@echo --- The list of labs :  ---
	@echo.
	for /D %%i in ("CG*") do @echo %%i
goto End

:InstallTarget
	@echo nothing to install
goto End
	
:End

