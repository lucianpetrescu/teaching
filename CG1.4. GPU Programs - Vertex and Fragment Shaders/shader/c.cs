#version 450
layout(local_size_x = 32) in;
layout(std430, binding = 0) buffer BlockName0 {
	float data[];
};
void main(){
	uint index = gl_GlobalInvocationID.x;
	if(index < 12) data[index] = index;
}
